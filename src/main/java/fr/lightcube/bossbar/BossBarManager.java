package fr.lightcube.bossbar;

import fr.lightcube.LCSpigot;
import fr.lightcube.utils.CommonUtilsSpigot;
import net.minecraft.server.v1_8_R3.Packet;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Nitorac on 26/05/2017.
 */
public class BossBarManager {

    private static final Map<UUID, BossBar> withers = new HashMap<>();

    public BossBarManager() {
        /*new BukkitRunnable() {
            @Override
            public void run() {
                //CommonUtilsSpigot.getOnlinePlayers().forEach(p -> teleport(p));
                CommonUtilsSpigot.getOnlinePlayers().forEach(p -> {
                    if(!hasBossBar(p)) return;
                    updateBossbar(p, getBossBar(p));
                });
            }
        }.runTaskTimer(LCSpigot.getInstance(), 0L, 10L);*/
    }

    public String getMessage(Player player) {
        if (!hasBossBar(player)) return "";
        return getBossBar(player).getMessage();
    }

    public float getPercentage(Player player) {
        if (!hasBossBar(player)) return 0;
        return getBossBar(player).getPercentage();
    }

    public void remove(Player player) {
        if (!hasBossBar(player)) return;
        removeBossbar(player);
    }

    public void updateAll(Player player, String message, float percentage) {
        if (percentage > 1) percentage = 1;
        else if (percentage < 0) percentage = 0;
        BossBar bar = getBossBar(player);
        if (bar == null) {
            bar = setMessage(player, message);
        }
        bar.setMessage(message);
        bar.setPercentage(percentage);
        updateBossbar(player, bar);
    }

    public void updateMessage(Player player, String message) {
        BossBar bar = getBossBar(player);
        if (bar == null) {
            bar = setMessage(player, message);
        }
        bar.setMessage(message);
        updateBossbar(player, bar);
    }

    public void updatePercentage(Player player, float percentage) {
        if (percentage > 1) percentage = 1;
        else if (percentage < 0) percentage = 0;
        BossBar bar = getBossBar(player);
        if (bar == null) {
            bar = setMessage(player, "&6&lLCBossBar");
        }
        bar.setPercentage(percentage);
        updateBossbar(player, bar);
    }

    public void onQuit(Player p) {
        removeBossbar(p);
    }

    public void onKick(Player p) {
        removeBossbar(p);
    }

    public void onTeleport(Player p) {
        teleport(p);
    }

    public void onRespawn(Player p) {
        teleport(p);
    }

    private void teleport(final Player player) {
        if (!hasBossBar(player)) return;
        new BukkitRunnable() {
            @Override
            public void run() {
                if (!hasBossBar(player)) return;
                BossBar bar = getBossBar(player);
                float percentage = bar.getPercentage();
                String message = bar.getMessage();
                sendPacket(player, bar.getDestroyPacket());
                bar = setMessage(player, message, percentage);
                updateBossbar(player, bar);
            }
        }.runTaskLater(LCSpigot.getInstance(), 2L);
    }

    public BossBar setMessage(Player player, String message, float percent) {
        //BossBar wither = new BossBar(message, player.getLocation().add(player.getEyeLocation().getDirection().multiply(30)).add(0, -10, 0), percent);
        BossBar wither = new BossBar(message, getWitherLoc(player), percent);
        sendPacket(player, wither.getSpawnPacket());
        withers.put(player.getUniqueId(), wither);
        return wither;
    }

    public BossBar setMessage(Player player, String message) {
        return setMessage(player, message, 100.0F);
    }

    public void removeBossbar(Player player) {
        try {
            sendPacket(player, getBossBar(player).getDestroyPacket());
            withers.remove(player.getUniqueId());
        } catch (Exception ignore) {
        }
    }

    public BossBar getBossBar(Player player) {
        return withers.get(player.getUniqueId());
    }

    public boolean hasBossBar(Player player) {
        return withers.containsKey(player.getUniqueId());
    }

    private Location getWitherLoc(Player p){
        return p.getLocation().add(p.getEyeLocation().getDirection().multiply(30));
    }

    public void updateBossbar(Player player, BossBar wither) {
        sendPacket(player, wither.getMetaPacket(wither.getWatcher()));
        sendPacket(player, wither.getTeleportPacket(getWitherLoc(player)));
        withers.put(player.getUniqueId(), wither);
    }

    public void disable() {
        CommonUtilsSpigot.getOnlinePlayers().forEach(this::removeBossbar);
        withers.clear();
    }

    public void sendPacket(Player p, Object packet) {
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket((Packet) packet);
    }
}
