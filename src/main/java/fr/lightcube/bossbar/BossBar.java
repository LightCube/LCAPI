package fr.lightcube.bossbar;

import fr.lightcube.utils.Utils;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

/**
 * Created by Nitorac on 27/05/2017.
 */
public class BossBar {

    public static final float MAX_HEALTH = 300;
    public float health = 0;
    public String name;
    private int x;
    private int y;
    private int z;
    private int pitch = 0;
    private int yaw = 0;
    private byte motX = 0;
    private byte motY = 0;
    private byte motZ = 0;
    private boolean visible = false;
    private World world;
    private EntityWither wither;
    private int id;

    public BossBar(String name, Location location, float percent) {
        setMessage(name);
        this.health = percent * MAX_HEALTH / 100;
        this.x = location.getBlockX();
        this.y = location.getBlockY();
        this.z = location.getBlockZ();
        this.world = ((CraftWorld) location.getWorld()).getHandle();
    }

    public Packet getSpawnPacket() {
        wither = new EntityWither(getWorld());
        wither.setLocation(getX(), getY(), getZ(), getPitch(), getYaw());
        wither.setInvisible(isVisible());
        wither.setCustomName(getName());
        wither.setHealth(getHealth());
        wither.motX = getMotX();
        wither.motY = getMotY();
        wither.motZ = getMotZ();
        this.id = wither.getId();
        return new PacketPlayOutSpawnEntityLiving(wither);
    }

    public Packet getDestroyPacket() {
        PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy();
        Utils.setFieldSilent(Utils.getFieldSilent(PacketPlayOutEntityDestroy.class, "a"), packet, new int[]{id});
        return packet;
    }

    public Packet getMetaPacket(DataWatcher watcher) {
        return new PacketPlayOutEntityMetadata(id, watcher, true);
    }

    public Packet getTeleportPacket(Location location) {
        return new PacketPlayOutEntityTeleport(
                this.id,
                location.getBlockX() * 32,
                location.getBlockY() * 32,
                location.getBlockZ() * 32,
                (byte)((int) location.getYaw() * 256 / 360),
                (byte)((int) location.getPitch() * 256 / 360),
                false
        );
    }

    public DataWatcher getWatcher() {
        DataWatcher watcher = new DataWatcher(wither);
        watcher.a(0, isVisible() ? (byte) 0 : (byte) 0x20);
        watcher.a(6, health);
        watcher.a(7, 0);
        watcher.a(8, (byte) 0);
        watcher.a(10, name);
        watcher.a(11, (byte) 1);
        return watcher;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public int getPitch() {
        return pitch;
    }

    public void setPitch(int pitch) {
        this.pitch = pitch;
    }

    public int getYaw() {
        return yaw;
    }

    public void setYaw(int yaw) {
        this.yaw = yaw;
    }

    public byte getMotX() {
        return motX;
    }

    public void setMotX(byte motX) {
        this.motX = motX;
    }

    public byte getMotY() {
        return motY;
    }

    public void setMotY(byte motY) {
        this.motY = motY;
    }

    public byte getMotZ() {
        return motZ;
    }

    public void setMotZ(byte motZ) {
        this.motZ = motZ;
    }

    public float getHealth() {
        return health;
    }

    public void setHealth(float health) {
        this.health = health;
    }

    public float getHealthScaled() {
        return health / MAX_HEALTH * 100;
    }

    private void setHealthScaled(float percentage) {
        this.health = percentage * MAX_HEALTH / 100;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public String getMessage() {
        return getName();
    }

    public void setMessage(String message) {
        setName(message);
    }

    public float getPercentage() {
        return getHealthScaled();
    }

    public void setPercentage(float percentage) {
        setHealthScaled(percentage);
    }
}
