package fr.lightcube.scoreboard;

import fr.lightcube.utils.CommonUtilsSpigot;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Nitorac on 28/05/2017.
 */
public class LCScoreboardManager {

    private static Map<UUID, LCScoreboard> scoreboards;

    public LCScoreboardManager() {
        scoreboards = new HashMap<>();
    }

    public void setScoreboard(Player p, LCScoreboard lcs){
        p.setScoreboard(lcs.getScoreboard());
        scoreboards.put(p.getUniqueId(), lcs);
    }

    public void removeScoreboard(Player p){
        p.setScoreboard(null);
        if(scoreboards.containsKey(p.getUniqueId())) scoreboards.remove(p.getUniqueId());
    }

    /**
     * Set LCScoreboard to all players !
     * @param lcs
     */
    public void setScoreboard(LCScoreboard lcs){
        CommonUtilsSpigot.getOnlinePlayers().forEach(p -> setScoreboard(p, lcs.clone()));
    }

    public LCScoreboard getScoreboard(Player p){
        return scoreboards.getOrDefault(p.getUniqueId(), null);
    }
}
