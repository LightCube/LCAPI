package fr.lightcube.warp;

import fr.lightcube.LCAPI;
import fr.lightcube.configuration.LCYamlConfiguration;
import fr.lightcube.utils.Csts;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.ArrayList;

/**
 * Created by Nitorac on 16/04/2017.
 */
public class WarpManager {

    private static ArrayList<Warp> warps;

    public WarpManager(){
        warps = new ArrayList<>();
        readWarpsFromFile();
    }

    public void readWarpsFromFile(){
        warps.clear();
        LCYamlConfiguration config = LCAPI.getConfigManager().get(Csts.WARPS_CONF).load();
        config.getSection("", false).forEach(warpName -> {
            String worldName = config.getString(warpName + ".world", "world");
            Double x = config.getDouble(warpName + ".x", 0.0D);
            Double y = config.getDouble(warpName + ".y", 0.0D);
            Double z = config.getDouble(warpName + ".z", 0.0D);
            Float yaw = config.getFloat(warpName + ".yaw", 0.0F);
            Float pitch = config.getFloat(warpName + ".pitch", 0.0F);
            String type = config.getString(warpName + ".type", "PUBLIC");

            World world = Bukkit.getWorld(worldName);
            if(world == null) return;
            warps.add(new Warp(warpName, new Location(world, x, y, z, yaw, pitch), Warp.WarpType.fromTxt(type)));
        });
    }

    public void saveWarps(){
        LCYamlConfiguration config = LCAPI.getConfigManager().get(Csts.WARPS_CONF);

        config.flush();

        warps.forEach(warp1 -> {
            config.setObject(warp1.getName() + ".world", warp1.getLoc().getWorld().getName());
            config.setObject(warp1.getName() + ".x", warp1.getLoc().getX());
            config.setObject(warp1.getName() + ".y", warp1.getLoc().getY());
            config.setObject(warp1.getName() + ".z", warp1.getLoc().getZ());
            config.setObject(warp1.getName() + ".yaw", warp1.getLoc().getYaw());
            config.setObject(warp1.getName() + ".pitch", warp1.getLoc().getPitch());
            config.setObject(warp1.getName() + ".type", warp1.getWarpType().getTxt());
        });

        config.save();
    }

    public void removeWarp(Warp warp){
        removeWarp(warp.getName());
    }

    public void removeWarp(String name){
        warps.stream().filter(warp -> warp.getName().equalsIgnoreCase(name)).findFirst().ifPresent(warp -> warps.remove(warp));
    }

    public Warp getWarp(String name){
        return warps.stream().filter(warp -> warp.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    public boolean teleportToWarp(Player p, String name){
        if(getWarp(name) == null){
            return false;
        }

        p.teleport(getWarp(name).getLoc(), PlayerTeleportEvent.TeleportCause.PLUGIN);
        return true;
    }

    public void addWarp(Warp warp){
        Warp existing = getWarp(warp.getName());
        if(existing == null){
            warps.add(warp);
        }else{
            warps.remove(existing);
            warps.add(warp);
        }
    }

    public ArrayList<String> getWarpNames(Warp.WarpType type){
        ArrayList<String> result = new ArrayList<>();
        warps.forEach(warp -> {
            if(warp.getWarpType().equals(type) || warp.getWarpType().equals(Warp.WarpType.PUBLIC)){
                result.add(warp.getName());
            }
        });
        return result;
    }

    public ArrayList<Warp> getWarpList(Warp.WarpType type){
        ArrayList<Warp> result = new ArrayList<>();
        warps.forEach(warp -> {
            if(warp.getWarpType().equals(type) || warp.getWarpType().equals(Warp.WarpType.PUBLIC)){
                result.add(warp);
            }
        });
        return result;
    }
}