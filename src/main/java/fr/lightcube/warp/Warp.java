package fr.lightcube.warp;

import org.bukkit.Location;

/**
 * Created by Nitorac on 16/04/2017.
 */
public class Warp {

    private String name;
    private Location loc;
    private WarpType warpType;

    public Warp(String name, Location loc, WarpType warpType){
        setName(name);
        setLoc(loc);
        setWarpType(warpType);
    }

    public Warp(String name, Location loc){
        this(name, loc, WarpType.PRIVATE);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLoc() {
        return loc;
    }

    public void setLoc(Location loc) {
        this.loc = loc;
    }

    public WarpType getWarpType() {
        return warpType;
    }

    public void setWarpType(WarpType warpType) {
        this.warpType = (warpType != null) ? warpType : WarpType.PUBLIC;
    }

    public boolean equalsName(Object obj){
        if(obj instanceof Warp && ((Warp)obj).getName().equalsIgnoreCase(getName())){
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object obj){
        if(obj instanceof Warp){
            Warp warp = (Warp)obj;
            if(warp.getName().equalsIgnoreCase(getName()) && warp.getLoc().equals(getLoc()) && warp.getWarpType().equals(getWarpType())){
                return true;
            }
        }
        return false;
    }


    public enum WarpType{
        /*******
         * Visible par les plugins et les joueurs
         */
        PUBLIC("PUBLIC"),


        /*******
         * Visible seulement par les plugins
         */
        PRIVATE("PRIVATE");

        private String txt;

        WarpType(String txt){
            this.txt = txt;
        }

        public String getTxt(){
            return txt;
        }

        public static WarpType fromTxt(String type){
            switch (type){
                case "PUBLIC":
                    return PUBLIC;
                case "PRIVATE":
                    return PRIVATE;
                default:
                    return PUBLIC;
            }
        }

        public boolean equalsWithTypes(WarpType type){
            return (type.equals(this) || type.equals(PUBLIC));
        }
    }
}
