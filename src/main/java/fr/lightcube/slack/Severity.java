package fr.lightcube.slack;

public enum Severity {
    INFO("Information", "#47d147"),
    WARN("Attention", "#ffd11a"),
    ERROR("Erreur", "#ff3333");

    private String colorHex;
    private String title;

    Severity(String title, String colorHex){
        this.title = title;
        this.colorHex = colorHex;
    }

    public String getColor(){
        return colorHex;
    }

    public String getTitle(){
        return title;
    }
}
