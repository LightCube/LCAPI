package fr.lightcube.slack;

import com.ullink.slack.simpleslackapi.SlackAttachment;
import com.ullink.slack.simpleslackapi.SlackPreparedMessage;
import com.ullink.slack.simpleslackapi.SlackSession;
import com.ullink.slack.simpleslackapi.impl.SlackChatConfiguration;
import com.ullink.slack.simpleslackapi.impl.SlackSessionFactory;
import fr.lightcube.LCAPI;
import fr.lightcube.players.LCPlayer;
import fr.lightcube.utils.Log;

import java.io.IOException;

/**
 * Created by Nitorac on 03/04/2017.
 */
public class SlackManager {

    private static SlackSession session;

    public SlackManager(){
        session = SlackSessionFactory.createWebSocketSlackSession("xoxb-163180252624-1XykA1UNrPkfhaLaC0fRQDjm");

        try {
            session.connect();
        } catch (IOException e) {
            e.printStackTrace();
            Log.FATAL.sendLog("Impossible de se connecter à Slack");
        }

    }

    public SlackSession getSession(){
        return session;
    }

    public void sendPlayerMessage(String channel, String message, LCPlayer player, boolean sendInstantly) throws IOException {
        SlackPreparedMessage slackMsg = new SlackPreparedMessage.Builder()
            .withMessage(message)
            .build();

        dispatch(() -> getSession().sendMessage(getSession().findChannelByName(channel), slackMsg, SlackChatConfiguration.getConfiguration()
            .withName(player.getName() + " — " + player.getServer().toString())
            .withIcon("https://visage.surgeplay.com/head/256/" + player.getUniqueId().toString().replaceAll("-", ""))), sendInstantly);
    }

    public void sendMsg(String channel, SlackPreparedMessage msg, boolean sendInstantly) throws IOException {
        dispatch(() -> getSession().sendMessage(getSession().findChannelByName(channel), msg), sendInstantly);
    }

    public void sendMsg(String channel, String msg, Severity sev, boolean sendInstantly) throws IOException {
        SlackAttachment attachment = new SlackAttachment();
        if(sev == null) sev = Severity.INFO;
        attachment.setTitle(sev.getTitle());
        attachment.setColor(sev.getColor());
        attachment.setText((sev.equals(Severity.ERROR)) ? "@everyone " + msg : msg);
        try{
            attachment.setFooter(LCAPI.getServerManager().getCurrentServer().toString());
        }catch (NullPointerException e){
            attachment.setFooter("Erreur");
        }
        attachment.setFallback(msg);

        SlackPreparedMessage slackMsg = new SlackPreparedMessage.Builder()
            .addAttachment(attachment)
            .build();

        dispatch(() -> getSession().sendMessage(getSession().findChannelByName(channel), slackMsg), sendInstantly);
    }

    private void dispatch(Runnable r, boolean sendInstantly){
        if(!sendInstantly){
            LCAPI.getCommonUtils().runAsync(r);
        }else{
            r.run();
        }
    }
}

