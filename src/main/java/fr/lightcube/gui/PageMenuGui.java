package fr.lightcube.gui;

import fr.lightcube.utils.Log;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

/**
 * Created by Nitorac on 05/06/2017.
 */
public class PageMenuGui{

    private ArrayList<PageGui> pages;
    private String title;
    private BaseGui parent;

    public PageMenuGui(String title){
        pages = new ArrayList<>();
        this.title = title;
    }

    public PageMenuGui(String title, BaseGui parent) {
        pages = new ArrayList<>();
        this.title = title;
        this.parent = parent;
    }

    public void open(Player player) {
        nextPage(getMinIndex() - 1).open(player);
    }

    public int getMinIndex(){
        Optional opt = pages.stream().min(Comparator.comparingInt(PageGui::getIndex));
        return (opt.isPresent()) ? ((PageGui)opt.get()).getIndex() : 0;
    }

    public int getMaxIndex(){
        Optional opt = pages.stream().max(Comparator.comparingInt(PageGui::getIndex));
        return (opt.isPresent()) ? ((PageGui)opt.get()).getIndex() : 0;
    }

    public String getTitle(){
        return title;
    }

    public String getTitleFromIndex(int index){
        return title + "§0 - Page §1" + (index+1);
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setParent(BaseGui parent){
        this.parent = parent;
    }

    public BaseGui getParent(){
        return parent;
    }

    public boolean hasParent(){
        return parent != null;
    }

    public PageMenuGui addPage(PageGui... pages){
        Arrays.stream(pages).filter(p -> !this.pages.contains(p)).forEach(p -> this.pages.add(p));
        return this;
    }

    public PageMenuGui addPages(ArrayList<PageGui> pages){
        pages.forEach(this::addPage);
        return this;
    }

    public boolean hasNextPage(int index){
        return containsPage(index + 1);
    }

    public boolean hasPrevPage(int index){
        return containsPage(index - 1);
    }

    public PageGui nextPage(int current){
        return (hasNextPage(current)) ? getPage(current + 1).onVisible() : null;
    }

    public PageGui prevPage(int current){
        return (hasPrevPage(current)) ? getPage(current - 1).onVisible() : null;
    }

    public PageGui getPage(int index){
        if(pages.size() <= 0){
            Log.ERROR.sendLog((getTitle() != null && !getTitle().isEmpty()) ? "Il y a 0 page dans l'inventaire paginé de titre : " + getTitle() : "Il y a 0 page dans l'inventaire paginé de titre null");
            return null;
        }
        return pages.stream().filter(p -> p.getIndex() == index).findFirst().orElse(null);
    }

    public void removePage(int index){
        if(containsPage(index)){
            pages.remove(index);
        }
    }

    public void replacePage(int index, PageGui newPage){
        if(containsPage(index)){
            pages.removeIf(p -> p.getIndex() == index);
        }
        addPage(newPage);
    }

    public boolean containsPage(PageGui page){
        return containsPage(page.getIndex());
    }

    public boolean containsPage(int index){
        return pages.stream().anyMatch(p -> p.getIndex() == index);
    }
}
