package fr.lightcube.gui.items;

import fr.lightcube.LCSpigot;
import fr.lightcube.gui.PageGui;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Nitorac on 10/06/2017.
 */
public class PrevPageItemGui extends BaseMenuItemGui{

    private PageGui page;

    public PrevPageItemGui(){
        this("§lPrécédent", new ItemStack(Material.ARROW, 1), "§oRevenir à la page précédente !");
    }

    public PrevPageItemGui(PageGui page){
        this(page,"§lPrécédent", new ItemStack(Material.ARROW, 1), "§oRevenir à la page précédente !");
    }

    public PrevPageItemGui(String displayName, ItemStack icon, String... lore) {
        super(displayName, icon, lore);

        this.e = e -> {
            LCSpigot.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(LCSpigot.getInstance(), () -> {
                if(e.getPlayer() != null && page != null && page.getPageParent().hasPrevPage(page.getIndex())) {
                    page.getPageParent().prevPage(page.getIndex()).open(e.getPlayer());
                }
            }, 1);
        };
    }

    public PrevPageItemGui(PageGui page, String displayName, ItemStack icon, String... lore) {
        this(displayName, icon, lore);
        this.page = page;
    }

    public PrevPageItemGui setPage(PageGui page){
        this.page = page;
        return this;
    }

    public PageGui getPage(){
        return page;
    }

    public PrevPageItemGui clone(){
        return new PrevPageItemGui(page, displayName, icon, lore.toArray(new String[lore.size()]));
    }
}
