package fr.lightcube.gui.items;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Nitorac on 03/06/2017.
 */
public class BackItemGui extends BaseMenuItemGui{

    public BackItemGui() {
        this(ChatColor.RED + "Retour", new ItemStack(Material.FENCE_GATE), "§oRevenir en arrière !");
    }

    public BackItemGui(String title, ItemStack icon, String...  lore){
        super(title, icon, lore);
        this.e = e -> e.setWillGoBack(true);
    }
}
