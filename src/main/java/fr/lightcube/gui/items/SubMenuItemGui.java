package fr.lightcube.gui.items;

import fr.lightcube.LCSpigot;
import fr.lightcube.gui.BaseGui;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Nitorac on 03/06/2017.
 */
public class SubMenuItemGui extends BaseMenuItemGui{
    private final BaseGui menu;

    public SubMenuItemGui(String displayName, ItemStack icon, BaseGui menu, String... lore) {
        super(displayName, icon, lore);
        this.menu = menu;
        this.e = e -> {
            LCSpigot.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(LCSpigot.getInstance(), () -> {
                if (e.getPlayer() != null) {
                    menu.open(e.getPlayer());
                }
            }, 1);
        };
    }
}
