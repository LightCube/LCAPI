package fr.lightcube.gui.items;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Nitorac on 03/06/2017.
 */
public class CloseItemGui extends BaseMenuItemGui{

    public CloseItemGui() {
        this(ChatColor.RED + "Fermer", new ItemStack(Material.BARRIER), "§oFermer le menu !");
    }

    public CloseItemGui(String title, ItemStack icon, String... lore) {
        super(title, icon, lore);
        this.e = e -> e.setWillClose(true);
    }
}
