package fr.lightcube.gui.items;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Nitorac on 03/06/2017.
 */
public class StaticMenuItemGui extends BaseMenuItemGui {

    public StaticMenuItemGui(String displayName, ItemStack icon, String... lore) {
        super(displayName, icon, lore);
        setNameAndLore(getIcon(), getDisplayName(), getLore());
    }

    @Override
    public ItemStack getFinalIcon(Player player) {
        return getIcon();
    }
}
