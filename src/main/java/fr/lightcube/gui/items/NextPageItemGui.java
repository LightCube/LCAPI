package fr.lightcube.gui.items;

import fr.lightcube.LCSpigot;
import fr.lightcube.gui.PageGui;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Nitorac on 10/06/2017.
 */
public class NextPageItemGui extends BaseMenuItemGui{

    private PageGui page;

    public NextPageItemGui() {
        this("§lSuivant", new ItemStack(Material.ARROW, 1), "§oPasser à la page suivante !");
    }

    public NextPageItemGui(PageGui page) {
        this(page, "§lSuivant", new ItemStack(Material.ARROW, 1), "§oPasser à la page suivante !");
    }

    public NextPageItemGui(String displayName, ItemStack icon, String... lore) {
        super(displayName, icon, lore);

        this.e = e -> {
            LCSpigot.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(LCSpigot.getInstance(), () -> {
                if(e.getPlayer() != null && page != null && page.getPageParent().hasNextPage(page.getIndex())) {
                    page.getPageParent().nextPage(page.getIndex()).open(e.getPlayer());
                }
            }, 1);
        };
    }

    public NextPageItemGui(PageGui page, String displayName, ItemStack icon, String... lore) {
        this(displayName, icon, lore);
        this.page = page;
    }

    public NextPageItemGui setPage(PageGui page){
        this.page = page;
        return this;
    }

    public PageGui getPage(){
        return page;
    }

    public NextPageItemGui clone(){
        return new NextPageItemGui(page, displayName, icon, lore.toArray(new String[lore.size()]));
    }
}
