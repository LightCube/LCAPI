package fr.lightcube.gui.items;

import fr.lightcube.gui.ItemClickEvent;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Nitorac on 03/06/2017.
 */
public class BaseMenuItemGui {
    protected final String displayName;
    protected final ItemStack icon;
    protected final List<String> lore;
    protected OnClickEvent e;

    public BaseMenuItemGui(String displayName, ItemStack icon, String... lore) {
        this.displayName = "§r" + displayName;
        this.icon = icon;
        this.lore = new ArrayList<>();
        Arrays.stream(lore).forEach(line -> this.lore.add("§r" + line));
    }

    public String getDisplayName() {
        return displayName;
    }

    public ItemStack getIcon() {
        return icon;
    }

    public List<String> getLore() {
        return lore;
    }

    public ItemStack getFinalIcon(Player player) {
        return setNameAndLore(getIcon().clone(), getDisplayName(), getLore());
    }

    public ItemClickEvent callClick(ItemClickEvent event){
        if(this.e != null){
            this.e.onClick(event);
        }
        return event;
    }

    public void setClickEvent(OnClickEvent e){
        this.e = e;
    }

    public static ItemStack setNameAndLore(ItemStack itemStack, String displayName, List<String> lore) {
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName(displayName);
        meta.setLore(lore);
        itemStack.setItemMeta(meta);
        return itemStack;
    }

public interface OnClickEvent{
    void onClick(ItemClickEvent e);
}
}