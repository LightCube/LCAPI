package fr.lightcube.gui;

import fr.lightcube.LCSpigot;
import fr.lightcube.gui.items.BaseMenuItemGui;
import fr.lightcube.gui.items.NextPageItemGui;
import fr.lightcube.gui.items.PrevPageItemGui;
import fr.lightcube.utils.Log;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Created by Nitorac on 05/06/2017.
 */
public class PageGui extends BaseGui {

    private PageMenuGui pageParent;
    private int index;
    private boolean isDefaultControl = false;
    private NextPageItemGui next;
    private PrevPageItemGui prev;
    private BaseMenuItemGui nextReplacer;
    private BaseMenuItemGui prevReplacer;

    public PageGui(PageMenuGui pageParent, int index, Size size, BaseGui parent) {
        super(pageParent.getTitleFromIndex(index), size, parent);
        this.pageParent = pageParent;
        this.index = index;
    }

    public PageGui(PageMenuGui pageParent, int index, Size size) {
        super(pageParent.getTitleFromIndex(index), size);
        this.pageParent = pageParent;
        this.index = index;
    }

    public PageGui(PageMenuGui pageParent, int index, String name, Size size, BaseGui parent) {
        super(name, size, parent);
        this.pageParent = pageParent;
        this.index = index;
    }

    public PageGui(PageMenuGui pageParent, int index, String name, Size size) {
        super(name, size);
        this.pageParent = pageParent;
        this.index = index;
    }

    public PageGui setItem(int position, BaseMenuItemGui item){
        return (PageGui)super.setItem(position, item);
    }

    public PageGui setDefaultControls(PrevPageItemGui prev, NextPageItemGui next){
        isDefaultControl = true;
        this.next = next;
        this.prev = prev;
        this.nextReplacer = getItem(this.items.length - 1);
        this.prevReplacer = getItem(this.items.length - 1 - 8);
        setItem(this.items.length - 1, next);
        setItem(this.items.length - 1 - 8, prev);
        return this;
    }

    public PageGui onVisible(){
        if(index == pageParent.getMinIndex()){
            setItem(this.items.length - 1 - 8, prevReplacer);
        }else if(index == pageParent.getMaxIndex()){
            setItem(this.items.length - 1, nextReplacer);
        }
        return this;
    }

    public PageMenuGui getPageParent(){
        return pageParent;
    }

    public int getIndex(){
        return index;
    }

    @Override
    public boolean hasParent(){
        return pageParent.hasParent();
    }

    @Override
    @SuppressWarnings("deprecation")
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getClick() == ClickType.LEFT) {
            int slot = event.getRawSlot();
            if (slot >= 0 && slot < size.getSize() && items[slot] != null) {
                Player player = (Player) event.getWhoClicked();
                ItemClickEvent itemClickEvent = new ItemClickEvent(player);
                items[slot].callClick(itemClickEvent);
                if (itemClickEvent.willUpdate()) {
                    update(player);
                } else {
                    player.updateInventory();
                    Player p = LCSpigot.getInstance().getServer().getPlayer(player.getUniqueId());
                    if (p != null) {
                        if (itemClickEvent.willClose() || itemClickEvent.willGoBack()) {
                            p.closeInventory();
                        }
                        if (itemClickEvent.willGoBack() && hasParent()) {
                            Log.INFO.sendLog("C'EST CENSÉ REVENIR EN ARRIÈRE");
                            pageParent.getParent().open(p);
                        }
                    }
                }
            }
        }
    }
}
