package fr.lightcube.gui;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

/**
 * Created by Nitorac on 03/06/2017.
 */
public class GuiHolder implements InventoryHolder {
    private BaseGui menu;
    private Inventory inventory;

    public GuiHolder(BaseGui menu, Inventory inventory) {
        this.menu = menu;
        this.inventory = inventory;
    }

    public BaseGui getMenu() {
        return menu;
    }

    @Override
    public Inventory getInventory() {
        return inventory;
    }
}