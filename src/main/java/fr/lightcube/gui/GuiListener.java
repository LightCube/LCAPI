package fr.lightcube.gui;

import fr.lightcube.utils.CommonUtilsSpigot;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

/**
 * Created by Nitorac on 03/06/2017.
 */
public class GuiListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getWhoClicked() instanceof Player && event.getInventory().getHolder() instanceof GuiHolder) {
            event.setCancelled(true);
            ((GuiHolder) event.getInventory().getHolder()).getMenu().onInventoryClick(event);
        }
    }
    public static void closeOpenMenus() {
        CommonUtilsSpigot.getOnlinePlayers().stream().filter(p -> p.getOpenInventory() != null).forEach(p -> {
            Inventory inventory = p.getOpenInventory().getTopInventory();
            if (inventory.getHolder() instanceof GuiHolder) {
                p.closeInventory();
            }
        });
    }
}
