package fr.lightcube.gui;

import fr.lightcube.LCSpigot;
import fr.lightcube.gui.items.BaseMenuItemGui;
import fr.lightcube.gui.items.StaticMenuItemGui;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * Created by Nitorac on 03/06/2017.
 */
public class BaseGui {

    public static final BaseMenuItemGui EMPTY_ITEM = new StaticMenuItemGui(" ", new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.GRAY.getData()));

    protected String name;
    protected Size size;
    protected BaseMenuItemGui[] items;
    protected BaseGui parent;

    public BaseGui(String name, Size size, BaseGui parent) {
        this.name = name;
        this.size = size;
        this.items = new BaseMenuItemGui[size.getSize()];
        this.parent = parent;
    }

    public BaseGui(String name, Size size) {
        this(name, size, null);
    }

    public String getName() {
        return name;
    }

    public Size getSize() {
        return size;
    }

    public boolean hasParent() {
        return parent != null;
    }

    public BaseGui getParent() {
        return parent;
    }

    public BaseGui setParent(BaseGui parent) {
        this.parent = parent;
        return this;
    }

    public BaseGui setItem(int position, BaseMenuItemGui menuItem) {
        items[position] = menuItem;
        return this;
    }

    public BaseMenuItemGui getItem(int position){
        return items[position];
    }

    public BaseGui fillEmptySlots(BaseMenuItemGui menuItem) {
        for (int i = 0; i < items.length; i++) {
            if (items[i] == null) {
                items[i] = menuItem;
            }
        }
        return this;
    }

    public BaseGui fillEmptySlots() {
        return fillEmptySlots(EMPTY_ITEM);
    }

    public void open(Player player) {
        Inventory inventory = LCSpigot.getInstance().getServer().createInventory(new GuiHolder(this, LCSpigot.getInstance().getServer().createInventory(player, size.getSize())), size.getSize(), name);
        apply(inventory, player);
        player.openInventory(inventory);
    }

    @SuppressWarnings("deprecation")
    public void update(Player player) {
        if (player.getOpenInventory() != null) {
            Inventory inventory = player.getOpenInventory().getTopInventory();
            if (inventory.getHolder() instanceof GuiHolder && ((GuiHolder) inventory.getHolder()).getMenu().equals(this)) {
                apply(inventory, player);
                player.updateInventory();
            }
        }
    }

    private void apply(Inventory inventory, Player player) {
        for (int i = 0; i < items.length; i++) {
            if (items[i] != null) {
                inventory.setItem(i, items[i].getFinalIcon(player));
            } else {
                inventory.setItem(i, null);
            }
        }
    }

    @SuppressWarnings("deprecation")
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getClick() == ClickType.LEFT) {
            int slot = event.getRawSlot();
            if (slot >= 0 && slot < size.getSize() && items[slot] != null) {
                Player player = (Player) event.getWhoClicked();
                ItemClickEvent itemClickEvent = new ItemClickEvent(player);
                items[slot].callClick(itemClickEvent);
                if (itemClickEvent.willUpdate()) {
                    update(player);
                } else {
                    player.updateInventory();
                    final UUID playerUuid = player.getUniqueId();
                    LCSpigot.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(LCSpigot.getInstance(), () -> {
                        Player p = LCSpigot.getInstance().getServer().getPlayer(playerUuid);
                        if (p != null) {
                            if (itemClickEvent.willClose() || itemClickEvent.willGoBack()) {
                                p.closeInventory();
                            }
                            if (itemClickEvent.willGoBack() && hasParent()) {
                                parent.open(p);
                            }
                        }
                    }, 1);
                }
            }
        }
    }

    public void destroy() {
        name = null;
        size = null;
        items = null;
        parent = null;
    }

    public enum Size {
        ONE_LINE(9),
        TWO_LINE(18),
        THREE_LINE(27),
        FOUR_LINE(36),
        FIVE_LINE(45),
        SIX_LINE(54);

        private final int size;

        Size(int size) {
            this.size = size;
        }

        public int getSize() {
            return size;
        }

        public static Size fit(int slots) {
            if (slots < 10) {
                return ONE_LINE;
            } else if (slots < 19) {
                return TWO_LINE;
            } else if (slots < 28) {
                return THREE_LINE;
            } else if (slots < 37) {
                return FOUR_LINE;
            } else if (slots < 46) {
                return FIVE_LINE;
            } else {
                return SIX_LINE;
            }
        }
    }
}
