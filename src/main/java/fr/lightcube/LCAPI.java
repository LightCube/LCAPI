package fr.lightcube;

import fr.lightcube.configuration.ConfigManager;
import fr.lightcube.configuration.LCYamlConfiguration;
import fr.lightcube.db.DBManager;
import fr.lightcube.guild.GuildManager;
import fr.lightcube.permissions.PermissionManager;
import fr.lightcube.players.PlayerManager;
import fr.lightcube.rank.RankManager;
import fr.lightcube.redis.RedisManager;
import fr.lightcube.servers.ServerManager;
import fr.lightcube.slack.SlackManager;
import fr.lightcube.utils.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Nitorac on 31/03/2017.
 */
public class LCAPI {

    private static DBManager dbManager;
    private static ConfigManager configManager;
    private static RankManager rankManager;
    private static SlackManager slackManager;
    private static RedisManager redisManager;
    private static PermissionManager permissionManager;
    private static PlayerManager playerManager;
    private static ServerManager serverManager;
    private static GuildManager guildManager;

    private static CommonUtils commonUtils;

    public static final ArrayList<String> blockedCommands = new ArrayList<>(Arrays.asList(
        "reload"
    ));

    public static ArrayList<String> getConfigFilePaths() {
        return new ArrayList<>(Arrays.asList(
            Utils.rootLoc + File.separator + Csts.RANK_CONF,
            Utils.rootLoc + File.separator + Csts.GENERAL_CONF,
            Utils.pluginLoc + File.separator + Csts.WORLD_CONF,
            Utils.rootLoc + File.separator + Csts.PERMS_CONF,
            Utils.serverLoc + File.separator + Csts.WARPS_CONF
        ));
    }

    /********************************** FAIRE ATTENTION A L'ORDRE DES DÉCLARATIONS !*****************************/
    public boolean init(){

        /******** Chargement de la configuration et initialisation des Utils *******/

        //Voir commentaire du constructeur de Utils
        try {
            new Utils();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        configManager = new ConfigManager();

        //Create and load (load caché dans le constructeur de LCYamlConfiguration) configs
        getConfigFilePaths().forEach(path -> LCAPI.getConfigManager().registerConfig(path));

        File bungeeConfigFile = new File(LCAPI.getConfigManager().get(Csts.GENERAL_CONF).getString("bungee.config-file", Utils.rootLoc + File.separator + "bungee" + File.separator + "config.yml"));
        if(!bungeeConfigFile.exists()){
            Log.FATAL.sendLog("Impossible de trouver la configuration Bungee (" + bungeeConfigFile.getAbsolutePath() + ")");
            return false;
        }
        Utils.bungeeConfig = new LCYamlConfiguration(bungeeConfigFile);

        commonUtils = (Csts.isBungee) ? new CommonUtilsBungee() : new CommonUtilsSpigot();


        /************************** Chargement des autres managers ***********************/

        slackManager = new SlackManager();
        dbManager = new DBManager();
        redisManager = new RedisManager();

        serverManager = new ServerManager();
        playerManager = new PlayerManager();
        rankManager = new RankManager();
        permissionManager = new PermissionManager();
        guildManager = new GuildManager();

        getCommonUtils().getServerList().forEach((server, maxPlayers) -> getServerManager().registerServer(server, maxPlayers));
        serverManager.setCurrentServer(Utils.serverLoc.getName());

        //DB
        try {
            LCAPI.getDBManager().init();
            Log.INFO.sendLog("Base de donnee connectee (" + LCAPI.getDBManager().getDbUrl() + ")");
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    ///////////////////
    // Get functions //
    ///////////////////

    public static DBManager getDBManager(){
        return dbManager;
    }

    public static RankManager getRankManager(){
        return rankManager;
    }

    public static ConfigManager getConfigManager(){
        return configManager;
    }

    public static SlackManager getSlackManager(){
        return slackManager;
    }

    public static CommonUtils getCommonUtils(){
        return commonUtils;
    }

    public static RedisManager getRedisManager(){
        return redisManager;
    }

    public static PlayerManager getPlayerManager(){
        return playerManager;
    }

    public static ServerManager getServerManager(){
        return serverManager;
    }

    public static PermissionManager getPermManager(){
        return permissionManager;
    }

    public static GuildManager getGuildManager(){
        return guildManager;
    }
}