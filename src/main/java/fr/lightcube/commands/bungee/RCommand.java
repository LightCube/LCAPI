package fr.lightcube.commands.bungee;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import fr.lightcube.utils.Csts;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created by Nitorac on 02/06/2017.
 */
public class RCommand extends CommandPermissive {

    private static final String[] help = {
        "§f-----------------[§6Light§bMsg §cHelp§f]-----------------",
        "/r [message] : Renvoie un message à la dernière personne qui vous la envoyée.",
        "§f-----------------[§6Light§bMsg §cHelp§f]-----------------"
    };

    public RCommand(String name, String perm){
        super(name, perm, help);
    }

    @Override
    boolean onCommand(CommandSender commandSender, String[] args) {
        if(args.length == 0){
            return false;
        }

        if(!(commandSender instanceof ProxiedPlayer)){
            commandSender.sendMessage(TextComponent.fromLegacyText(Csts.MSG_PREFIX + ChatColor.RED + "Vous devez être un joueur pour répondre !"));
            return true;
        }

        ProxiedPlayer sender = (ProxiedPlayer)commandSender;

        Joiner joiner = Joiner.on(" ").skipNulls();
        if(MsgCommand.lastReceptor.containsKey(sender.getUniqueId())){
            MsgCommand.sendMessage(sender, MsgCommand.lastReceptor.get(sender.getUniqueId()), joiner.join(args));
            return true;
        }else{
            sender.sendMessage(TextComponent.fromLegacyText(Csts.MSG_PREFIX + ChatColor.RED + "Personne ne vous a envoyé de messages récemment."));
            return true;
        }
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
        return Lists.newArrayList();
    }
}
