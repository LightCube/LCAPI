package fr.lightcube.commands.bungee;

import com.google.common.base.Joiner;
import fr.lightcube.LCAPI;
import fr.lightcube.LCBungee;
import fr.lightcube.players.LCPlayer;
import fr.lightcube.redis.RedisManager;
import fr.lightcube.utils.CommonUtilsBungee;
import fr.lightcube.utils.Csts;
import fr.lightcube.utils.Log;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.SQLException;
import java.util.*;

import static fr.lightcube.utils.Csts.LOGERRORBDD;
import static fr.lightcube.utils.Csts.MSGSPY_PREFIX;
import static fr.lightcube.utils.PermCsts.LCAPI_MSGSPY_BLOCK;
import static fr.lightcube.utils.PermCsts.LCAPI_MSG_BYPASS_COOLDOWN;

/**
 * Created by Nitorac on 01/06/2017.
 */
public class MsgCommand extends CommandPermissive{

    private final static Map<UUID, Long> cooldownMap = new HashMap<>();
    public final static Map<UUID, UUID> lastReceptor = new HashMap<>();

    private final static String[] help = {
        "§f-----------------[§6Light§bMsg §cHelp§f]-----------------",
        "/msg [on/off] : Active/désactive vos messages privés.",
        "/msg [joueur] [message] : Envoie un message privé au joueur.",
        "§f-----------------[§6Light§bMsg §cHelp§f]-----------------"
    };

    public MsgCommand(String name, String perm){
        super(name, perm, help);
    }

    @Override
    boolean onCommand(CommandSender cmdSender, String[] args) {
        switch (args.length){
            case 0:
                return false;
            case 1:
                try {
                    switch (args[0].toLowerCase()) {
                        case "on":
                            LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer)cmdSender).setMsgToggle(true);
                            cmdSender.sendMessage(TextComponent.fromLegacyText(Csts.MSG_PREFIX + ChatColor.WHITE + "Vous venez de " + ChatColor.GREEN + "d'activer" + ChatColor.WHITE + " vos messages privés !"));
                            break;
                        case "off":
                            LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer)cmdSender).setMsgToggle(false);
                            cmdSender.sendMessage(TextComponent.fromLegacyText(Csts.MSG_PREFIX + ChatColor.WHITE + "Vous venez de " + ChatColor.RED + "désactiver" + ChatColor.WHITE + " vos messages privés !"));
                            break;
                        default:
                            return false;
                    }
                    return true;
                } catch (SQLException e) {
                    Log.ERROR.sendLog(LOGERRORBDD);
                    e.printStackTrace();
                    cmdSender.sendMessage(TextComponent.fromLegacyText(Csts.MSGERRORPLAYER));
                }
                break;
            default:
                Joiner joiner = Joiner.on(" ").skipNulls();
                ProxiedPlayer receiver = LCBungee.getInstance().getProxy().getPlayer(args[0]);
                if(receiver != null){
                    if (!cmdSender.getName().equals(args[0])) {
                        args[0] = null;
                        sendMessage(cmdSender, receiver.getUniqueId(), joiner.join(args));
                        return true;
                    }else{
                        cmdSender.sendMessage(TextComponent.fromLegacyText(Csts.MSG_PREFIX + ChatColor.RED + "Vous ne pouvez pas vous envoyer de message privé !"));
                        return true;
                    }
                }else{
                    cmdSender.sendMessage(TextComponent.fromLegacyText(Csts.MSG_PREFIX + ChatColor.RED + "Ce joueur n'est pas connecté !"));
                    return true;
                }
        }
        return false;
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] args) {
        ArrayList<String> result = new ArrayList<>();
        String lastArg = args[args.length - 1];

        switch (args.length){
            case 1:
                lastArg = args[0];
                result = CommonUtilsBungee.getOnlinePlayerNames();
        }

        return CommonUtilsBungee.partial(lastArg, result);
    }

    public static boolean sendMessage(CommandSender cmdSender, UUID receiver, String message){
        if(!(cmdSender instanceof ProxiedPlayer)) return false;
        ProxiedPlayer ps = (ProxiedPlayer) cmdSender;
        ProxiedPlayer pr = LCBungee.getInstance().getProxy().getPlayer(receiver);
        LCPlayer lcps = LCAPI.getPlayerManager().getLCPlayer(ps);
        LCPlayer lcpr;
        if(LCAPI.getPermManager().hasPermissionBungee(LCAPI.getPlayerManager().getLCPlayer(ps), LCAPI_MSG_BYPASS_COOLDOWN) || !cooldownMap.containsKey(ps.getUniqueId()) || (System.currentTimeMillis() - cooldownMap.get(ps.getUniqueId())) > (Csts.MSG_COOLDOWN * 1000)){
            if ((lcpr = LCAPI.getPlayerManager().getLCPlayer(pr)) != null){
                if (lcps.getMsgToggle() || LCAPI.getPermManager().hasPermissionBungee(lcps, LCAPI_MSG_BYPASS_COOLDOWN)) {
                    if (lcpr.getMsgToggle()) {
                        ps.sendMessage(TextComponent.fromLegacyText(ChatColor.AQUA + "Envoyé à " + lcpr.getFullPrefix() + " : " + ChatColor.GRAY  + message));
                        pr.sendMessage(TextComponent.fromLegacyText(ChatColor.AQUA + "Reçu de " + lcps.getFullPrefix() + " : " + ChatColor.GRAY  + message));
                        LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.SPIGOT_ONLY, "PlaySound", pr.getServer().getInfo().getName() + Csts.SPLITTER + receiver + Csts.SPLITTER + "random.orb" + Csts.SPLITTER + "1.5F");
                        cooldownMap.put(ps.getUniqueId(), System.currentTimeMillis());
                        lastReceptor.put(lcpr.getUniqueId(), lcps.getUniqueId());

                        MsgSpyCommand.spiers.cellSet().stream().filter(cell -> cell.getColumnKey() != null && !cell.getColumnKey().equals("OFF")).forEach(cell -> {
                            ProxiedPlayer pt = LCBungee.getInstance().getProxy().getPlayer(cell.getRowKey());
                            if(pt == null || cell.getColumnKey() == null || cell.getValue() == null) return;
                            if ((!LCAPI.getPermManager().hasPermissionBungee(lcps, LCAPI_MSGSPY_BLOCK) && !LCAPI.getPermManager().hasPermissionBungee(lcpr, "lcapi.msgspy.block") ) || LCAPI.getPermManager().hasPermissionBungee(LCAPI.getPlayerManager().getLCPlayer(pt), "lcapi.msgspy.bypass")) {
                                switch (cell.getColumnKey()) {
                                    case "PLAYER":
                                        if (cell.getValue().equals(ps.getUniqueId().toString()) || cell.getValue().equals(pr.getUniqueId().toString())) {
                                            pt.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + lcps.getFullPrefix() + " > " + lcpr.getFullPrefix() + " : " + message));
                                        }
                                        break;
                                    case "WORD":
                                        if (message.toLowerCase().contains(cell.getValue().toLowerCase())) {
                                            pt.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + lcps.getFullPrefix() + " > " + lcpr.getFullPrefix() + " : " + message));
                                        }
                                        break;
                                    case "SERVER":
                                        if (cell.getValue().equals(lcpr.getServer().toString()) || cell.getValue().equals(lcps.getServer().toString()) || cell.getValue().equals("GLOBAL")) {
                                            pt.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + lcps.getFullPrefix() + " > " + lcpr.getFullPrefix() + " : " + message));
                                        }
                                        break;
                                    default:
                                }
                            }
                        });
                    } else {
                        cmdSender.sendMessage(TextComponent.fromLegacyText(Csts.MSG_PREFIX + ChatColor.RED + "Ce joueur a désactivé ses messages privés !"));
                    }
                } else {
                    cmdSender.sendMessage(TextComponent.fromLegacyText(Csts.MSG_PREFIX + ChatColor.RED + "Vous devez avoir vos messages privés d'activé pour pouvoir en envoyer aux autres joueurs !"));
                }
            } else {
                cmdSender.sendMessage(TextComponent.fromLegacyText(Csts.MSG_PREFIX + ChatColor.RED + "Ce joueur n'est pas connecté !"));
            }
            return true;
        }else{
            cmdSender.sendMessage(TextComponent.fromLegacyText(Csts.MSG_PREFIX + ChatColor.RED + "Vous devez encore attendre " + String.format(Locale.FRENCH, "%.2f", (Csts.MSG_COOLDOWN - (float)(System.currentTimeMillis() - cooldownMap.get(ps.getUniqueId())) / 1000)) + " secondes avant de renvoyer un message privé."));
            return true;
        }
    }
}
