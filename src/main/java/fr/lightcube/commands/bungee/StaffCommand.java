package fr.lightcube.commands.bungee;

import com.google.common.collect.Lists;
import fr.lightcube.LCAPI;
import fr.lightcube.players.LCPlayer;
import fr.lightcube.utils.CommonUtilsBungee;
import fr.lightcube.utils.Csts;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;

import static fr.lightcube.utils.PermCsts.LCAPI_IS_STAFF;

/**
 * Created by montenvers on 02/06/2017.
 */
public class StaffCommand extends CommandPermissive {

    private static final String[] help = {
            "§f-----------------[§6Light§bMsg §cHelp§f]-----------------",
            "/staff : Affiche les membres du staff acctuellement en ligne.",
            "§f-----------------[§6Light§bMsg §cHelp§f]-----------------"
    };

    public StaffCommand(String name, String perm){
        super(name, perm, help);
    }

    @Override
    boolean onCommand(CommandSender commandSender, String[] args) {
        if (commandSender instanceof ProxiedPlayer) {
            ArrayList<LCPlayer> players = new ArrayList<>();
            for (ProxiedPlayer p : CommonUtilsBungee.getOnlinePlayers()) {
                LCPlayer lcp = LCAPI.getPlayerManager().getLCPlayer(p);
                if (LCAPI.getPermManager().hasPermissionBungee(lcp, LCAPI_IS_STAFF)) {
                    players.add(lcp);
                }
            }
            players.sort((o1, o2) -> (o1.getRank().getRankID() >= o2.getRank().getRankID()) ? -1 : 1);
            if (players.size() > 0) {
                commandSender.sendMessage(TextComponent.fromLegacyText(Csts.STAFF_PREFIX + org.bukkit.ChatColor.WHITE + "Voici les membres du staff actuellement en " + org.bukkit.ChatColor.GREEN + "ligne " + org.bukkit.ChatColor.WHITE + ":"));
                boolean isStaff = LCAPI.getPermManager().hasPermissionBungee(LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) commandSender), LCAPI_IS_STAFF);
                players.forEach(lcPlayer -> commandSender.sendMessage(TextComponent.fromLegacyText(" ◈ " + lcPlayer.getFullPrefix() + ((isStaff) ? " §7(" + lcPlayer.getServer().toString() + ")": ""))));
                commandSender.sendMessage(TextComponent.fromLegacyText(Csts.STAFF_PREFIX + org.bukkit.ChatColor.WHITE + "En cas de question, utilisez le /askstaff [question]. Notre équipe vous répondra au plus vite !"));
            } else {
                commandSender.sendMessage(TextComponent.fromLegacyText(Csts.STAFF_PREFIX + org.bukkit.ChatColor.WHITE + "Aucun membre du staff n'est actuellement " + org.bukkit.ChatColor.RED + "connecté " + org.bukkit.ChatColor.WHITE + "! Veuillez réessayer plus tard !"));
            }
            return true;
        }
        return false;
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
        return Lists.newArrayList();
    }
}
