package fr.lightcube.commands.bungee;

import com.google.common.base.Joiner;
import fr.lightcube.LCAPI;
import fr.lightcube.LCBungee;
import fr.lightcube.players.LCPlayer;
import fr.lightcube.redis.RedisManager;
import fr.lightcube.servers.Server;
import fr.lightcube.utils.*;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bukkit.Bukkit;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static fr.lightcube.utils.Csts.LOGERRORBDD;
import static fr.lightcube.utils.Csts.MSGSPY_PREFIX;

/**
 * Created by Nitorac on 01/06/2017.
 */
public class ModCommand extends CommandPermissive{
    private final static String[] help = {
            "§f-----------------[§6Light§bModération§cHelp§f]-----------------",
            "/mod [on/off] : Active/désactive les commandes de modération.",
            "§f-----------------[§6Light§bModération§cHelp§f]-----------------"
    };

    public ModCommand(String name, String perm){
        super(name, perm, help);
    }

    @Override
    boolean onCommand(CommandSender cmdSender, String[] args) {
        /*ProxiedPlayer p = (ProxiedPlayer) cmdSender;
        switch (args.length){
            case 0:
                return false;
            case 1:
                switch (args[0].toLowerCase()) {
                    case "on":
                        try {
                            LCAPI.getDBManager().update("UPDATE players SET MOD_TOGGLE = 1 WHERE (UUID = ?)", p.getUniqueId().toString());
                            p.sendMessage(TextComponent.fromLegacyText(Csts.MOD_PREFIX + ChatColor.WHITE + "Vous venez d'activer vos outils de modération !"));
                        } catch (SQLException e) {
                            Log.ERROR.sendLog(LOGERRORBDD);
                            e.printStackTrace();
                        }
                        return true;
                    case "off":
                        try {
                            LCAPI.getDBManager().update("UPDATE players SET MOD_TOGGLE = 0 WHERE (UUID = ?)", p.getUniqueId().toString());
                            p.sendMessage(TextComponent.fromLegacyText(Csts.MOD_PREFIX + ChatColor.WHITE + "Vous venez désactiver vos outils de modération !"));
                        } catch (SQLException e) {
                            Log.ERROR.sendLog(LOGERRORBDD);
                            e.printStackTrace();
                        }
                        return true;
                    default:
                        return false;
                }
            case 2:

                return true;
            default:
                return false;
        }*/
        return false;
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender cmdSender, String[] args) {
        ArrayList<String> result = new ArrayList<>();
        String lastArg = args[args.length - 1];

        switch (args.length){
            case 1:
                result.add("on");
                result.add("off");
                lastArg = args[0];
                break;
        }

        return CommonUtilsBungee.partial(lastArg, result);
    }
}
