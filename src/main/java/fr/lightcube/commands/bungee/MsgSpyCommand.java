package fr.lightcube.commands.bungee;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import fr.lightcube.LCAPI;
import fr.lightcube.LCBungee;
import fr.lightcube.utils.CommonUtilsBungee;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.UUID;

import static fr.lightcube.utils.Csts.MSGSPY_PREFIX;

/**
 * Created by Nitorac on 03/06/2017.
 */
public class MsgSpyCommand extends CommandPermissive {

    public static Table<UUID, String, String> spiers = HashBasedTable.create();

    private static String[] help = {
        "§f-----------------[§6Light§bMsgSpy§cHelp§f]-----------------",
        "/msgspy [on/off] : Active/désactive l'espionnage.",
        "/msgspy [on/off] [joueur] : Active/désactive l'espionnage pour le joueur demandé.",
        "/msgspy player [joueur] : Espionne un joueur.",
        "/msgspy server [serveur] : Espionne un seveur.",
        "/msgspy word [mot] : Espionne un mot.",
        "§f-----------------[§6Light§bMsgSpy§cHelp§f]-----------------"
    };

    public MsgSpyCommand(String name, String perm){
        super(name, perm, help);
    }

    @Override
    boolean onCommand(CommandSender cmdSender, String[] args) {
        if(!(cmdSender instanceof ProxiedPlayer)){
            cmdSender.sendMessage(TextComponent.fromLegacyText("§cVous ne pouvez espionner des messages qu'en étant joueur !"));
            return true;
        }

        ProxiedPlayer ps = (ProxiedPlayer)cmdSender;
        switch (args.length) {
            case 0:
                return false;
            case 1:
                switch (args[0].toLowerCase()) {
                    case "on":
                        spiers.put(ps.getUniqueId(), "SERVER", "GLOBAL");
                        ps.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + ChatColor.WHITE + "Vous venez " + ChatColor.GREEN + "d'activer" + ChatColor.WHITE + " le msgspy !"));
                        return true;
                    case "off":
                        spiers.put(ps.getUniqueId(), "OFF", "");
                        ps.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + ChatColor.WHITE + "Vous venez de " + ChatColor.RED + "désactiver" + ChatColor.WHITE + " le msgspy !"));
                        return true;
                    case "global":
                        spiers.put(ps.getUniqueId(), "SERVER", "GLOBAL");
                        ps.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + ChatColor.WHITE + "Vous surveillez le serveur : " + ChatColor.GRAY + "GLOBAL" + ChatColor.WHITE + " !"));
                        return true;
                    default:
                        return false;
                }
            case 2:
                switch(args[0].toLowerCase()) {
                    case "joueur":
                    case "player":
                        if (args[1].length()!=0) {
                            ProxiedPlayer pr = LCBungee.getInstance().getProxy().getPlayer(args[1]);
                            if (pr != null) {
                                spiers.put(ps.getUniqueId(), "PLAYER", pr.getUniqueId().toString());
                                ps.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + ChatColor.WHITE + "Vous surveillez le joueur : " + ChatColor.GRAY + LCAPI.getPlayerManager().getLCPlayer(pr).getFullPrefix() + ChatColor.WHITE + " !"));
                            } else {
                                ps.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + ChatColor.RED + "Le joueur spécifié n'est pas reconnu !"));
                            }
                            return true;
                        } else {
                            return false;
                        }
                    case "serveur":
                    case "server":
                        if (args[1].length()==0) {
                            spiers.put(ps.getUniqueId(), "SERVER", ps.getServer().toString());
                            ps.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + ChatColor.WHITE + "Vous surveillez le serveur : " + ChatColor.GRAY + "GLOBAL" + ChatColor.WHITE + " !"));
                        } else {
                            spiers.put(ps.getUniqueId(), "SERVER", args[1]);
                            ps.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + ChatColor.WHITE + "Vous surveillez le serveur : " + ChatColor.GRAY + args[1] + ChatColor.WHITE + " !"));
                        }
                        return true;
                    case "mot":
                    case "word":
                        if (args[1].length() != 0) {
                            spiers.put(ps.getUniqueId(), "WORD", args[1]);
                            ps.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + ChatColor.WHITE + "Vous surveillez le mot : " + ChatColor.GRAY + args[1] + ChatColor.WHITE + " !"));
                            return true;
                        } else {
                            return false;
                        }
                    case "on":
                        UUID victim = CommonUtilsBungee.resolvePlayer(cmdSender, args[1]);
                        if(victim != null){
                            spiers.put(victim, "SERVER", "GLOBAL");
                            ps.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + ChatColor.WHITE + "Vous avez " + ChatColor.GREEN + "activé" + ChatColor.WHITE + " le message spy pour le joueur " + args[1]));
                        }else{
                            ps.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + ChatColor.RED + "Le joueur spécifié n'est pas reconnu !"));
                        }
                        return true;
                    case "off":
                        UUID victim2 = CommonUtilsBungee.resolvePlayer(cmdSender, args[1]);
                        if(victim2 != null){
                            if(spiers.containsRow(victim2) && !spiers.rowMap().get(victim2).containsKey("OFF")){
                                spiers.put(victim2, "OFF", "");
                                ps.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + ChatColor.WHITE + "Vous avez " + ChatColor.RED + "desactivé" + ChatColor.WHITE + " le message spy pour le joueur " + args[1]));
                            }else{
                                ps.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + ChatColor.WHITE + "Le joueur n'a pas son message spy activé !"));
                            }
                        }else{
                            ps.sendMessage(TextComponent.fromLegacyText(MSGSPY_PREFIX + ChatColor.RED + "Le joueur spécifié n'est pas reconnu !"));
                        }
                        return true;
                    default:
                        return false;
                }
            default:
                return false;
        }
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] args) {
        final ArrayList<String> result = new ArrayList<>();
        String lastArg = args[args.length - 1];
        switch (args.length){
            case 1:
                lastArg = args[0];
                result.add("on");
                result.add("off");
                result.add("global");
                result.add("player");
                result.add("server");
                result.add("word");
                break;
            case 2:
                switch (args[0].toLowerCase()) {
                    case "player":
                    case "joueur":
                        CommonUtilsBungee.getOnlinePlayerNames();
                        break;
                    case "server":
                    case "serveur":
                        LCAPI.getServerManager().getAllServers().forEach(p -> result.add(p.toString()));
                        result.add("GLOBAL");
                        break;
                    case "word":
                    case "mot":
                        break;
                    case "on":
                    case "off":
                        CommonUtilsBungee.getOnlinePlayerNames();
                    default:
                }
        }
        return CommonUtilsBungee.partial(lastArg, result);
    }
}
