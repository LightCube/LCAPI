package fr.lightcube.commands.bungee;

import com.google.common.collect.Lists;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import static fr.lightcube.utils.Csts.INFO_PREFIX;

/**
 * Created by montenvers on 20/06/2017.
 */
public class InfoCommand extends CommandPermissive {

    private static final String[] help = {
            "§f-----------------[§6Light§bInfo§cHelp§f]-----------------",
            "/info : Renvoie les informations importantes du joueur.",
            "§f-----------------[§6Light§bInfo§cHelp§f]-----------------"
    };

    public InfoCommand(String name, String perm){
        super(name, perm, help);
    }

    @Override
    boolean onCommand(CommandSender commandSender, String[] args) {
        ProxiedPlayer p = (ProxiedPlayer) commandSender;
        p.sendMessage(TextComponent.fromLegacyText(INFO_PREFIX + ChatColor.WHITE + "Voici les liens importants du serveur :"));
        //Twitter
        TextComponent twitterbase = new TextComponent("   ◈ ");
        TextComponent twitter1 = new TextComponent("Twitter : ");
        TextComponent twitter2 = new TextComponent("@LightCube_Fr");
        twitterbase.setBold(true);
        twitter1.setBold(false);
        twitter2.setBold(false);
        twitterbase.setColor(ChatColor.GRAY);
        twitter1.setColor(ChatColor.GOLD);
        twitter2.setColor(ChatColor.AQUA);
        twitter2.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://twitter.com/Lightcube_fr"));
        twitter2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Cliquez pour ouvrir le lien.").create()));
        twitterbase.addExtra(twitter1);
        twitterbase.addExtra(twitter2);
        //Forum
        TextComponent forumbbase = new TextComponent("   ◈ ");
        TextComponent forum1 = new TextComponent("Forum : ");
        TextComponent forum2 = new TextComponent("forum.light-cube.fr");
        forumbbase.setBold(true);
        forum1.setBold(false);
        forum2.setBold(false);
        twitterbase.setColor(ChatColor.GRAY);
        forum1.setColor(ChatColor.GOLD);
        forum2.setColor(ChatColor.AQUA);
        forum2.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://forum.light-cube.fr/"));
        forum2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Cliquez pour ouvrir le lien.").create()));
        forumbbase.setColor(ChatColor.GRAY);
        forumbbase.addExtra(forum1);
        forumbbase.addExtra(forum2);
        //TeamSpeak
        TextComponent teamspeakbase = new TextComponent("   ◈ ");
        TextComponent teamspeak1 = new TextComponent("TeamSpeak3 : ");
        TextComponent teamspeak2 = new TextComponent("ts.light-cube.fr");
        teamspeakbase.setBold(true);
        teamspeak1.setBold(false);
        teamspeak2.setBold(false);
        twitterbase.setColor(ChatColor.GRAY);
        teamspeak1.setColor(ChatColor.GOLD);
        teamspeak2.setColor(ChatColor.AQUA);
        teamspeak2.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "ts3server://ts.light-cube.fr?&username="+p.getName()));
        teamspeak2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Cliquez pour ouvrir le lien.").create()));
        teamspeakbase.setColor(ChatColor.GRAY);
        teamspeakbase.addExtra(teamspeak1);
        teamspeakbase.addExtra(teamspeak2);
        //Site
        TextComponent sitebase = new TextComponent("   ◈ ");
        TextComponent site1 = new TextComponent("Site : ");
        TextComponent site2 = new TextComponent("light-cube.fr");
        sitebase.setBold(true);
        site1.setBold(false);
        site2.setBold(false);
        twitterbase.setColor(ChatColor.GRAY);
        site1.setColor(ChatColor.GOLD);
        site2.setColor(ChatColor.AQUA);
        site2.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://light-cube.fr/"));
        site2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Cliquez pour ouvrir le lien.").create()));
        sitebase.setColor(ChatColor.GRAY);
        sitebase.addExtra(site1);
        sitebase.addExtra(site2);
        //Send
        p.sendMessage(sitebase);
        p.sendMessage(forumbbase);
        p.sendMessage(teamspeakbase);
        p.sendMessage(twitterbase);
        return true;
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
        return Lists.newArrayList();
    }
}
