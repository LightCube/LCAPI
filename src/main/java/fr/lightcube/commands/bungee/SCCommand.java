package fr.lightcube.commands.bungee;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import fr.lightcube.LCAPI;
import fr.lightcube.LCSpigot;
import fr.lightcube.players.LCPlayer;
import fr.lightcube.utils.CommonUtilsBungee;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bukkit.ChatColor;

import java.io.IOException;
import java.util.UUID;

import static fr.lightcube.utils.PermCsts.LCAPI_COLORED_CHAT;
import static fr.lightcube.utils.PermCsts.LCAPI_IS_STAFF;

/**
 * Created by montenvers on 20/06/2017.
 */
public class SCCommand extends CommandPermissive {

    private static final String[] help = {
            "§f-----------------[§6Light§bStaff§cHelp§f]-----------------",
            "/sc : Envoie un message dans le staff chat.",
            "§f-----------------[§6Light§bStaff§cHelp§f]-----------------"
    };

    public SCCommand(String name, String perm){
        super(name, perm, help);
    }

    @Override
    boolean onCommand(CommandSender commandSender, String[] args) {
        if(args.length < 1){
            return false;
        }
        LCPlayer lcp;
        Joiner joiner = Joiner.on(" ").skipNulls();
        String chatDisplay = ChatColor.translateAlternateColorCodes('&', LCAPI.getRankManager().getChatDisplay());
        String formattedPseudo;
        String rawmsg = joiner.join(args);
        if(commandSender instanceof ProxiedPlayer){
            ProxiedPlayer p = (ProxiedPlayer) commandSender;
            lcp = LCAPI.getPlayerManager().getLCPlayer(p);
            formattedPseudo = lcp.getRank().getColor() + lcp.getName() + " §7(" + LCAPI.getPlayerManager().getLCPlayer(p).getServer()+ ")§r";
            rawmsg = (p.hasPermission(LCAPI_COLORED_CHAT)) ? ChatColor.translateAlternateColorCodes('&', rawmsg) : rawmsg;
        }else{
            formattedPseudo = commandSender.getName();
            lcp = new LCPlayer(commandSender.getName(), UUID.randomUUID());
        }
        String msg = chatDisplay.replace("%pseudo%", formattedPseudo).replace("%msg%", rawmsg);
        CommonUtilsBungee.sendToPlayerPermFilter(msg, LCAPI_IS_STAFF);
        try {
            if(!msg.equals((msg = msg.replace("@everyone", "@here").replace("@channel", "@here")))){
                commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "ATTENTION : @everyone ou @channel ne sont pas supportés par les robots."));
            }
            LCAPI.getSlackManager().sendPlayerMessage("staff-chat", rawmsg, lcp, false);
        } catch (IOException e) {
            commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Impossible d'envoyer le message sur Slack, veuillez en informer un développeur."));
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
        return Lists.newArrayList();
    }
}
