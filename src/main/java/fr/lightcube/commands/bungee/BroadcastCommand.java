package fr.lightcube.commands.bungee;

import fr.lightcube.LCAPI;
import fr.lightcube.utils.CommonUtilsBungee;
import fr.lightcube.utils.Log;
import fr.lightcube.utils.Utils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static fr.lightcube.utils.Csts.LOGERRORBDD;

/**
 * Created by Nitorac on 01/06/2017.
 */
public class BroadcastCommand extends CommandPermissive{
    private final static String[] help = {
            "§f-----------------[§6Light§bBroadcast§cHelp§f]-----------------",
            "/br [msg]: Envoie une annonce a tous les joueurs.",
            "/br [all/server] [msg] : Envoie une annonce a tous les joueurs.",
            "§f-----------------[§6Light§bBroadcast§cHelp§f]-----------------"
    };

    public BroadcastCommand(String name, String perm){
        super(name, perm, help);
    }

    @Override
    boolean onCommand(CommandSender cmdSender, String[] args) {
        switch (args.length){
            case 0:
                return false;
            case 1:
                if (LCAPI.getPermManager().hasPermissionBungee(LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender), "lcapi.br.all")) {
                    return false;
                } else {
                    try {
                        ResultSet rs = LCAPI.getDBManager().getResultPreStat("SELECT * FROM players WHERE UUID = ? AND MOD_TOGGLE = 1", ((ProxiedPlayer) cmdSender).getUniqueId().toString());
                        if (rs.next()) {
                            CommonUtilsBungee.getOnlinePlayers().stream().filter(ProxiedPlayer -> LCAPI.getPlayerManager().getLCPlayer(ProxiedPlayer).getServer().equals(LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender).getServer())).forEach(ProxiedPlayer -> ProxiedPlayer.sendMessage(TextComponent.fromLegacyText(ChatColor.BOLD + "§6[MODERATION] " + LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender).getFullPrefix() + ChatColor.AQUA + ChatColor.BOLD + " : " + args[0])));
                        } else {
                            CommonUtilsBungee.getOnlinePlayers().stream().filter(ProxiedPlayer -> LCAPI.getPlayerManager().getLCPlayer(ProxiedPlayer).getServer().equals(LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender).getServer())).forEach(ProxiedPlayer -> ProxiedPlayer.sendMessage(TextComponent.fromLegacyText(ChatColor.BOLD + "§6[ANNONCE] " + LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender).getFullPrefix() + ChatColor.AQUA + ChatColor.BOLD + " : " + args[0])));
                        }
                    } catch (SQLException e) {
                        Log.ERROR.sendLog(LOGERRORBDD);
                        e.printStackTrace();
                    }
                    return true;
                }
            case 2:
                if (LCAPI.getPermManager().hasPermissionBungee(LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender), "lcapi.br.all")) {
                    switch (args[0].toLowerCase()) {
                        case "all":
                            try {
                                ResultSet rs = LCAPI.getDBManager().getResultPreStat("SELECT * FROM players WHERE UUID = ? AND MOD_TOGGLE = 1", ((ProxiedPlayer) cmdSender).getUniqueId().toString());
                                if (rs.next()) {
                                    CommonUtilsBungee.getOnlinePlayers().stream().filter(ProxiedPlayer -> LCAPI.getPlayerManager().getLCPlayer(ProxiedPlayer).getServer().equals(LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender).getServer())).forEach(ProxiedPlayer -> ProxiedPlayer.sendMessage(TextComponent.fromLegacyText(ChatColor.BOLD + "§6[MODERATION] " + LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender).getFullPrefix() + ChatColor.AQUA + ChatColor.BOLD + " : " + args[1])));
                                } else {
                                    CommonUtilsBungee.getOnlinePlayers().stream().filter(ProxiedPlayer -> LCAPI.getPlayerManager().getLCPlayer(ProxiedPlayer).getServer().equals(LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender).getServer())).forEach(ProxiedPlayer -> ProxiedPlayer.sendMessage(TextComponent.fromLegacyText(ChatColor.BOLD + "§6[ANNONCE] " + LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender).getFullPrefix() + ChatColor.AQUA + ChatColor.BOLD + " : " + args[1])));
                                }
                            } catch (SQLException e) {
                                Log.ERROR.sendLog(LOGERRORBDD);
                                e.printStackTrace();
                            }
                            return true;
                        case "server":
                            try {
                                ResultSet rs = LCAPI.getDBManager().getResultPreStat("SELECT * FROM players WHERE UUID = ? AND MOD_TOGGLE = 1", ((ProxiedPlayer) cmdSender).getUniqueId().toString());
                                if (rs.next()) {
                                    CommonUtilsBungee.getOnlinePlayers().forEach(ProxiedPlayer -> ProxiedPlayer.sendMessage(TextComponent.fromLegacyText(ChatColor.BOLD + "§6[MODERATION] " + LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender).getFullPrefix() + ChatColor.AQUA + ChatColor.BOLD + " : " + args[1])));
                                } else {
                                    CommonUtilsBungee.getOnlinePlayers().forEach(ProxiedPlayer -> ProxiedPlayer.sendMessage(TextComponent.fromLegacyText(ChatColor.BOLD + "§6[ANNONCE] " + LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender).getFullPrefix() + ChatColor.AQUA + ChatColor.BOLD + " : " + args[1])));
                                }
                            } catch (SQLException e) {
                                Log.ERROR.sendLog(LOGERRORBDD);
                                e.printStackTrace();
                            }
                            return true;
                        default:
                            return false;
                    }
                }
                break;
            case 3:
                if (LCAPI.getPermManager().hasPermissionBungee(LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender), "lcapi.br.all")) {
                    switch (args[0].toLowerCase()) {
                        case "server":
                            CommonUtilsBungee.getOnlinePlayers().stream().filter(proxiedPlayer -> LCAPI.getPlayerManager().getLCPlayer(proxiedPlayer).getServer().equals(LCAPI.getServerManager().getServerSilent(args[1]))).forEach(proxiedPlayer -> proxiedPlayer.sendMessage(TextComponent.fromLegacyText(ChatColor.BOLD + "§6[ANNONCE] " + LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender).getFullPrefix() + ChatColor.AQUA + ChatColor.BOLD + " : " + args[2])));
                            return true;
                        default:
                            return false;
                    }
                }
                return true;
            default:
                return false;
        }
        return false;
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender cmdSender, String[] args) {
        ArrayList<String> result = new ArrayList<>();
        String lastArg = args[args.length - 1];

        switch (args.length){
            case 1:
                if (LCAPI.getPermManager().hasPermissionBungee(LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender), "lcapi.br.all")) {
                    result.add("all");
                    result.add("server");
                }
                lastArg = args[0];
                break;
            case 2:
                if (LCAPI.getPermManager().hasPermissionBungee(LCAPI.getPlayerManager().getLCPlayer((ProxiedPlayer) cmdSender), "lcapi.br.all")) {
                    result = Utils.bungeeConfig.getSection("servers", false);
                }
                lastArg = args[1];
        }

        return CommonUtilsBungee.partial(lastArg, result);
    }
}
