package fr.lightcube.commands.bungee;

import fr.lightcube.LCAPI;
import fr.lightcube.permissions.PermissionManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import java.util.Arrays;

/**
 * Created by Nitorac on 31/05/2017.
 */
public abstract class CommandPermissive extends Command implements TabExecutor {

    private final String basePermission;
    private final String[] help;

    public CommandPermissive(String name, String permission, String[] help) {
        super(name);
        this.basePermission = permission;
        this.help = help;
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if(commandSender instanceof ProxiedPlayer && !LCAPI.getPermManager().hasPermissionBungee(LCAPI.getPlayerManager().getLCPlayer(((ProxiedPlayer)commandSender)), basePermission)){
            commandSender.sendMessage(new TextComponent(PermissionManager.HASNT_PERMS_MESSAGE));
            return;
        }
        if(!onCommand(commandSender, args)){
            Arrays.stream(help).forEachOrdered(line -> commandSender.sendMessage(TextComponent.fromLegacyText(line)));
        }
    }

    abstract boolean onCommand(CommandSender cmdSender, String[] args);
}
