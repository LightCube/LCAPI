package fr.lightcube.commands.bungee;

import fr.lightcube.LCAPI;
import fr.lightcube.LCBungee;
import fr.lightcube.rank.Rank;
import fr.lightcube.redis.RedisManager;
import fr.lightcube.utils.CommonUtilsBungee;
import fr.lightcube.utils.Csts;
import fr.lightcube.utils.Log;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by montenvers on 21/06/2017.
 */
public class SetRankCommand extends CommandPermissive {

    private static final String[] help = {
            "§f-----------------[§6Light§bRank §cHelp§f]-----------------",
            "/setrank [joueur] [rank] : Défini le rang d'un joueur.",
            "§f-----------------[§6Light§bRank §cHelp§f]-----------------"
    };

    public SetRankCommand(String name, String perm){
        super(name, perm, help);
    }

    @Override
    boolean onCommand(CommandSender commandSender, String[] args) {
        if (args.length == 2){
            UUID victimUuid = CommonUtilsBungee.resolvePlayer(commandSender, args[0]);
            if(victimUuid == null) return true;

            ProxiedPlayer victim = ProxyServer.getInstance().getPlayer(victimUuid);

            if (victim != null) {
                Rank rank = LCAPI.getRankManager().getRankFromName(args[1]);
                if(rank == null){
                    commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Impossible de trouver le rang demandé !"));
                    return true;
                }

                int rankId = 0;

                if(commandSender instanceof ProxiedPlayer){
                    rankId = LCAPI.getPlayerManager().getLCPlayer(((ProxiedPlayer)commandSender)).getRank().getRankID();
                }else if(commandSender.equals(LCBungee.getInstance().getProxy().getConsole())){
                    rankId = Integer.MAX_VALUE;
                }

                if(commandSender instanceof ProxiedPlayer && victimUuid.compareTo(((ProxiedPlayer) commandSender).getUniqueId()) == 0){
                    commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Vous ne pouvez définir votre propre rang !"));
                    return true;
                }

                if(LCAPI.getPlayerManager().getLCPlayer(victim).getRank().getRankID() >= rankId){
                    commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Vous ne pouvez pas modifier le rang d'un joueur ayant un rang supérieur ou égal au votre."));
                    return true;
                }

                if(Math.abs(rankId/10) <= Math.abs(rank.getRankID())/10){
                    commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Vous ne pouvez pas grader le joueur " + ChatColor.WHITE
                            + victim.getName() + ChatColor.RED + " au dessus de ou à votre propre rang !"));
                    return true;
                }

                try {
                    LCAPI.getDBManager().update("UPDATE players SET RANK = ? WHERE UUID = ?", rank.getRankID(), victim.getUniqueId().toString());
                    victim.sendMessage(TextComponent.fromLegacyText(ChatColor.GREEN + "Votre nouveau rang est "+ rank.getName()));
                    commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.GREEN + "Le nouveau rang de " + ChatColor.WHITE + victim.getName() + ChatColor.GREEN + " est " + ChatColor.WHITE + rank.getName() + ChatColor.GREEN + "."));
                    LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BOTH, "LCReload", "Player " + victim.getUniqueId().toString());
                    if (LCAPI.getPlayerManager().getLCPlayer(victim).getServer().getGroup().toLowerCase().equals("lobby")) {
                        LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.SPIGOT_ONLY, "LCHUBUpdate", LCAPI.getPlayerManager().getLCPlayer(victim).getServer().toString() + Csts.SPLITTER + victim.getUniqueId().toString() + Csts.SPLITTER + "SCOREBOARD/TAB" + Csts.SPLITTER + "RANK" + Csts.SPLITTER + rank.getRankID());
                    }
                }catch (Exception e){
                    Log.ERROR.sendLog("La commande setrank n'as pas fonctionnée pour le joueur "+ args[0] + " : " + e.getLocalizedMessage());
                }
            }else {
                commandSender.sendMessage(TextComponent.fromLegacyText("§cImpossible de trouver le joueur §f" + args[0]));
            }
            return true;
        }

        return false;
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] args) {
        ArrayList<String> result = new ArrayList<>();
        String lastArg = args[args.length - 1];

        switch (args.length){
            case 1:
                lastArg = args[0];
                result = CommonUtilsBungee.getOnlinePlayerNames();
                break;
            case 2:
                result = LCAPI.getRankManager().getRanksNames();
                break;
        }

        return CommonUtilsBungee.partial(lastArg, result);
    }
}
