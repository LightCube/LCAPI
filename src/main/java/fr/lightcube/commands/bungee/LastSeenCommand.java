package fr.lightcube.commands.bungee;

import fr.lightcube.LCAPI;
import fr.lightcube.servers.Server;
import fr.lightcube.utils.CommonUtilsBungee;
import fr.lightcube.utils.Log;
import fr.lightcube.utils.Utils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Nitorac on 01/06/2017.
 */
public class LastSeenCommand extends CommandPermissive{
    private final static String[] help = {
            "§f-----------------[§6Light§bCube§cHelp§f]-----------------",
            "/lastseen [joueur] : Donne la dernière date de connexion du joueur.",
            "§f-----------------[§6Light§bCube§cHelp§f]-----------------"
    };

    public LastSeenCommand(String name, String perm){
        super(name, perm, help);
    }

    @Override
    boolean onCommand(CommandSender cmdSender, String[] args) {
        if (args.length == 1) {
            UUID victimUuid = Utils.fromString(args[0]);
            ResultSet rows;
            try {
                if(victimUuid != null){
                    rows = LCAPI.getDBManager().getResultPreStat("SELECT * FROM players WHERE UUID = ?", victimUuid.toString());
                    if(!rows.next()){
                        cmdSender.sendMessage(TextComponent.fromLegacyText("§cImpossible de trouver le UUID dans la base de données."));
                        return true;
                    }
                }else{
                    rows = LCAPI.getDBManager().getResultPreStat("SELECT * FROM players WHERE NAME = ?", args[0]);
                    if(!rows.next()){
                        cmdSender.sendMessage(TextComponent.fromLegacyText("§cImpossible de trouver le joueur dans la base de données."));
                        return true;
                    }
                }
                if(rows.getBoolean("IS_ONLINE")){
                    cmdSender.sendMessage(TextComponent.fromLegacyText(org.bukkit.ChatColor.GREEN + "Le joueur " + org.bukkit.ChatColor.WHITE + rows.getString("NAME") +
                            org.bukkit.ChatColor.GREEN + " est en ligne et connecté sur le serveur " + org.bukkit.ChatColor.WHITE +
                            new Server(rows.getString("CUR_SERVER")).toString()));
                }else{
                    DateFormat format = new SimpleDateFormat("dd/MM/yyyy' " + org.bukkit.ChatColor.GREEN + "à" + org.bukkit.ChatColor.WHITE + " 'HH:mm");
                    cmdSender.sendMessage(TextComponent.fromLegacyText(org.bukkit.ChatColor.GREEN + "Le joueur " + org.bukkit.ChatColor.WHITE + rows.getString("NAME") +
                            org.bukkit.ChatColor.GREEN + " s'est connecté pour la dernière fois le " + org.bukkit.ChatColor.WHITE + format.format(new Date(rows.getLong("LAST_LOGIN"))) +
                            org.bukkit.ChatColor.GREEN + " sur le serveur "  + org.bukkit.ChatColor.WHITE + new Server(rows.getString("CUR_SERVER")).toString()));
                }
                return true;
            } catch (Exception e) {
                Log.ERROR.sendLog("Impossible d'exectuer la commande LastSeen (" + args[0] + ")");
                e.printStackTrace();
                cmdSender.sendMessage(TextComponent.fromLegacyText("§cUne erreur s'est produite !"));
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender cmdSender, String[] args) {
        ArrayList<String> result = new ArrayList<>();
        String lastArg = args[args.length - 1];
        switch (args.length){
            case 1:
                result = CommonUtilsBungee.getOnlinePlayerNames();
                lastArg = args[0];
                break;
        }
        return CommonUtilsBungee.partial(lastArg, result);
    }
}
