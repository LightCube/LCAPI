package fr.lightcube.commands.bungee;

import fr.lightcube.LCAPI;
import fr.lightcube.redis.RedisManager;
import fr.lightcube.utils.CommonUtilsBungee;
import fr.lightcube.utils.Csts;
import fr.lightcube.utils.Log;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bukkit.ChatColor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import static fr.lightcube.utils.Csts.*;

/**
 * Created by montenvers on 20/06/2017.
 */
public class LightCoinsCommand extends CommandPermissive {

    private static final String[] help = {
            "§f-----------------[§6Light§bCoins§cHelp§f]-----------------",
            "/lc get [joueur] : Affiche le nombre de coins d'un joueur.",
            "/lc add [joueur] [coins] : Ajoute un nombre de coins à un joueur.",
            "/lc remove [joueur] [coins] : Enlève un nombre de coins à un joueur.",
            "/lc set [joueur] [coins] : Défini le nombre de coins d'un joueur.",
            "§f-----------------[§6Light§bCoins§cHelp§f]-----------------"
    };

    public LightCoinsCommand(String name, String perm){
        super(name, perm, help);
    }

    @Override
    boolean onCommand(CommandSender commandSender, String[] args) {
        if (commandSender instanceof ProxiedPlayer) {
            ProxiedPlayer send = (ProxiedPlayer) commandSender;
            if (args.length == 0 || args.length == 1) {
                return false;
            }
            try {
                ProxiedPlayer player;
                if (args[1].length() == 36) { player = ProxyServer.getInstance().getPlayer(UUID.fromString(args[1])); } else { player = ProxyServer.getInstance().getPlayer(args[1]); }
                ResultSet rs = LCAPI.getDBManager().getResultPreStat("SELECT * FROM players WHERE UUID = ?", player.getUniqueId().toString());
                if (args.length == 2) {
                    switch (args[0].toLowerCase()) {
                        case "get":
                            if (rs.next()) {
                                send.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + LCAPI.getPlayerManager().getLCPlayer(player).getFullPrefix() + ChatColor.WHITE + " a " + rs.getInt("LIGHT_COINS") + " LightCoins !"));
                            } else {
                                send.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.RED + "Le joueur n'existe pas dans la BDD !"));
                            }
                            return true;
                    }
                } else if (args.length == 3) {
                    switch (args[0].toLowerCase()) {
                        case "add":
                            if (rs.next()) {
                                int pcoins = Integer.parseInt(args[2]) + rs.getInt("LIGHT_COINS");
                                if (Integer.parseInt(args[2]) > 0 && pcoins >= 0) {
                                    LCAPI.getDBManager().update("UPDATE players SET LIGHT_COINS = ? WHERE (UUID = ?)", pcoins, player.getUniqueId().toString());
                                    send.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.WHITE + "Vous venez d'envoyer " + Integer.parseInt(args[2]) + " LightCoins à " + LCAPI.getPlayerManager().getLCPlayer(player).getFullPrefix() + " !"));
                                    player.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.WHITE + "Vous avez obtenu " + args[2] + " LightCoins "));
                                    if (LCAPI.getPlayerManager().getLCPlayer(player).getServer().getGroup().toLowerCase().equals("lobby")) {
                                        LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.SPIGOT_ONLY, "LCHUBUpdate", LCAPI.getPlayerManager().getLCPlayer(player).getServer().toString() + Csts.SPLITTER + player.getUniqueId().toString() + Csts.SPLITTER + "SCOREBOARD" + Csts.SPLITTER + "LIGHTCOINS" + Csts.SPLITTER + args[2]);
                                    }
                                    return true;
                                } else if (Integer.parseInt(args[2]) < 0 && pcoins >= 0) {
                                    LCAPI.getDBManager().update("UPDATE players SET LIGHT_COINS = ? WHERE (UUID = ?)", pcoins, player.getUniqueId().toString());
                                    send.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.WHITE + "Vous venez d'enlever " + -(Integer.parseInt(args[2])) + " LightCoins à " + LCAPI.getPlayerManager().getLCPlayer(player).getFullPrefix() + " !"));
                                    player.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.WHITE + "Vous avez perdu " + -(Integer.parseInt(args[2])) + " LightCoins !"));
                                    if (LCAPI.getPlayerManager().getLCPlayer(player).getServer().getGroup().toLowerCase().equals("lobby")) {
                                        LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.SPIGOT_ONLY, "LCHUBUpdate", LCAPI.getPlayerManager().getLCPlayer(player).getServer().toString() + Csts.SPLITTER + player.getUniqueId().toString() + Csts.SPLITTER + "SCOREBOARD" + Csts.SPLITTER + "LIGHTCOINS" + Csts.SPLITTER + args[2]);
                                    }
                                    return true;
                                } else if (pcoins >= 0){
                                    send.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.RED + "Vous ne pouvez pas envoyer 0 LightCoin !"));
                                    return true;
                                } else {
                                    send.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.RED + "Le joueur ne peux pas avoir un nombre de LightCoins négatif !"));
                                    return true;
                                }
                            } else {
                                send.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.RED + "Le joueur n'existe pas dans la BDD !"));
                                return true;
                            }
                        case "remove":
                            if (rs.next()) {
                                int mcoins = rs.getInt("LIGHT_COINS") - Integer.parseInt(args[2]);
                                if (Integer.parseInt(args[2]) > 0 && mcoins >= 0) {
                                    LCAPI.getDBManager().update("UPDATE players SET LIGHT_COINS = ? WHERE (UUID = ?)", mcoins, player.getUniqueId().toString());
                                    send.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.WHITE + "Vous venez d'enlever " + Integer.parseInt(args[2]) + " LightCoins à " + LCAPI.getPlayerManager().getLCPlayer(player).getFullPrefix() + " !"));
                                    player.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.WHITE + "Vous avez perdu " + args[2] + " LightCoins "));
                                    if (LCAPI.getPlayerManager().getLCPlayer(player).getServer().getGroup().toLowerCase().equals("lobby")) {
                                        LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.SPIGOT_ONLY, "LCHUBUpdate", LCAPI.getPlayerManager().getLCPlayer(player).getServer().toString() + Csts.SPLITTER + player.getUniqueId().toString() + Csts.SPLITTER + "SCOREBOARD" + Csts.SPLITTER + "LIGHTCOINS" + Csts.SPLITTER + args[2]);
                                    }
                                    return true;
                                } else if (Integer.parseInt(args[2]) < 0 && mcoins >= 0) {
                                    LCAPI.getDBManager().update("UPDATE players SET LIGHT_COINS = ? WHERE (UUID = ?)", mcoins, player.getUniqueId().toString());
                                    send.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.WHITE + "Vous venez d'envoyer " + -(Integer.parseInt(args[2])) + " LightCoins à " + LCAPI.getPlayerManager().getLCPlayer(player).getFullPrefix() + " !"));
                                    player.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.WHITE + "Vous avez obtenu " + -(Integer.parseInt(args[2])) + " LightCoins !"));
                                    if (LCAPI.getPlayerManager().getLCPlayer(player).getServer().getGroup().toLowerCase().equals("lobby")) {
                                        LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.SPIGOT_ONLY, "LCHUBUpdate", LCAPI.getPlayerManager().getLCPlayer(player).getServer().toString() + Csts.SPLITTER + player.getUniqueId().toString() + Csts.SPLITTER + "SCOREBOARD" + Csts.SPLITTER + "LIGHTCOINS" + Csts.SPLITTER + args[2]);
                                    }
                                    return true;
                                } else if (mcoins >= 0){
                                    send.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.RED + "Vous ne pouvez pas envoyer 0 LightCoin !"));
                                    return true;
                                } else {
                                    send.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.RED + "Le joueur ne peux pas avoir un nombre de LightCoins négatif !"));
                                    return true;
                                }
                            } else {
                                send.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.RED + "Le joueur n'existe pas dans la BDD !"));
                                return true;
                            }
                        case "set":
                            if (rs.next() && Integer.parseInt(args[2])>=0) {
                                LCAPI.getDBManager().update("UPDATE players SET LIGHT_COINS = ? WHERE (UUID = ?)", Integer.parseInt(args[2]), player.getUniqueId().toString());
                                send.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.WHITE + "Vous venez de définir le nombre de LightCoins de " + LCAPI.getPlayerManager().getLCPlayer(player).getFullPrefix() + " à " + args[2] + " LightCoins ! "));

                                if (LCAPI.getPlayerManager().getLCPlayer(player).getServer().getGroup().toLowerCase().equals("lobby")) {
                                    LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.SPIGOT_ONLY, "LCHUBUpdate", LCAPI.getPlayerManager().getLCPlayer(player).getServer().toString() + Csts.SPLITTER + player.getUniqueId().toString() + Csts.SPLITTER + "SCOREBOARD" + Csts.SPLITTER + "LIGHTCOINS" + Csts.SPLITTER + args[2]);
                                }
                            } else if(Integer.parseInt(args[2])<0) {
                                send.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.RED + "Le joueur ne peux pas avoir un nombre de LightCoins négatif !"));
                            } else  {
                                send.sendMessage(TextComponent.fromLegacyText(COINS_PREFIX + ChatColor.RED + "Le joueur n'existe pas dans la BDD !"));
                            }
                            return true;
                        default:
                            return false;
                    }
                } else {
                    return false;
                }
            } catch (SQLException e) {
                Log.ERROR.sendLog(LOGERRORBDD);
                e.printStackTrace();
                send.sendMessage(TextComponent.fromLegacyText(MSGERRORPLAYER));
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
        ArrayList<String> result = new ArrayList<>();
        String lastArg = strings[strings.length - 1];
        switch (strings.length){
            case 1:
                lastArg = strings[0];
                result.add("get");
                result.add("add");
                result.add("remove");
                result.add("set");
                break;
            case 2:
                result = CommonUtilsBungee.getOnlinePlayerNames();
                break;
            default:
        }
        return CommonUtilsBungee.partial(lastArg, result);
    }
}
