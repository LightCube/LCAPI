package fr.lightcube.commands.spigot;

import com.google.common.base.Joiner;
import fr.lightcube.LCSpigot;
import fr.lightcube.utils.CommonUtilsSpigot;
import fr.lightcube.utils.Utils;
import fr.lightcube.warp.Warp;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Created by Nitorac on 17/04/2017.
 */
public class WarpCommand implements TabExecutor {

    public static final ArrayList<String> subcommands = new ArrayList<>(Arrays.asList(
        "list", "set", "remove"
    ));

    @Override
    public boolean onCommand(CommandSender cmdSender, Command command, String s, String[] args) {
        switch(args.length){
            case 1:
                if(!(cmdSender instanceof Player)){
                    cmdSender.sendMessage(ChatColor.RED + "Vous devez être un joueur pour exécuter cette commande !");
                    return true;
                }

                Player sender = (Player)cmdSender;

                if(args[0].equalsIgnoreCase("list")){
                    Joiner joiner = Joiner.on(", ").skipNulls();
                    String liste = joiner.join(Utils.mergeArraysString(LCSpigot.getWarpManager().getWarpNames(Warp.WarpType.PUBLIC)));
                    if(!liste.isEmpty()){
                        sender.sendMessage(ChatColor.GREEN + "Les warps disponibles sont : " + liste);
                    }else{
                        sender.sendMessage(ChatColor.RED + "Il n'y a aucun warp disponible !");
                    }
                }else if(LCSpigot.getWarpManager().getWarp(args[0]) != null){
                    LCSpigot.getWarpManager().teleportToWarp(sender, args[0]);
                }else{
                    sender.sendMessage(ChatColor.RED + "Le warp demandé n'existe pas !");
                }
                break;
            case 2:
                if(args[0].equalsIgnoreCase("set")) {
                    if (!(cmdSender instanceof Player)) {
                        cmdSender.sendMessage(ChatColor.RED + "Vous devez être un joueur pour exécuter cette commande !");
                        return true;
                    }

                    if (Pattern.matches("^[a-zA-Z0-9_]+$", args[1])) {
                        LCSpigot.getWarpManager().addWarp(new Warp(args[1], ((Player) cmdSender).getLocation(), Warp.WarpType.PUBLIC));
                        LCSpigot.getWarpManager().saveWarps();
                        cmdSender.sendMessage(ChatColor.GREEN + "Le warp " + ChatColor.WHITE + args[1] + ChatColor.GREEN + " a bien été enregistré.");
                    } else {
                        cmdSender.sendMessage(ChatColor.RED + "Le nom du warp est invalide ! (pas de caractères spéciaux : lettres, chiffers et underscore seulements)");
                    }
                }else if(args[0].equalsIgnoreCase("remove")){
                    if(LCSpigot.getWarpManager().getWarp(args[1]) != null){
                        LCSpigot.getWarpManager().removeWarp(args[1]);
                        LCSpigot.getWarpManager().saveWarps();
                        cmdSender.sendMessage(ChatColor.GREEN + "Le warp " + ChatColor.WHITE + args[1] + ChatColor.GREEN + " a bien été supprimé.");
                    }else{
                        cmdSender.sendMessage(ChatColor.RED + "Impossible de trouver le warp demandé !");
                    }
                }else{
                    UUID victimUuid = CommonUtilsSpigot.resolvePlayer(cmdSender, args[0]);
                    if (victimUuid == null) break;
                    Player victim = Bukkit.getPlayer(victimUuid);
                    if (victim == null) {
                        cmdSender.sendMessage(ChatColor.RED + "Impossible de trouver le joueur demandé !");
                        break;
                    }

                    if (LCSpigot.getWarpManager().teleportToWarp(victim, args[1])) {
                        cmdSender.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.WHITE + victim.getName() + ChatColor.GREEN + " a été téléporté au warp " + ChatColor.WHITE + args[1] + ChatColor.GREEN + ".");
                    } else {
                        cmdSender.sendMessage(ChatColor.RED + "Impossible de trouver le warp demandé !");
                        break;
                    }
                }
                break;
            default:
                return false;
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args){
        ArrayList<String> result = new ArrayList<>();
        String lastArg = args[args.length - 1];

        switch (args.length){
            case 1:
                lastArg = args[0];
                result = Utils.mergeArraysString(subcommands, LCSpigot.getWarpManager().getWarpNames(Warp.WarpType.PUBLIC), CommonUtilsSpigot.getOnlinePlayerNames());
                break;
            case 2:
                if(CommonUtilsSpigot.getOnlinePlayerNames().contains(args[0]) || args[0].equalsIgnoreCase("remove")){
                    result = LCSpigot.getWarpManager().getWarpNames(Warp.WarpType.PUBLIC);
                }
        }

        return CommonUtilsSpigot.partial(lastArg, result);
    }
}
