package fr.lightcube.commands.spigot;

import com.google.common.collect.ImmutableList;
import fr.lightcube.LCAPI;
import fr.lightcube.permissions.Group;
import fr.lightcube.permissions.PermissionManager;
import fr.lightcube.redis.RedisManager;
import fr.lightcube.utils.Csts;
import fr.lightcube.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.util.StringUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.*;

/**
 * Created by Nitorac on 24/05/2017.
 */
public class PermissionsCommand implements TabExecutor{

    private final List<String> BOOLEAN = ImmutableList.of("true", "false");
    private final List<String> ROOT_SUBS = ImmutableList.of("reload", "about", "check", "info", "dump", "group", "player");
    private final List<String> GROUP_SUBS = ImmutableList.of("list", "players", "setperm", "unsetperm");
    private final List<String> PLAYER_SUBS = ImmutableList.of("setgroup", "addgroup", "removegroup", "setperm", "unsetperm");

    private final HashSet<Permission> permSet = new HashSet<>();
    private final ArrayList<String> permList = new ArrayList<>();

    public boolean onCommand(CommandSender sender, Command command, String label, String[] split) {
        if (split.length < 1) {
            return !checkPerm(sender, "help") || usage(sender, command);
        }

        String subcommand = split[0];
        if (subcommand.equals("reload")) {
            if (!checkPerm(sender, "reload")) return true;
            LCAPI.getPermManager().reloadConfig();
            if (LCAPI.getPermManager().configLoadError) {
                LCAPI.getPermManager().configLoadError = false;
                sender.sendMessage(ChatColor.RED + "La configuration est invalide, veuillez regarde la console.");
            } else {
                LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BOTH, "Permissions", "Reload");
                LCAPI.getPermManager().refreshPermissions();
                sender.sendMessage(ChatColor.GREEN + "Configuration rechargée sur tous les serveurs.");
            }
            return true;
        } else if (subcommand.equals("check")) {
            if (!checkPerm(sender, "check")) return true;
            if (split.length != 2 && split.length != 3) return usage(sender, command, subcommand);

            String node = split[1];
            Permissible permissible;
            if (split.length == 2) {
                permissible = sender;
            } else {
                permissible = Bukkit.getServer().getPlayer(split[2]);
            }

            String name = (permissible instanceof Player) ? ((Player) permissible).getName() : (permissible instanceof ConsoleCommandSender) ? "Console" : "Unknown";

            if (permissible == null) {
                sender.sendMessage(ChatColor.RED + "Le joueur " + ChatColor.WHITE + split[2] + ChatColor.RED + " est introuvable.");
            } else {
                boolean set = permissible.isPermissionSet(node), has = permissible.hasPermission(node);
                String sets = set ? " a la permission " : " a la permission (par défaut) ";
                String perm = has ? "true" : "false";
                sender.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.WHITE + name + ChatColor.GREEN + sets + ChatColor.WHITE + node + ChatColor.GREEN + " définie sur " + ChatColor.WHITE + perm + ChatColor.GREEN + ".");
            }
            return true;
        } else if (subcommand.equals("info")) {
            if (!checkPerm(sender, "info")) return true;
            if (split.length != 2) return usage(sender, command, subcommand);

            String node = split[1];
            Permission perm = Bukkit.getServer().getPluginManager().getPermission(node);

            if (perm == null) {
                sender.sendMessage(ChatColor.RED + "La permission " + ChatColor.WHITE + node + ChatColor.RED + " est introuvable.");
            } else {
                sender.sendMessage(ChatColor.GREEN + "Info sur la permission " + ChatColor.WHITE + perm.getName() + ChatColor.GREEN + ":");
                sender.sendMessage(ChatColor.GREEN + "Défaut: " + ChatColor.WHITE + perm.getDefault());
                if (perm.getDescription() != null && perm.getDescription().length() > 0) {
                    sender.sendMessage(ChatColor.GREEN + "Description: " + ChatColor.WHITE + perm.getDescription());
                }
                if (perm.getChildren() != null && perm.getChildren().size() > 0) {
                    sender.sendMessage(ChatColor.GREEN + "Enfants: " + ChatColor.WHITE + perm.getChildren().size());
                }
                if (perm.getPermissibles() != null && perm.getPermissibles().size() > 0) {
                    int num = 0, numTrue = 0;
                    for (Permissible who : perm.getPermissibles()) {
                        ++num;
                        if (who.hasPermission(perm)) {
                            ++numTrue;
                        }
                    }
                    sender.sendMessage(ChatColor.GREEN + "Définie sur: " + ChatColor.WHITE + num + ChatColor.GREEN + " (" + ChatColor.WHITE + numTrue + ChatColor.GREEN + " true)");
                }
            }
            return true;
        } else if (subcommand.equals("dump")) {
            if (!checkPerm(sender, "dump")) return true;
            if (split.length < 1 || split.length > 3) return usage(sender, command, subcommand);

            int page;
            Permissible permissible;
            if (split.length == 1) {
                permissible = sender;
                page = 1;
            } else if (split.length == 2) {
                permissible = sender;
                try {
                    page = Integer.parseInt(split[1]);
                } catch (NumberFormatException ex) {
                    if (split[1].equalsIgnoreCase("-file")) {
                        page = -1;
                    } else {
                        permissible = Bukkit.getServer().getPlayer(split[1]);
                        page = 1;
                    }
                }
            } else {
                permissible = Bukkit.getServer().getPlayer(split[1]);
                try {
                    page = Integer.parseInt(split[2]);
                } catch (NumberFormatException ex) {
                    if (split[2].equalsIgnoreCase("-file")) {
                        page = -1;
                    } else {
                        page = 1;
                    }
                }
            }

            if (permissible == null) {
                sender.sendMessage(ChatColor.RED + "Le joueur " + ChatColor.WHITE + split[1] + ChatColor.RED + " est introuvable.");
                return true;
            }

            ArrayList<PermissionAttachmentInfo> dump = new ArrayList<>(permissible.getEffectivePermissions());
            dump.sort(Comparator.comparing(PermissionAttachmentInfo::getPermission));

            if (page == -1) {
                // Dump to file
                File file = new File(Utils.rootLoc, "dump.txt");
                try {
                    FileOutputStream fos = new FileOutputStream(file);
                    PrintStream out = new PrintStream(fos);
                    // right now permissible is always a CommandSender
                    out.println("Dump de permissions pour: " + ((CommandSender) permissible).getName());
                    out.println(new Date().toString());

                    for (PermissionAttachmentInfo info : dump) {
                        if (info.getAttachment() == null) {
                            out.println(info.getPermission() + "=" + info.getValue() + " (défaut)");
                        } else {
                            out.println(info.getPermission() + "=" + info.getValue() + " (" + info.getAttachment().getPlugin().getDescription().getName() + ")");
                        }
                    }

                    out.close();
                    fos.close();

                    sender.sendMessage(ChatColor.GREEN + "Le dump de permissions a été écrit dans " + ChatColor.WHITE + file);
                } catch (IOException e) {
                    sender.sendMessage(ChatColor.RED + "Impossible d'écrire dans dump.txt, veuillez regarder la console pour plus de détails");
                    sender.sendMessage(ChatColor.RED + e.toString());
                    e.printStackTrace();
                }
                return true;
            }

            int numpages = 1 + (dump.size() - 1) / 8;
            if (page > numpages) {
                page = numpages;
            } else if (page < 1) {
                page = 1;
            }

            ChatColor g = ChatColor.GREEN, w = ChatColor.WHITE, r = ChatColor.RED;

            int start = 8 * (page - 1);
            sender.sendMessage(ChatColor.RED + "[==== " + ChatColor.GREEN + "Page " + page + " sur " + numpages + ChatColor.RED + " ====]");
            for (int i = start; i < start + 8 && i < dump.size(); ++i) {
                PermissionAttachmentInfo info = dump.get(i);

                if (info.getAttachment() == null) {
                    sender.sendMessage(g + "Noeud " + w + info.getPermission() + g + "=" + w + info.getValue() + g + " (" + r + "défaut" + g + ")");
                } else {
                    sender.sendMessage(g + "Noeud " + w + info.getPermission() + g + "=" + w + info.getValue() + g + " (" + w + info.getAttachment().getPlugin().getDescription().getName() + g + ")");
                }
            }
            return true;
        } else if (subcommand.equals("group")) {
            if (split.length < 2) {
                return !checkPerm(sender, "group.help") || usage(sender, command, subcommand);
            }
            groupCommand(sender, command, split);
            return true;
        } else if (subcommand.equals("player")) {
            if (split.length < 2) {
                return !checkPerm(sender, "player.help") || usage(sender, command, subcommand);
            }
            playerCommand(sender, command, split);
            return true;
        } else {
            return !checkPerm(sender, "help") || usage(sender, command);
        }
    }

    private boolean groupCommand(CommandSender sender, Command command, String[] split) {
        String subcommand = split[1];

        if (subcommand.equals("list")) {
            if (!checkPerm(sender, "group.list")) return true;
            if (split.length != 2) return usage(sender, command, "group list");

            String result = "", sep = "";
            for (String key : LCAPI.getPermManager().getNode("groups").getKeys(false)) {
                result += sep + key;
                sep = ", ";
            }
            sender.sendMessage(ChatColor.GREEN + "Groupes: " + ChatColor.WHITE + result);
            return true;
        } else if (subcommand.equals("players")) {
            if (!checkPerm(sender, "group.players")) return true;
            if (split.length != 3) return usage(sender, command, "group players");
            String group = split[2];

            if (LCAPI.getPermManager().getNode("groups/" + group) == null) {
                sender.sendMessage(ChatColor.RED + "Le groupe " + ChatColor.WHITE + group + ChatColor.RED + " est introuvable.");
                return true;
            }

            List<String> users = new LinkedList<>();
            for (String userKey : LCAPI.getPermManager().getNode("users").getKeys(false)) {
                ConfigurationSection node = LCAPI.getPermManager().getNode("users/" + userKey);
                if (node.getStringList("groups").contains(group)) {
                    try {
                        // show UUID and name if available
                        UUID uuid = UUID.fromString(userKey);
                        String name = node.getString("name", "???");
                        users.add(name + ChatColor.GREEN + " (" + ChatColor.WHITE + uuid + ChatColor.GREEN + ")");
                    } catch (IllegalArgumentException ex) {
                        // show as unconverted name-only entry
                        users.add(userKey + ChatColor.GREEN + " (" + ChatColor.WHITE + "non-converti" + ChatColor.GREEN + ")");
                    }
                }
            }
            sender.sendMessage(ChatColor.GREEN + "Joueurs dans " + ChatColor.WHITE + group + ChatColor.GREEN + " (" + ChatColor.WHITE + users.size() + ChatColor.GREEN + "):");
            for (String user : users) {
                sender.sendMessage("  " + user);
            }
            return true;
        } else if (subcommand.equals("setperm")) {
            if (!checkPerm(sender, "group.setperm")) return true;
            if (split.length != 4 && split.length != 5) return usage(sender, command, "group setperm");
            String group = split[2];
            String perm = split[3];
            boolean value = (split.length != 5) || Boolean.parseBoolean(split[4]);

            String node = "permissions";
            if (LCAPI.getPermManager().getNode("groups/" + group) == null) {
                sender.sendMessage(ChatColor.RED + "Le groupe " + ChatColor.WHITE + group + ChatColor.RED + " est introuvable.");
                return true;
            }

            LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BUNGEE_ONLY, "Permissions", "SetGroupPerm" + Csts.SPLITTER + group + Csts.SPLITTER + perm + Csts.SPLITTER + value);

            if (perm.contains(":")) {
                String server = perm.substring(0, perm.indexOf(':'));
                perm = perm.substring(perm.indexOf(':') + 1);
                node = "servers/" + server;
            }

            LCAPI.getPermManager().createNode("groups/" + group + "/" + node).set(perm, value);
            LCAPI.getPermManager().refreshForGroup(group);

            sender.sendMessage(ChatColor.GREEN + "Le groupe " + ChatColor.WHITE + group + ChatColor.GREEN + " possède maintenant la permission " + ChatColor.WHITE + perm + ChatColor.GREEN + " définie sur " + ChatColor.WHITE + value + ChatColor.GREEN + ".");
            return true;
        } else if (subcommand.equals("unsetperm")) {
            if (!checkPerm(sender, "group.unsetperm")) return true;
            if (split.length != 4) return usage(sender, command, "group unsetperm");
            String group = split[2].toLowerCase();
            String perm = split[3];

            String node = "permissions";
            if (LCAPI.getPermManager().getNode("groups/" + group) == null) {
                sender.sendMessage(ChatColor.RED + "Le groupe " + ChatColor.WHITE + group + ChatColor.RED + " est introuvable.");
                return true;
            }

            String basePerm = perm;

            if (perm.contains(":")) {
                String server = perm.substring(0, perm.indexOf(':'));
                perm = perm.substring(perm.indexOf(':') + 1);
                node = "servers/" + server;
            }

            ConfigurationSection sec = LCAPI.getPermManager().createNode("groups/" + group + "/" + node);
            if (!sec.contains(perm)) {
                sender.sendMessage(ChatColor.GREEN + "Le groupe " + ChatColor.WHITE + group + ChatColor.GREEN + " n'a pas la permission " + ChatColor.WHITE + perm + ChatColor.GREEN + " définie.");
                return true;
            }

            LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BUNGEE_ONLY, "Permissions", "UnsetGroupPerm" + Csts.SPLITTER + group + Csts.SPLITTER + basePerm + Csts.SPLITTER);

            sec.set(perm, null);
            LCAPI.getPermManager().refreshForGroup(group);
            sender.sendMessage(ChatColor.GREEN + "Le groupe " + ChatColor.WHITE + group + ChatColor.GREEN + " n'a plus la permission " + ChatColor.WHITE + perm + ChatColor.GREEN + " définie.");
            return true;
        } else {
            return !checkPerm(sender, "group.help") || usage(sender, command);
        }
    }

    private boolean playerCommand(CommandSender sender, Command command, String[] split) {
        String subcommand = split[1];

        if (subcommand.equals("groups")) {
            if (!checkPerm(sender, "player.groups")) return true;
            if (split.length != 3) return usage(sender, command, "player groups");
            UUID player = resolvePlayer(sender, split[2]);
            if (player == null) return true;

            if (LCAPI.getPermManager().getNode("users/" + player) == null) {
                sender.sendMessage(ChatColor.RED + "Le joueur " + ChatColor.WHITE + Bukkit.getPlayer(player).getName() + ChatColor.RED + " est dans le groupe par défaut.");
                return true;
            }

            int count = 0;
            StringBuilder text = new StringBuilder();
            String sep = "";
            for (String group : LCAPI.getPermManager().getNode("users/" + player).getStringList("groups")) {
                ++count;
                text.append(sep).append(group);
                sep = ", ";
            }
            sender.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.WHITE + Bukkit.getPlayer(player).getName() + ChatColor.GREEN + " appartient aux groupes (" + ChatColor.WHITE + count + ChatColor.GREEN + "): " + ChatColor.WHITE + text);
            return true;
        } else if (subcommand.equals("setgroup")) {
            if (!checkPerm(sender, "player.setgroup")) return true;
            if (split.length != 4) return usage(sender, command, "player setgroup");
            UUID player = resolvePlayer(sender, split[2]);
            if (player == null) return true;

            if(!rankCheck(sender, player, split)){
                return true;
            }
            LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BUNGEE_ONLY, "Permissions", "PlayerSetGroup" + Csts.SPLITTER + player.toString() + Csts.SPLITTER + split[3] + Csts.SPLITTER);

            LCAPI.getPermManager().createNode("users/" + player).set("groups", Collections.singletonList(LCAPI.getPermManager().getGroup(split[3]).getName()));
            LCAPI.getPermManager().refreshForPlayer(player);
            sender.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.WHITE + Bukkit.getPlayer(player).getName() + ChatColor.GREEN + " est maintenant dans " + ChatColor.WHITE + split[3] + ChatColor.GREEN + ".");

            return true;
        } else if (subcommand.equals("addgroup")) {
            if (!checkPerm(sender, "player.addgroup")) return true;
            if (split.length != 4) return usage(sender, command, "player addgroup");
            UUID player = resolvePlayer(sender, split[2]);
            if (player == null) return true;

            if(!rankCheck(sender, player, split)){
                return true;
            }

            String group = split[3];

            List<String> list = LCAPI.getPermManager().createNode("users/" + player).getStringList("groups");
            if (list.contains(group)) {
                sender.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.WHITE + Bukkit.getPlayer(player).getName() + ChatColor.GREEN + " est déjà dans le groupe " + ChatColor.WHITE + group + ChatColor.GREEN + ".");
                return true;
            }
            list.add(group);
            LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BUNGEE_ONLY, "Permissions", "PlayerAddGroup" + Csts.SPLITTER + player.toString() + Csts.SPLITTER + group + Csts.SPLITTER);
            LCAPI.getPermManager().getNode("users/" + player).set("groups", list);
            LCAPI.getPermManager().refreshForPlayer(player);

            sender.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.WHITE + Bukkit.getPlayer(player).getName() + ChatColor.GREEN + " appartient maintenant au groupe " + ChatColor.WHITE + group + ChatColor.GREEN + ".");
            return true;
        } else if (subcommand.equals("removegroup")) {
            if (!checkPerm(sender, "player.removegroup")) return true;
            if (split.length != 4) return usage(sender, command, "player removegroup");
            UUID player = resolvePlayer(sender, split[2]);
            if (player == null) return true;

            if(sender instanceof Player && player.compareTo(((Player)sender).getUniqueId()) == 0){
                sender.sendMessage(ChatColor.RED + "Vous ne pouvez pas retirer votre propre groupe.");
                return true;
            }

            String group = split[3];

            List<String> list = LCAPI.getPermManager().createNode("users/" + player).getStringList("groups");
            if (!list.contains(group)) {
                sender.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.WHITE + Bukkit.getPlayer(player).getName() + ChatColor.GREEN + " n'est pas dans le groupe " + ChatColor.WHITE + group + ChatColor.GREEN + ".");
                return true;
            }

            LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BUNGEE_ONLY, "Permissions", "PlayerRemoveGroup" + Csts.SPLITTER + player.toString() + Csts.SPLITTER + group + Csts.SPLITTER);
            list.remove(group);
            LCAPI.getPermManager().getNode("users/" + player).set("groups", list);

            LCAPI.getPermManager().refreshForPlayer(player);

            sender.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.WHITE + Bukkit.getPlayer(player).getName() + ChatColor.GREEN + " n'est plus dans le groupe " + ChatColor.WHITE + group + ChatColor.GREEN + ".");
            return true;
        } else if (subcommand.equals("setperm")) {
            if (!checkPerm(sender, "player.setperm")) return true;
            if (split.length != 4 && split.length != 5) return usage(sender, command, "player setperm");
            UUID player = resolvePlayer(sender, split[2]);
            if (player == null) return true;
            String perm = split[3];
            boolean value = (split.length != 5) || Boolean.parseBoolean(split[4]);

            String node = "permissions";
            LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BUNGEE_ONLY, "Permissions", "SetPlayerPerm" + Csts.SPLITTER + player.toString() + Csts.SPLITTER + perm + Csts.SPLITTER + value);
            if (perm.contains(":")) {
                String server = perm.substring(0, perm.indexOf(':'));
                perm = perm.substring(perm.indexOf(':') + 1);
                node = "servers/" + server;
            }

            LCAPI.getPermManager().createNode("users/" + player + "/" + node).set(perm, value);
            LCAPI.getPermManager().refreshForPlayer(player);

            sender.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.WHITE + Bukkit.getPlayer(player).getName() + ChatColor.GREEN + " possède maintenant la permission " + ChatColor.WHITE + perm + ChatColor.GREEN + " définie sur " + ChatColor.WHITE + value + ChatColor.GREEN + ".");
            return true;
        } else if (subcommand.equals("unsetperm")) {
            if (!checkPerm(sender, "player.unsetperm")) return true;
            if (split.length != 4) return usage(sender, command, "player unsetperm");
            UUID player = resolvePlayer(sender, split[2]);
            if (player == null) return true;
            String perm = split[3];

            String basePerm = perm;

            String node = "permissions";
            if (perm.contains(":")) {
                String server = perm.substring(0, perm.indexOf(':'));
                perm = perm.substring(perm.indexOf(':') + 1);
                node = "servers/" + server;
            }

            ConfigurationSection sec = LCAPI.getPermManager().createNode("users/" + player + "/" + node);
            if (!sec.contains(perm)) {
                sender.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.WHITE + Bukkit.getPlayer(player).getName() + ChatColor.GREEN + " n'a pas la permission " + ChatColor.WHITE + perm + ChatColor.GREEN + " définie.");
                return true;
            }
            LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BUNGEE_ONLY, "Permissions", "UnsetPlayerPerm" + Csts.SPLITTER + player.toString() + Csts.SPLITTER + basePerm);
            sec.set(perm, null);
            LCAPI.getPermManager().refreshForPlayer(player);

            sender.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.WHITE + Bukkit.getPlayer(player).getName() + ChatColor.GREEN + " n'a plus la permission " + ChatColor.WHITE + perm + ChatColor.GREEN + " définie.");
            return true;
        } else {
            return !checkPerm(sender, "player.help") || usage(sender, command);
        }
    }

    private UUID resolvePlayer(CommandSender sender, String arg) {
        arg = arg.toLowerCase();

        // see if it resolves to a single player name
        List<Player> players = Bukkit.getServer().matchPlayer(arg);
        if (players.size() == 1) {
            return players.get(0).getUniqueId();
        } else if (players.size() > 1) {
            sender.sendMessage(ChatColor.RED + "Le pseudo " + ChatColor.WHITE + arg + ChatColor.RED + " est ambigüe.");
            return null;
        }

        if (arg.length() == 32) {
            // expand UUIDs which do not have dashes in them
            arg = arg.substring(0, 8) + "-" + arg.substring(8, 12) + "-" + arg.substring(12, 16) +
                "-" + arg.substring(16, 20) + "-" + arg.substring(20, 32);
        }
        if (arg.length() == 36) {
            // is of correct UUID length
            try {
                return UUID.fromString(arg);
            } catch (IllegalArgumentException ex) {
                // ignore
            }
        }

        sender.sendMessage(ChatColor.RED + "Impossible d'interpréter: " + ChatColor.WHITE + arg);
        sender.sendMessage(ChatColor.RED + "Vous devez donner le UUID ou le nom d'un joueur en ligne.");
        return null;
    }

    // -- utilities --

    private boolean rankCheck(CommandSender sender, UUID player, String[] split){
        if(sender instanceof Player && player.compareTo(((Player)sender).getUniqueId()) == 0){
            sender.sendMessage(ChatColor.RED + "Vous ne pouvez pas définir votre propre groupe.");
            return false;
        }

        Group group2set = LCAPI.getPermManager().getGroup(split[3]);

        if(group2set == null){
            sender.sendMessage(ChatColor.RED + "Le groupe " + ChatColor.WHITE + split[3] + ChatColor.RED + " n'existe pas.");
            return false;
        }

        int[] maxSenderPriority = {0};

        if(sender instanceof ConsoleCommandSender){
            maxSenderPriority[0] = Integer.MAX_VALUE;
        }else{
            LCAPI.getPermManager().getGroups(((Player)sender).getUniqueId()).forEach(group -> {
                if(maxSenderPriority[0] <= group.getPriority()){
                    maxSenderPriority[0] = group.getPriority();
                }
            });
        }

        if(maxSenderPriority[0] <= group2set.getPriority()){
            sender.sendMessage(ChatColor.RED + "Vous ne pouvez pas grader un joueur à votre grade ou supérieur !");
            return false;
        }

        int[] maxVictimPriority = {0};
        LCAPI.getPermManager().getGroups(player).forEach(group -> {
            if(maxVictimPriority[0] <= group.getPriority()){
                maxVictimPriority[0] = group.getPriority();
            }
        });

        if(maxSenderPriority[0] <= maxVictimPriority[0]){
            sender.sendMessage(ChatColor.RED + "Vous ne pouvez pas grader un joueur de votre grade ou supérieur !");
            return false;
        }

        return true;
    }

    private boolean checkPerm(CommandSender sender, String subnode) {
        boolean ok = sender.hasPermission("permissions." + subnode);
        if (!ok) {
            sender.sendMessage(PermissionManager.HASNT_PERMS_MESSAGE);
        }
        return ok;
    }

    private boolean usage(CommandSender sender, Command command) {
        sender.sendMessage(ChatColor.RED + "[====" + ChatColor.GREEN + " /permissons " + ChatColor.RED + "====]");
        for (String line : command.getUsage().split("\\n")) {
            if ((line.startsWith("/<command> group") && !line.startsWith("/<command> group -")) ||
                (line.startsWith("/<command> player") && !line.startsWith("/<command> player -"))) {
                continue;
            }
            sender.sendMessage(formatLine(line));
        }
        return true;
    }

    private boolean usage(CommandSender sender, Command command, String subcommand) {
        sender.sendMessage(ChatColor.RED + "[====" + ChatColor.GREEN + " /permissons " + subcommand + " " + ChatColor.RED + "====]");
        for (String line : command.getUsage().split("\\n")) {
            if (line.startsWith("/<command> " + subcommand)) {
                sender.sendMessage(formatLine(line));
            }
        }
        return true;
    }

    private String formatLine(String line) {
        int i = line.indexOf(" - ");
        String usage = line.substring(0, i);
        String desc = line.substring(i + 3);

        usage = usage.replace("<command>", "permissions");
        usage = usage.replaceAll("\\[[^]:]+]", ChatColor.AQUA + "$0" + ChatColor.GREEN);
        usage = usage.replaceAll("\\[[^]]+:]", ChatColor.AQUA + "$0" + ChatColor.LIGHT_PURPLE);
        usage = usage.replaceAll("<[^>]+>", ChatColor.LIGHT_PURPLE + "$0" + ChatColor.GREEN);

        return ChatColor.GREEN + usage + " - " + ChatColor.WHITE + desc;
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        // Remember that we can return null to default to online player name matching

        /*
        reload - reload the configuration from disk.
        check <node> [player] - check if a player or the sender has a permission (any plugin).
        info <node> - prints information on a specific permission.
        dump [player] [page] - prints info about a player's (or the sender's) permissions.
        setrank <player> <group> - set a player to be in a group with per-group permissions.
        group - list group-related commands.
        player - list player-related commands.
         */
        String lastArg = args[args.length - 1];

        if (args.length <= 1) {
            return partial(args[0], ROOT_SUBS);
        } else if (args.length == 2) {
            String sub = args[0];
            if (sub.equals("check")) {
                return partial(lastArg, allNodes());
            } else if (sub.equals("info")) {
                return partial(lastArg, allNodes());
            } else if (sub.equals("dump")) {
                return null;
            } else if (sub.equals("rank") || sub.equals("setrank")) {
                return null;
            } else if (sub.equals("group")) {
                return partial(lastArg, GROUP_SUBS);
            } else if (sub.equals("player")) {
                return partial(lastArg, PLAYER_SUBS);
            }
        } else {
            String sub = args[0];
            // note that dump is excluded here because there's no real reason to tab-complete page numbers
            if (sub.equals("check") && args.length == 3) {
                return null;
            } else if ((sub.equals("rank") || sub.equals("setrank")) && args.length == 3) {
                return partial(lastArg, allGroups());
            } else if (sub.equals("group")) {
                return groupComplete(sender, args);
            } else if (sub.equals("player")) {
                return playerComplete(sender, args);
            }
        }

        return ImmutableList.of();
    }

    private List<String> groupComplete(CommandSender sender, String[] args) {
        String sub = args[1];
        String lastArg = args[args.length - 1];
        /*
        group list - list all groups.
        group players <group> - list players in a group.
        group setperm <group> <[server:]node> [true|false] - set a permission on a group.
        group unsetperm <group> <[server:]node> - unset a permission on a group.
         */

        if (sub.equals("players")) {
            if (args.length == 3) {
                return partial(lastArg, allGroups());
            }
        } else if (sub.equals("setperm")) {
            if (args.length == 3) {
                return partial(lastArg, allGroups());
            } else if (args.length == 4) {
                return serverNodeComplete(lastArg);
            } else if (args.length == 5) {
                return partial(lastArg, BOOLEAN);
            }
        } else if (sub.equals("unsetperm")) {
            if (args.length == 3) {
                return partial(lastArg, allGroups());
            } else if (args.length == 4) {
                // TODO: maybe only show nodes that are already set?
                return serverNodeComplete(lastArg);
            }
        }

        return ImmutableList.of();
    }

    private List<String> playerComplete(CommandSender sender, String[] args) {
        String sub = args[1];
        String lastArg = args[args.length - 1];
        /*
        player groups <player> - list groups a player is in.
        player setgroup <player> <group,...> - set a player to be in only the given groups.
        player addgroup <player> <group> - add a player to a group.
        player removegroup <player> <group> - remove a player from a group.
        player setperm <player> <[server:]node> [true|false] - set a permission on a player.
        player unsetperm <player> <[server:]node> - unset a permission on a player.
         */

        // A convenience in case I later want to replace online players with something else
        final List<String> players = null;

        if (sub.equals("groups")) {
            if (args.length == 3) {
                return players;
            }
        } else if (sub.equals("setgroup")) {
            if (args.length == 3) {
                return players;
            } else if (args.length == 4) {
                // do some magic to complete after any commas
                int idx = lastArg.lastIndexOf(',');
                if (idx == -1) {
                    return partial(lastArg, allGroups());
                } else {
                    String done = lastArg.substring(0, idx + 1); // includes the comma
                    String toComplete = lastArg.substring(idx + 1);
                    List<String> groups = partial(toComplete, allGroups());
                    List<String> result = new ArrayList<String>(groups.size());
                    for (String group : groups) {
                        result.add(done + group);
                    }
                    return result;
                }
            }
        } else if (sub.equals("addgroup") || sub.equals("removegroup")) {
            if (args.length == 3) {
                return players;
            } else if (args.length == 4) {
                return partial(lastArg, allGroups());
            }
        } else if (sub.equals("setperm")) {
            if (args.length == 3) {
                return players;
            } else if (args.length == 4) {
                return serverNodeComplete(lastArg);
            } else if (args.length == 5) {
                return partial(lastArg, BOOLEAN);
            }
        } else if (sub.equals("unsetperm")) {
            if (args.length == 3) {
                return players;
            } else if (args.length == 4) {
                // TODO: maybe only show nodes that are already set?
                return serverNodeComplete(lastArg);
            }
        }

        return ImmutableList.of();
    }

    private Collection<String> allGroups() {
        return LCAPI.getPermManager().getConfig().getConfigurationSection("groups").getKeys(false);
    }

    private Collection<String> allNodes() {
        Set<Permission> newPermSet = Bukkit.getServer().getPluginManager().getPermissions();
        if (!permSet.equals(newPermSet)) {
            permSet.clear();
            permSet.addAll(newPermSet);

            permList.clear();
            for (Permission p : permSet) {
                permList.add(p.getName());
            }
            Collections.sort(permList);
        }
        return permList;
    }

    private List<String> serverNodeComplete(String token) {
        // TODO: complete [server:]node
        return partial(token, allNodes());
    }

    private List<String> partial(String token, Collection<String> from) {
        return StringUtil.copyPartialMatches(token, from, new ArrayList<>(from.size()));
    }
}