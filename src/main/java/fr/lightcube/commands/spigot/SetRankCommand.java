package fr.lightcube.commands.spigot;

import fr.lightcube.LCAPI;
import fr.lightcube.rank.Rank;
import fr.lightcube.redis.RedisManager;
import fr.lightcube.utils.CommonUtilsSpigot;
import fr.lightcube.utils.Log;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Nitorac on 27/03/2017.
 */
public class SetRankCommand implements TabExecutor {

    @Override
    public boolean onCommand(CommandSender cmdSender, Command command, String s, String[] args) {
        if (args.length == 2){
            UUID victimUuid = CommonUtilsSpigot.resolvePlayer(cmdSender, args[0]);
            if(victimUuid == null) return true;

            Player victim = Bukkit.getPlayer(victimUuid);

            if (victim != null) {
                Rank rank = LCAPI.getRankManager().getRankFromName(args[1]);
                if(rank == null){
                    cmdSender.sendMessage(ChatColor.RED + "Impossible de trouver le rang demandé !");
                    return true;
                }

                int rankId = 0;

                if(cmdSender instanceof Player){
                    rankId = LCAPI.getPlayerManager().getLCPlayer(((Player)cmdSender)).getRank().getRankID();
                }else if(cmdSender instanceof ConsoleCommandSender){
                    rankId = Integer.MAX_VALUE;
                }

                if(cmdSender instanceof Player && victimUuid.compareTo(((Player) cmdSender).getUniqueId()) == 0){
                    cmdSender.sendMessage(ChatColor.RED + "Vous ne pouvez définir votre propre rang !");
                    return true;
                }

                if(LCAPI.getPlayerManager().getLCPlayer(victim).getRank().getRankID() >= rankId){
                    cmdSender.sendMessage(ChatColor.RED + "Vous ne pouvez pas modifier le rang d'un joueur ayant un rang supérieur ou égal au votre.");
                    return true;
                }

                if(Math.abs(rankId/10) <= Math.abs(rank.getRankID())/10){
                    cmdSender.sendMessage(ChatColor.RED + "Vous ne pouvez pas grader le joueur " + ChatColor.WHITE
                    + victim.getName() + ChatColor.RED + " au dessus de ou à votre propre rang !");
                    return true;
                }

                try {
                    LCAPI.getDBManager().update("UPDATE players SET RANK = ? WHERE UUID = ?", rank.getRankID(), victim.getUniqueId().toString());
                    victim.sendMessage(ChatColor.GREEN + "Votre nouveau rang est "+ rank.getName());
                    cmdSender.sendMessage(ChatColor.GREEN + "Le nouveau rang de " + ChatColor.WHITE + victim.getName() + ChatColor.GREEN + " est " + ChatColor.WHITE + rank.getName() + ChatColor.GREEN + ".");
                    LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BOTH, "LCReload", "Player " + victim.getUniqueId().toString());
                }catch (Exception e){
                    Log.ERROR.sendLog("La commande setrank n'as pas fonctionnée pour le joueur "+ args[0] + " : " + e.getLocalizedMessage());
                }
            }else {
                cmdSender.sendMessage("§cImpossible de trouver le joueur §f" + args[0]);
            }
            return true;
        }

        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args){
        ArrayList<String> result = new ArrayList<>();
        String lastArg = args[args.length - 1];

        switch (args.length){
            case 1:
                lastArg = args[0];
                result = CommonUtilsSpigot.getOnlinePlayerNames();
                break;
            case 2:
                result = LCAPI.getRankManager().getRanksNames();
        }

        return CommonUtilsSpigot.partial(lastArg, result);
    }
}
