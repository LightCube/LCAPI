package fr.lightcube.commands.spigot;

import com.google.common.collect.ImmutableList;
import fr.lightcube.LCAPI;
import fr.lightcube.redis.RedisManager;
import fr.lightcube.utils.CommonUtilsSpigot;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by Nitorac on 22/04/2017.
 */
public class ReloadCommand implements TabExecutor {

    private static final ArrayList<String> subcommands = new ArrayList<>(Arrays.asList(
        "all", "warp", "chat", "player", "perm"
    ));

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(args.length == 1){
            switch(args[0].toLowerCase()){
                case "all":
                    CommonUtilsSpigot.reloadWarp();
                    CommonUtilsSpigot.reloadPerm();
                    CommonUtilsSpigot.reloadRank();
                    CommonUtilsSpigot.reloadPlayers();
                    LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BOTH, "LCReload", "All");
                    commandSender.sendMessage(ChatColor.GREEN + "Le plugin a été reload !");
                    break;
                case "warp":
                    CommonUtilsSpigot.reloadWarp();
                    LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BOTH, "LCReload", "Warps");
                    commandSender.sendMessage(ChatColor.GREEN + "Les warps ont été reload !");
                    break;
                case "chat":
                    CommonUtilsSpigot.reloadRank();
                    LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BOTH, "LCReload", "Ranks");
                    commandSender.sendMessage(ChatColor.GREEN + "Le chat a été reload !");
                    break;
                case "player":
                    CommonUtilsSpigot.reloadPlayers();
                    LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BOTH, "LCReload", "Players");
                    commandSender.sendMessage(ChatColor.GREEN + "Tous les joueurs ont été reload !");
                    break;
                case "perm":
                    CommonUtilsSpigot.reloadPerm();
                    commandSender.sendMessage(ChatColor.GREEN + "Les permissions ont été reload !");
                    break;
                default:
                    return false;
            }
            return true;
        }else if(args.length == 2){
            if(args[0].equalsIgnoreCase("player")){
                UUID victimUuid = CommonUtilsSpigot.resolvePlayer(commandSender, args[1]);
                if(victimUuid == null) return true;

                Player victim = Bukkit.getPlayer(victimUuid);
                if(victim == null){
                    commandSender.sendMessage(ChatColor.RED + "Impossible de trouver le joueur demandé !");
                    return true;
                }

                LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BOTH, "LCReload", "Player " + victimUuid.toString());
                LCAPI.getPlayerManager().reloadPlayer(victim);
                commandSender.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.WHITE + victim.getName() + ChatColor.GREEN + " a bien été rechargé !");
            }
        }

        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args) {
        String lastArg = args[args.length - 1];
        if(args.length == 1){
            return CommonUtilsSpigot.partial(args[0], subcommands);
        }else if(args.length == 2){
            if(args[0].equalsIgnoreCase("player")){
                return CommonUtilsSpigot.partial(lastArg, CommonUtilsSpigot.getOnlinePlayerNames());
            }
        }
        return ImmutableList.of();
    }
}
