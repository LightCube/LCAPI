package fr.lightcube.commands.spigot;

import com.google.common.base.Joiner;
import fr.lightcube.utils.CommonUtilsSpigot;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Nitorac on 20/05/2017.
 */
public class ConsoleCommand implements TabExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (strings.length < 1) {
            return false;
        }
        Joiner joiner = Joiner.on(" ").skipNulls();
        UUID victim = CommonUtilsSpigot.resolvePlayer(commandSender, strings[0], true);
        if (victim != null) {
            strings[0] = null;
            if (strings[1].toLowerCase().equals("sc") || strings[1].toLowerCase().equals("staffchat")) {
                commandSender.sendMessage(ChatColor.RED + "Vous ne pouvez pas executer cette commande à la place d'un autre joueur !");
            } else {
                if (Bukkit.getServer().dispatchCommand(Bukkit.getPlayer(victim), joiner.join(strings).trim())) {
                    commandSender.sendMessage(ChatColor.GREEN + "Votre commande a bien été exécutée à la place de " + ChatColor.WHITE + Bukkit.getPlayer(victim).getName() + ChatColor.GREEN + ".");
                } else {
                    commandSender.sendMessage(ChatColor.RED + "Une erreur est survenue dans votre commande.");
                }
            }
            return true;
        }

        if (Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), joiner.join(strings).trim())) {
            commandSender.sendMessage(ChatColor.GREEN + "Votre commande a bien été exécutée par le serveur.");
        } else {
            commandSender.sendMessage(ChatColor.RED + "Une erreur est survenue dans votre commande.");
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        return new ArrayList<>();
    }
}
