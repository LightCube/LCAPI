package fr.lightcube.servers.exceptions;

/**
 * Created by Nitorac on 27/06/2017.
 */
public class ServerNotFoundException extends Exception {

    private String server;

    public ServerNotFoundException(String server){
        this.server = server;
    }

    @Override
    public String getMessage(){
        return "Le serveur " + server + " est introuvable !";
    }
}
