package fr.lightcube.servers;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Ordering;
import com.google.common.collect.Table;
import fr.lightcube.LCAPI;
import fr.lightcube.LCBungee;
import fr.lightcube.game.GameState;
import fr.lightcube.players.LCPlayer;
import fr.lightcube.redis.RedisManager;
import fr.lightcube.servers.exceptions.ServerNotFoundException;
import fr.lightcube.utils.Csts;
import fr.lightcube.utils.Log;
import fr.lightcube.utils.Utils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Nitorac on 06/05/2017.
 */
public class ServerManager {

    private static Map<String, ArrayList<Server>> groups;
    private Server currentServer;

    public ServerManager(){
        groups = new HashMap<>();
        LCAPI.getRedisManager().registerReceiver("ServerListener", (subchannel, msg) -> {
            switch (subchannel){
                case "SendToServerGroupBUNGEE":
                    String[] splitted = msg.split(Csts.SPLITTER, 3);
                    sendPlayerToServerGroupBungee(UUID.fromString(splitted[0]), splitted[1], Strategy.fromString(splitted[2]));
                    break;
                case "SendToServerBUNGEE":
                    String[] splitted2 = msg.split(Csts.SPLITTER, 2);
                    sendPlayerToServerBungee(UUID.fromString(splitted2[0]), getServerSilent(splitted2[1]));
                    break;
            }
        });
    }


    public void setCurrentServer(String curServerName){
        if(!Csts.isBungee){
            currentServer = getServerSilent(curServerName);
        }else{
            currentServer = new Server("bungee-#0", ProxyServer.getInstance().getConfig().getPlayerLimit());
        }
    }


    public Server registerServer(String server, int maxPlayers) {
        Server server2register = createServerSilent(server, maxPlayers);
        if(server2register == null) return null;

        groups.computeIfAbsent(server2register.getGroup(), k -> new ArrayList<>());
        if(!groups.get(server2register.getGroup()).contains(server2register)){
            groups.get(server2register.getGroup()).add(server2register);
        }

        return server2register;
    }

    private void sendPlayerToServerGroupBungee(UUID player, String group, Strategy strat){
        sendPlayerToServerBungee(player, strat.getServerFromGroupBungee(player, group));
    }

    private void sendPlayerToServerBungee(UUID player, Server server){
        LCBungee.getInstance().getProxy().getPlayer(player).connect(LCBungee.getInstance().getProxy().getServerInfo(server.toString()));
    }

    public ArrayList<Server> getServersFromGroup(String group){
        return groups.get(group);
    }

    public void sendPlayerToLobby(UUID player){
        sendPlayerToServerGroup(player, "lobby", Strategy.LOBBY);
    }

    public void sendPlayerToServerGroup(UUID player, String group, Strategy strat){
        if(Csts.isBungee){
            sendPlayerToServerGroupBungee(player, group, strat);
        }else{
            LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BUNGEE_ONLY, "SendToServerGroupBUNGEE", player.toString() + Csts.SPLITTER + group + Csts.SPLITTER + strat.toString());
        }
    }

    public void sendPlayerToServer(UUID player, String server, Strategy strat) throws ServerNotFoundException{
        Server serverS = getServer(server);

        if(Csts.isBungee){
            sendPlayerToServerBungee(player, serverS);
        }else{
            LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BUNGEE_ONLY, "SendToServerBUNGEE", player.toString() + Csts.SPLITTER + serverS.toString());
        }
    }

    public boolean contains(String serverName){
        return getAllServers().stream().anyMatch(server -> server.toString().equals(serverName));
    }

    public ArrayList<Server> getAllServers(){
        ArrayList<Server> result = new ArrayList<>();
        groups.values().forEach(result::addAll);
        return result;
    }

    public Server getServer(String server) throws ServerNotFoundException{
        Server fake = createServer(server, null);
        return getServer(fake.getGroup(), fake.getNumber());
    }

    public Server getServer(String group, int number) throws ServerNotFoundException{
        return groups.get(group).stream().filter(f -> f.toString().startsWith(group) && f.toString().endsWith(String.valueOf(number))).findFirst().orElse(null);
    }

    public Server getServerSilent(String server){
        try{
            return getServer(server);
        }catch (ServerNotFoundException e){
            e.printStackTrace();
            Log.ERROR.sendLog(e.getMessage());
            return null;
        }
    }

    public Server getServerSilent(String group, int number){
        try{
            return getServer(group, number);
        }catch (ServerNotFoundException e){
            e.printStackTrace();
            Log.ERROR.sendLog(e.getMessage());
            return null;
        }
    }

    public Server getServerFromGroup(UUID player, String group, Strategy strat){
        return strat.getServerFromGroupBungee(player, group);
    }

    private Server createServer(String server, Integer maxPlayers) throws ServerNotFoundException {
        String[] mixed = server.split("-#");
        int number;
        if (mixed.length != 2 || (number = Utils.parseInt(mixed[1], -1)) <= 0) throw new ServerNotFoundException(server);

        return new Server(mixed[0], number, maxPlayers);
    }

    private Server createServerSilent(String server, Integer maxPlayers){
        try {
            return createServer(server, maxPlayers);
        }catch (ServerNotFoundException e){
            e.printStackTrace();
            return null;
        }
    }

    public boolean unregisterServer(Server server2remove){
        return groups.get(server2remove.getGroup()).removeIf(server -> server.equals(server2remove));
    }

    public Server getCurrentServer(){
        return currentServer;
    }


public enum Strategy {
    GAME("GAME"),
    LOBBY("LOBBY");

    String strat;

    Strategy(String strat){
        this.strat = strat;
    }

    public String toString(){
        return strat;
    }

    public static Strategy fromString(String s){
        switch(s){
            case "GAME":
                return Strategy.GAME;
            case "LOBBY":
                return Strategy.LOBBY;
        }
        return null;
    }

    public Server getServerFromGroupBungee(UUID player, String group){
        ArrayList<Server> servers = new ArrayList<>(LCAPI.getServerManager().getServersFromGroup(group));
        LCPlayer lcPlayer = LCAPI.getPlayerManager().getLCPlayer(player);
        ProxiedPlayer pplayer = LCBungee.getInstance().getProxy().getPlayer(player);

        if(servers.isEmpty() && pplayer != null){
            pplayer.sendMessage(TextComponent.fromLegacyText(Csts.MSG_GAME + ChatColor.RED + "Impossible de vous envoyer au serveur demandé ! (Le groupe de serveurs n'existe pas)"));
            return null;
        }

        Table<ServerInfo, Integer, Integer> serverPlayers = HashBasedTable.create();

        servers.forEach(s -> {
            ServerInfo serv = LCBungee.getInstance().getProxy().getServerInfo(s.toString());
            if(s.getMaxPlayers() == null){
                Log.ERROR.sendLog("Le serveur " + s.toString() + " n'a pas défini son nombre de joueur maximal !");
                return;
            }
            serverPlayers.put(serv, serv.getPlayers().size(), s.getMaxPlayers());
        });


        switch(strat){
            case "LOBBY":
                ImmutableTable<ServerInfo, Integer, Integer> sortedLobby = sortTableLobby(serverPlayers);
                for(Table.Cell<ServerInfo, Integer, Integer> cell : sortedLobby.cellSet()){
                    if(cell == null || cell.getRowKey() == null || cell.getColumnKey() == null || cell.getValue() == null) {
                        Log.ERROR.sendLog("NULL VALUE POUR " + "    " + cell.getRowKey() + "    " + cell.getColumnKey() + "     " + cell.getValue());
                        continue;
                    }
                    if(cell.getColumnKey() * 100 / cell.getValue() <= 85){
                        return LCAPI.getServerManager().getServerSilent(cell.getRowKey().getName());
                    }

                }

                for(Table.Cell<ServerInfo, Integer, Integer> cell : sortedLobby.cellSet()){
                    if(cell == null || cell.getRowKey() == null || cell.getColumnKey() == null || cell.getValue() == null) continue;
                    if(cell.getColumnKey() < cell.getValue()) {
                        return LCAPI.getServerManager().getServerSilent(cell.getRowKey().getName());
                    }
                }
                break;
            case "GAME":
                ImmutableTable<ServerInfo, Integer, Integer> sortedGame = sortTableGame(serverPlayers);
                for(Table.Cell<ServerInfo, Integer, Integer> cell : sortedGame.cellSet()){
                    if(cell == null || cell.getRowKey() == null || cell.getColumnKey() == null || cell.getValue() == null) continue;
                    if(cell.getColumnKey() < cell.getValue() && LCBungee.getGameCoordinator().getStateFromServerName(cell.getRowKey().getName()).equals(GameState.ACCEPTING_PLAYERS)) {
                        return LCAPI.getServerManager().getServerSilent(cell.getRowKey().getName());
                    }
                }
                break;
        }
        return null;
    }

    public ImmutableTable<ServerInfo, Integer, Integer> sortTableLobby(Table<ServerInfo, Integer, Integer> table){
        Ordering<Table.Cell<ServerInfo, Integer, Integer>> comparator = new Ordering<Table.Cell<ServerInfo, Integer, Integer>>() {
            @Override
            public int compare(@Nullable Table.Cell<ServerInfo, Integer, Integer> cell1, @Nullable Table.Cell<ServerInfo, Integer, Integer> cell2) {
                if(cell1 == null || cell2 == null || cell1.getRowKey() == null || cell2.getRowKey() == null) return 0;
                Server serv1 = LCAPI.getServerManager().createServerSilent(cell1.getRowKey().getName(), null);
                Server serv2 = LCAPI.getServerManager().createServerSilent(cell2.getRowKey().getName(), null);
                if(serv1 == null || serv2 == null) return 0;

                return serv2.getNumber().compareTo(serv1.getNumber());
            }
        };

        ImmutableTable.Builder<ServerInfo, Integer, Integer> sortedBuilder = ImmutableTable.builder(); // preserves insertion order
        comparator.reverse().sortedCopy(table.cellSet()).forEach(sortedBuilder::put);

        return sortedBuilder.build();
    }

    public ImmutableTable<ServerInfo, Integer, Integer> sortTableGame(Table<ServerInfo, Integer, Integer> table){
        Ordering<Table.Cell<ServerInfo, Integer, Integer>> comparator = new Ordering<Table.Cell<ServerInfo, Integer, Integer>>() {
            @Override
            public int compare(@Nullable Table.Cell<ServerInfo, Integer, Integer> cell1, @Nullable Table.Cell<ServerInfo, Integer, Integer> cell2) {
                if(cell1 == null || cell2 == null || cell1.getColumnKey() == null || cell2.getColumnKey() == null) return 0;
                return cell1.getColumnKey().compareTo(cell2.getColumnKey());
                //return serv2.getNumber().compareTo(serv1.getNumber());
            }
        };

        ImmutableTable.Builder<ServerInfo, Integer, Integer> sortedBuilder = ImmutableTable.builder(); // preserves insertion order
        comparator.reverse().sortedCopy(table.cellSet()).forEach(sortedBuilder::put);

        return sortedBuilder.build();
    }
}
}