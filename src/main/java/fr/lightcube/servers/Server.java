package fr.lightcube.servers;

import fr.lightcube.utils.Log;
import lombok.Getter;

/**
 * Created by Nitorac on 06/05/2017.
 */
public class Server {

    @Getter private Integer number;
    @Getter private String group;
    @Getter private Integer maxPlayers;

    public Server(String group, int number, Integer maxPlayers){
        setGroup(group);
        setNumber(number);
        setMaxPlayers(maxPlayers);
    }

    public Server(Server server){
        this(server.getGroup(), server.getNumber(), null);
    }

    public Server(String savedString) {
        this(savedString, null);
    }

    public Server(String savedString, Integer maxPlayers){
        String[] splitted = savedString.split("-#");

        if(splitted.length != 2){
            Log.ERROR.sendLog("Le string donné est incorrect : " + savedString);
            splitted = new String[]{"", "-1"};
        }
        setGroup(splitted[0]);
        setNumber(Integer.parseInt(splitted[1]));
        setMaxPlayers(maxPlayers);
    }

    private void setGroup(String group){
        this.group = group;
    }

    private void setNumber(Integer number){
        this.number = number;
    }

    private void setMaxPlayers(Integer max){
        this.maxPlayers = max;
    }

    @Override
    public Server clone(){
        return new Server(group, number, maxPlayers);
    }

    @Override
    public String toString(){
        return getGroup() + "-#" + String.valueOf(getNumber());
    }

    @Override
    public boolean equals(Object obj){
        if(obj instanceof Server){
            Server server2test = (Server)obj;
            if(server2test.getGroup().equals(getGroup()) && server2test.toString().equals(toString()) && server2test.getNumber() == getNumber()){
                return true;
            }
        }
        return false;
    }
}