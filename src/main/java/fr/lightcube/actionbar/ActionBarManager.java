package fr.lightcube.actionbar;

import fr.lightcube.LCSpigot;
import fr.lightcube.utils.CommonUtilsSpigot;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Nitorac on 04/06/2017.
 */
public class ActionBarManager {

    public ActionBarManager(){

    }

    public void sendMessageAllPlayers(String msg){
        CommonUtilsSpigot.getOnlinePlayers().forEach(pl -> sendMessage(pl, msg));
    }

    public void sendMessageAllPlayers(String msg, int duration){
        CommonUtilsSpigot.getOnlinePlayers().forEach(pl -> sendMessage(pl, msg, duration));
    }

    public void sendMessage(Player player, String msg){
        ActionBarEvent actionBarMessageEvent = new ActionBarEvent(player, msg);
        LCSpigot.getInstance().getServer().getPluginManager().callEvent(actionBarMessageEvent);
        if (actionBarMessageEvent.isCancelled())
            return;

        CraftPlayer cPlayer = (CraftPlayer)player;
        String string = ChatColor.translateAlternateColorCodes('&', msg);
        IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + string + "\"}");
        PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte)2);
        cPlayer.getHandle().playerConnection.sendPacket(ppoc);
    }

    // DISPARITION DE L'ACTION BAR : 3 secondes
    public void sendMessage(final Player player, final String message, int duration) {
        sendMessage(player, message);
        int[] durationLoop = {duration};
        new BukkitRunnable(){
            @Override
            public void run(){
                if(durationLoop[0] == 0){
                    sendMessage(player, "   ");
                    this.cancel();
                    return;
                }
                if(durationLoop[0] % 2 == 0) sendMessage(player, message);

                durationLoop[0]--;
            }
        }.runTaskTimer(LCSpigot.getInstance(), 0L, 20L);
    }
}
