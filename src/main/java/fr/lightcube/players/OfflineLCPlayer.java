package fr.lightcube.players;

import fr.lightcube.rank.Rank;

/**
 * Created by Nitorac on 21/03/2017.
 */
public abstract class OfflineLCPlayer {

    public String name;
    public long seen;
    public boolean isOnline;
    public String connectedServer;
    public boolean isMuted;
    public boolean isBanned;
    public Rank rank;

    public OfflineLCPlayer(String name) {
        this.name = name;
    }
}
