package fr.lightcube.players;

import fr.lightcube.LCAPI;
import fr.lightcube.guild.Guild;
import fr.lightcube.rank.Rank;
import fr.lightcube.servers.Server;
import fr.lightcube.utils.Log;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Created by Nitorac on 21/03/2017.
 */
public class LCPlayer {

    private UUID uuid;
    private String baseName;
    private String nickName;
    private long lastSeen;
    private long firstSeen;
    private Rank rank;
    private boolean msgToggle;
    private Guild guild;

    public LCPlayer(Player p) {
        this.baseName = p.getName();
        this.uuid = p.getUniqueId();
        reloadAllPlayerInfos();
    }

    public LCPlayer(ProxiedPlayer p) {
        this.baseName = p.getName();
        this.uuid = p.getUniqueId();
        reloadAllPlayerInfos();
    }

    public LCPlayer(String baseName, UUID uuid) {
        this.baseName = baseName;
        this.uuid = uuid;
        reloadAllPlayerInfos();
    }

    public void reloadAllPlayerInfos(){
        try {
            ResultSet request = LCAPI.getDBManager().getResultPreStat("SELECT * FROM players WHERE UUID = ?", getUniqueId().toString());
            if(request.next()){
                this.rank = LCAPI.getRankManager().getRankFromID(request.getInt("RANK"));
                this.nickName = request.getString("NICKNAME");
                this.lastSeen = request.getLong("LAST_LOGIN");
                this.firstSeen = request.getLong("FIRST_LOGIN");
                this.msgToggle = request.getBoolean("MSG_TOGGLE");
                //this.guild = LCAPI.getGuildManager().
            }else{
                // @Todo Load default chat !!!! and handle default bugged player !
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void reloadRank(){
        try {
            ResultSet rankDb = LCAPI.getDBManager().getResultPreStat("SELECT RANK FROM players WHERE UUID = ?", getUniqueId().toString());
            if(rankDb.next()){
                this.rank = LCAPI.getRankManager().getRankFromID(rankDb.getInt("RANK"));
            }else{
                // @Todo Load default chat !!!!
            }
            String exceptionCheck = rank.getName();
        } catch (Exception e) {
            Log.ERROR.sendLog("Impossible de récupérer le rang de la base de données (" + getName() + ")");
            e.printStackTrace();
        }
    }

    public UUID getUniqueId() {
        return uuid;
    }

    public String getName() {
        return baseName;
    }

    public String getNickName(){
        return nickName;
    }

    public void setNickName(String nick) throws SQLException{
        LCAPI.getDBManager().update("UPDATE players SET NICKNAME = ? WHERE UUID = ?", nick, getUniqueId().toString());
        this.nickName = nick;
    }

    public boolean getMsgToggle(){
        return msgToggle;
    }

    public void setMsgToggle(boolean toggle) throws SQLException{
        LCAPI.getDBManager().update("UPDATE players SET MSG_TOGGLE = ? WHERE UUID = ?", toggle, getUniqueId().toString());
        this.msgToggle = toggle;
    }

    public long getFirstSeen(){
        return firstSeen;
    }

    public long getLastSeen() {
        return lastSeen;
    }

    public void setOnline(boolean online){
        LCAPI.getDBManager().updateSilent("UPDATE players SET IS_ONLINE = ? WHERE UUID = ?", online, getUniqueId().toString());
    }

    public boolean isOnline() {
        try {
            ResultSet rs = LCAPI.getDBManager().getResultPreStat("SELECT * FROM players WHERE UUID = ?", getUniqueId().toString());
            if(rs.next()){
                return rs.getBoolean("IS_ONLINE");
            }
            throw new Exception();
        } catch (Exception e) {
            Log.ERROR.sendLog("Impossible de récupérer le IS_ONLINE de la base de données (" + getName() + ")");
            e.printStackTrace();
            return false;
        }
    }

    public Server getServer() {
        try {
            ResultSet rankDb = LCAPI.getDBManager().getResultPreStat("SELECT * FROM players WHERE UUID = ?", getUniqueId().toString());
            if(rankDb.next()){
                return LCAPI.getServerManager().getServer(rankDb.getString("CUR_SERVER"));
            }
            throw new Exception();
        } catch (Exception e) {
            Log.ERROR.sendLog("Impossible de récupérer le IS_ONLINE de la base de données (" + getName() + ")");
            e.printStackTrace();
            return null;
        }
    }

    public String getFullPrefix(){
        return ChatColor.translateAlternateColorCodes('&', getRank().getPrefix() + getName() + "§r");
    }

    public Rank getRank(){
        return rank;
    }

    /*public void setGuild(Guild g) throws SQLException {
        this.guild = g;
        LCAPI.getDBManager().update("UPDATE players SET GUILD_UUID = ? WHERE UUID = ?", g.(), getUniqueId().toString());
    }

    public void removeGuild() throws SQLException {
        this.guild = null;
        LCAPI.getDBManager().update("UPDATE players SET GUILD_UUID = ? WHERE UUID = ?", "", getUniqueId().toString());
    }*/
}