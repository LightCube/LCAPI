package fr.lightcube.players;

import fr.lightcube.LCAPI;
import fr.lightcube.db.Tables;
import fr.lightcube.utils.Log;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Nitorac on 21/03/2017.
 */
public class PlayerManager {
    public static ArrayList<LCPlayer> lcPlayers;

    public PlayerManager() {
        lcPlayers = new ArrayList<>();
    }

    public ArrayList<LCPlayer> getLCPlayers() {
        return new ArrayList<>(lcPlayers);
    }

    public LCPlayer getLCPlayer(Player player) {
        if(player == null) return null;
        return getLCPlayer(player.getUniqueId());
    }

    public int getPlayersNumber(){
        return lcPlayers.size();
    }

    public LCPlayer getLCPlayer(UUID player) {
        if(player == null) return null;
        return lcPlayers.stream().filter(p -> p.getUniqueId().compareTo(player) == 0).findFirst().orElse(null);
    }

    public void reloadPlayer(Player p){
        Log.DEBUG.sendLog("Le joueur " + p.getName() + "se reload");
        getLCPlayer(p).reloadAllPlayerInfos();
    }

    public void reloadPlayer(ProxiedPlayer p){
        getLCPlayer(p).reloadAllPlayerInfos();
    }

    public LCPlayer getLCPlayer(ProxiedPlayer player) {
        if(player == null) return null;
        return getLCPlayer(player.getUniqueId());
    }

    public void reset(){
        lcPlayers.clear();
    }

    ////////////////////////////////////////////
    // Vérifie si le joueur existe dans la DB //
    ////////////////////////////////////////////
    public boolean existInDB(Player p) throws SQLException {
        ResultSet rs = LCAPI.getDBManager().getResultPreStat("SELECT * FROM players WHERE UUID = ?", p.getUniqueId().toString());
        return rs.next();
    }

    ////////////////////////////////////////////
    // Vérifie si le joueur existe dans la DB //
    ////////////////////////////////////////////
    public boolean existInDB(ProxiedPlayer p) throws SQLException {
        ResultSet rs = LCAPI.getDBManager().getResultPreStat("SELECT * FROM players WHERE UUID = ?", p.getUniqueId().toString());
        return rs.next();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Ajoute les joueurs à la DB si c'est leur première connexion, sinon met à jour les attributs LAST_LOGIN & IS_ONLINE & NAME//
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void addPlayer(Player p) {
        lcPlayers.add(new LCPlayer(p));
    }

    public void addPlayer(ProxiedPlayer p) {
        try {
            if(!existInDB(p)){
                createLCPlayer(p);
            }else{
                lcPlayers.add(new LCPlayer(p));
                LCAPI.getDBManager().update("UPDATE players SET IS_ONLINE = ?, LAST_LOGIN = ?, NAME = ? WHERE UUID = ?", true, Instant.now().toEpochMilli(), p.getName(), p.getUniqueId().toString());
            }
        }catch(Exception e){
            Log.ERROR.sendLog("Impossible de créer le joueur " + p.getDisplayName());
            Log.ERROR.sendLog(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////
    // Retire le joueur de la liste et passe son attribut IS_ONLINE en false sur la DB //
    /////////////////////////////////////////////////////////////////////////////////////
    public void removePlayer(Player p) {
        lcPlayers.stream().filter(lc -> lc.getUniqueId().compareTo(p.getUniqueId()) == 0).findFirst().ifPresent(lcPlayer -> lcPlayers.remove(lcPlayer));
    }

    /////////////////////////////////////////////////////////////////////////////////////
    // Retire le joueur de la liste et passe son attribut IS_ONLINE en false sur la DB //
    /////////////////////////////////////////////////////////////////////////////////////
    public void removePlayer(ProxiedPlayer p) {
        try {
            lcPlayers.stream().filter(lc -> lc.getUniqueId().compareTo(p.getUniqueId()) == 0).findFirst().ifPresent(lcPlayer -> {
                lcPlayer.setOnline(false);
                lcPlayers.remove(lcPlayer);
            });
        } catch (Exception e) {
            Log.ERROR.sendLog("Impossible de retirer le joueur " + p.getDisplayName());
            Log.ERROR.sendLog(e.getLocalizedMessage());
        }
    }

    public void createLCPlayer(ProxiedPlayer p) {
        try {
            LCAPI.getDBManager().addRow(Tables.PLAYERS,
                p.getUniqueId().toString(),
                p.getName(),
                "",
                0,
                0,
                0,
                0,
                0,
                true,
                "",
                Instant.now().toEpochMilli(),
                Instant.now().toEpochMilli(),
                true,
                ""
                //false
            );
            lcPlayers.add(new LCPlayer(p));
        } catch (Exception e) {
            Log.ERROR.sendLog("Impossible de créer le joueur " + p.getName() + " a cause de : " + e.getMessage());
        }
    }
}