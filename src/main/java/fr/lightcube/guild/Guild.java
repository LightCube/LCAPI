package fr.lightcube.guild;

/**
 * Created by montenvers on 31/05/2017.
 */
public class Guild {
    /*@Getter private UUID uuid;
    @Getter private String name;
    @Getter private UUID leader;
    @Getter private int level;
    @Getter private int xp;
    @Getter private int coins;

    private Guild(UUID uuid, String name, UUID leader, int level, int xp, int coins){

    }

    public static Guild fromUUID(UUID uuid) throws GuildNotFoundException{
        ResultSet guildPointer = LCAPI.getDBManager().getResultPreStat("SELECT * FROM players WHERE UUID = ?",uuid.toString());

    }

    public static Guild fromPlayer(LCPlayer p) throws GuildNotFoundException{
        try {
            ResultSet guildPointer = LCAPI.getDBManager().getResultPreStat("SELECT * FROM players WHERE UUID = ?", p.getUniqueId().toString());
            if(!guildPointer.next()){
                throw new GuildNotFoundException();
            }

            return Guild.fromUUID(UUID.fromString(guildPointer.getString("GUILD_UUID")));
        } catch (SQLException e) {
            Log.ERROR.sendLog("Imossible d'exécuter de créer la guilde avec la bdd : " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public void reloadGuild() throws SQLException{
        ResultSet guild = LCAPI.getDBManager().getResultPreStat("SELECT * FROM guilds WHERE UUID = ?", getUniqueId().toString());

        this.name = guild.getString("NAME");
        this.leader = UUID.fromString(guild.getString("UUID"));
        this.xp = guild.getInt("XP");
        this.level = guild.getInt("LEVEL");
        this.coins = guild.getInt("COINS");
    }

    public UUID getUniqueId(){
        return uuid;
    }

    public void setName(String name){
        LCAPI.getDBManager().updateSilent("UPDATE guilds SET NAME = ? WHERE UUID = ?", name, getUniqueId().toString());
        this.name = name;
    }

    public void setLeader(UUID leader){
        LCAPI.getDBManager().updateSilent("UPDATE guilds SET LEADER = ? WHERE UUID = ?", leader.toString(), getUniqueId().toString());
        this.leader = leader;
    }

    public void setXp(int xp){
        LCAPI.getDBManager().updateSilent("UPDATE guilds SET XP = ? WHERE UUID = ?", xp, getUniqueId().toString());
        this.xp = xp;
    }

    public void setLevel(int level){
        LCAPI.getDBManager().updateSilent("UPDATE guilds SET XP = ? WHERE UUID = ?", level, getUniqueId().toString());
        this.level = level;
    }

    public void setCoins(int coins){
        LCAPI.getDBManager().updateSilent("UPDATE guilds SET COINS = ? WHERE UUID = ?", coins, getUniqueId().toString());
        this.coins = coins;
    }*/
}
