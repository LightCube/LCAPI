package fr.lightcube.utils;

import fr.lightcube.LCAPI;
import fr.lightcube.LCSpigot;
import fr.lightcube.redis.RedisManager;
import fr.lightcube.slack.Severity;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by Nitorac on 05/04/2017.
 */
public class CommonUtilsSpigot extends CommonUtils {

    public static void reloadWarp(){
        LCSpigot.getWarpManager().readWarpsFromFile();
    }

    public static void reloadRank(){
        LCAPI.getRankManager().reloadRanks();
        CommonUtilsSpigot.getOnlinePlayers().forEach(player -> LCAPI.getPlayerManager().getLCPlayer(player).reloadRank());
    }

    public static void reloadPlayers(){
        CommonUtilsSpigot.getOnlinePlayers().forEach(player -> LCAPI.getPlayerManager().reloadPlayer(player));
    }

    public static void reloadPerm(){
        LCAPI.getRedisManager().sendMessage(RedisManager.ReceiverType.BOTH, "Permissions", "Reload");
    }

    public int getPort(){
        return Bukkit.getServer().getPort();
    }

    public void runAsync(Runnable runnable) {
        LCSpigot.getInstance().getServer().getScheduler().runTaskAsynchronously(LCSpigot.getInstance(), runnable);
    }

    public void runTaskLater(Runnable runnable, long delayTick) {
        LCSpigot.getInstance().getServer().getScheduler().runTaskLater(LCSpigot.getInstance(), runnable, delayTick);
    }

    public void runTaskTimer(Runnable runnable, long delayTick, long repeatTick) {
        LCSpigot.getInstance().getServer().getScheduler().runTaskTimer(LCSpigot.getInstance(), runnable, delayTick, repeatTick);
    }

    public void dispatchCommand(String s){
        Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), s);
    }

    public static ArrayList<String> getOnlinePlayerNames(){
        ArrayList<String> result = new ArrayList<>();
        Bukkit.getServer().getOnlinePlayers().forEach(player -> result.add(player.getName()));
        return result;
    }

    public static CommandSender getConsoleSender(){
        return Bukkit.getServer().getConsoleSender();
    }

    public void runTask(Runnable runnable){
        Bukkit.getScheduler().runTask(LCSpigot.getInstance(), runnable);
    }

    @Deprecated
    public static void sendToPlayerRankFilter(String message, int rankID){
        getOnlinePlayers().stream().filter(player -> LCAPI.getPlayerManager().getLCPlayer(player).getRank().getRankID() >= rankID).forEach(player -> player.sendMessage(message));
    }

    public static void sendToPlayerPermFilter(String message, String perm) {
        CommonUtilsSpigot.getOnlinePlayers().stream().filter(player -> perm == null || player.hasPermission(perm)).forEach(player -> player.sendMessage(message));
    }

    public static void sendToAllPlayers(String message){ sendToPlayerPermFilter(message, null);}

    public static Collection<? extends Player> getOnlinePlayers(){
        return Bukkit.getOnlinePlayers();
    }

    public static Collection<? extends Player> getOnlinePlayersExcluding(Player p){
        return Bukkit.getOnlinePlayers().stream().filter(player -> player.getUniqueId().compareTo(p.getUniqueId()) != 0).collect(Collectors.toList());
    }

    public void safeShutdown(String reason, boolean alert) {
        Log.WARN.sendLog("Shutdown du serveur ! Raison : " + reason);
        if(alert){
            try {
                LCAPI.getSlackManager().sendMsg("2_haut-staff", "@channel Le serveur " + LCAPI.getServerManager().getCurrentServer().toString() + " s'est fermé !\nRaison : " + reason, Severity.ERROR, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Bukkit.getOnlinePlayers().forEach(p -> p.kickPlayer("Un problème est survenu, nous sommes en train de le régler !"));
        Server s = LCSpigot.getInstance().getServer();
        s.savePlayers();

        s.getWorlds().forEach(world -> {
            world.save();
            s.unloadWorld(world, true);
        });

        s.shutdown();
    }

    public static List<String> partial(String token, Collection<String> from) {
        return StringUtil.copyPartialMatches(token, from, new ArrayList<>(from.size()));
    }

    public static UUID resolvePlayer(CommandSender sender, String arg){
        return resolvePlayer(sender, arg, false);
    }

    public static UUID resolvePlayer(CommandSender sender, String arg, boolean silent) {
        arg = arg.toLowerCase();

        // see if it resolves to a single player name
        List<Player> players = LCSpigot.getInstance().getServer().matchPlayer(arg);
        if (players.size() == 1) {
            return players.get(0).getUniqueId();
        } else if (players.size() > 1) {
            if(!silent){
                sender.sendMessage(ChatColor.RED + "Le pseudo " + ChatColor.WHITE + arg + ChatColor.RED + " est ambigüe.");
            }
            return null;
        }

        UUID uuid = Utils.fromString(arg);
        if(uuid != null){
            return uuid;
        }

        if(!silent) {
            sender.sendMessage(ChatColor.RED + "Impossible d'interpréter: " + ChatColor.WHITE + arg);
            sender.sendMessage(ChatColor.RED + "Vous devez donner le UUID ou le nom d'un joueur en ligne.");
        }
        return null;
    }
}