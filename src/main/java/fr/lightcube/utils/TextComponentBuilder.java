package fr.lightcube.utils;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Nitorac on 07/03/2017.
 */
public class TextComponentBuilder {

    private TextComponent root = new TextComponent("");

    public TextComponentBuilder() {
        root.setText("");
    }

    public TextComponentBuilder(String baseText) {
        root.setText(baseText);
    }

    public TextComponentBuilder(TextComponent textComponent) {
        root = textComponent;
    }

    public TextComponentBuilder(BaseComponent... extras) {
        root.setExtra(new ArrayList<>(Arrays.asList(extras)));
    }

    public TextComponentBuilder setColor(ChatColor color) {
        root.setColor(color);
        return this;
    }

    public TextComponentBuilder setBold(Boolean bold) {
        root.setBold(bold);
        return this;
    }

    public TextComponentBuilder setItalic(Boolean italic) {
        root.setItalic(italic);
        return this;
    }

    public TextComponentBuilder setUnderlined(Boolean underlined) {
        root.setUnderlined(underlined);
        return this;
    }

    public TextComponentBuilder setStrikethrough(Boolean strikethrough) {
        root.setStrikethrough(strikethrough);
        return this;
    }

    public TextComponentBuilder setObfuscated(Boolean obfuscated) {
        root.setObfuscated(obfuscated);
        return this;
    }

    public TextComponentBuilder setInsertion(String insertion) {
        root.setInsertion(insertion);
        return this;
    }

    public TextComponentBuilder setClickEvent(ClickEvent clickEvent) {
        root.setClickEvent(clickEvent);
        return this;
    }

    public TextComponentBuilder setHoverEvent(HoverEvent hoverEvent) {
        root.setHoverEvent(hoverEvent);
        return this;
    }

    public TextComponentBuilder addExtra(BaseComponent comp){
        root.addExtra(comp);
        return this;
    }

    public TextComponentBuilder addExtra(String text){
        root.addExtra(text);
        return this;
    }

    public TextComponent build(){
        return new TextComponent(root);
    }
}
