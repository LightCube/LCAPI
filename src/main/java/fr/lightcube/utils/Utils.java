package fr.lightcube.utils;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.google.common.collect.Maps;
import fr.lightcube.configuration.LCYamlConfiguration;
import org.bukkit.entity.Player;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.*;

/**
 * Created by Nitorac on 21/03/2017.
 */
public class Utils {

    /******************************************
     *          Constructeur permettant la définition de vars static par une méthode non-static
     */
    public Utils() throws Exception{
        pluginLoc = new File(new File(".").getCanonicalPath() + File.separator + "plugins");
        serverLoc = pluginLoc.getParentFile();
        rootLoc = serverLoc.getParentFile();
    }

    //  Dossier du serveur actuel ( /LightCubeServer/build ou /LightCubeServer/detente ou /LightCubeServer/dev )
    public static File serverLoc;

    // Dossier plugins du serveur actuel ( /LightCubeServer/build/plugins ou /LightCubeServer/detente/plugins ou /LightCubeServer/dev/plugins )
    public static File pluginLoc;

    // Dossier de l'ensemble des serveurs ( /LightCubeServer )
    public static File rootLoc;

    // Fichier de configuration de Bungee
    public static LCYamlConfiguration bungeeConfig;

    public static void formatError(Log state, Exception e) {
        state.sendLog(e.getLocalizedMessage());
        Arrays.stream(e.getStackTrace()).forEach(st -> state.sendLog(st.toString()));
    }

    @SafeVarargs
    public static ArrayList<String> mergeArraysString(ArrayList<String>... obj){
        ArrayList<String> combined = new ArrayList<>();
        Arrays.stream(obj).forEach(combined::addAll);
        return combined;
    }

    public static String removeLastChar(String s) {
        return (s.length() > 0) ? s.substring(0, s.length() - 1) : "";
    }

    public static List<String> filterTabCompletions(List<String> list, String prefix) {
        final List<String> result = new ArrayList<>();

        list.stream().filter(s -> s.toLowerCase().startsWith(prefix.toLowerCase())).forEach(result::add);

        return result;
    }

    public static void writeObject(DataOutputStream dos, Object obj){
        try {
            switch (obj.getClass().getSimpleName().toLowerCase()) {
                case "integer":
                case "int":
                    dos.writeInt((int) obj);
                    break;
                case "boolean":
                    dos.writeBoolean((boolean) obj);
                    break;
                case "byte":
                    dos.writeByte((byte) obj);
                    break;
                case "string":
                    dos.writeUTF((String) obj);
                    break;
                case "double":
                    dos.writeDouble((double) obj);
                    break;
                case "short":
                    dos.writeShort((short) obj);
                    break;
                case "long":
                    dos.writeLong((long) obj);
                    break;
                case "float":
                    dos.writeFloat((float) obj);
                    break;
            }
        }catch (IOException ex){
            Log.ERROR.sendLog("Impossible d analyser l objet pour envoyer le pluginmessage : " + ex.getMessage());
        }
    }

    public static <K, V> Map<K, V> createMap(Object... objs){
        K key = null;
        V value = null;

        Map<K, V> result = Maps.newHashMap();

        try{
            for(int i = 0; i < objs.length ; i++){
                if(i%2 == 0){
                    key = (K)objs[i];
                }else{
                    value = (V)objs[i];
                }

                if(key != null && value != null){
                    result.put(key, value);
                    key = null;
                    value = null;
                }
            }
        }catch (Exception e){
            Log.ERROR.sendLog("Erreur de déclaration de Map");
            e.printStackTrace();
        }

        return result;
    }

    public static String capitalize(String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }

    public static int parseInt(String number, int defaultVal) {
        try {
            return Integer.parseInt(number);
        }catch(NumberFormatException e) {
            return defaultVal;
        }
    }

    public static String plurialize(int number, String base){
        return plurialize(number, base, null);
    }

    public static String plurialize(int number, String base, String exception){
        if(number < 2){
            return base;
        }else{
            if(exception == null){
                return base + "s";
            }else{
                return exception;
            }
        }
    }

    public static String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuilder buffer = new StringBuilder();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }
    }

    /******************  PROTOCOL LIB   METHODS !!******************/
    public static PacketContainer getPacket(EnumWrappers.PlayerInfoAction action, PlayerInfoData data) {
        return getPacket(action, Collections.singletonList(data));
    }

    public static PacketContainer getPacket(EnumWrappers.PlayerInfoAction action, List<PlayerInfoData> data) {
        PacketContainer packet = ProtocolLibrary.getProtocolManager().createPacket(PacketType.Play.Server.PLAYER_INFO);
        packet.getPlayerInfoAction().write(0, action);
        packet.getPlayerInfoDataLists().write(0, data);
        return packet;
    }

    public static void send(Player player, List<PacketContainer> packets) {
        try {
            for (PacketContainer packet : packets)
                ProtocolLibrary.getProtocolManager().sendServerPacket(player, packet, false);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /********************* REFLECTION UTILS ****************************/
    public static Object getFieldValue(Class clasz, Object declaredClass, String field) throws NoSuchFieldException, IllegalAccessException {
        return getField(clasz, field).get(declaredClass);
    }

    public static Object getFieldValueSilent(Class clasz, Object declaredClass, String field){
        try{
            return getFieldValue(clasz, declaredClass, field);
        }catch (NoSuchFieldException | IllegalAccessException e){
            e.printStackTrace();
        }
        return null;
    }

    public static Field getField(Class clasz, String field) throws NoSuchFieldException {
        Field f = clasz.getDeclaredField(field);
        f.setAccessible(true);
        return f;
    }

    public static Field getFieldSilent(Class clasz, String field){
        try {
            return getField(clasz, field);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setField(Field field, Object obj, Object value) throws IllegalAccessException {
        field.setAccessible(true);
        field.set(obj, value);
    }

    public static void setFieldSilent(Field field, Object obj, Object value){
        try{
            setField(field, obj, value);
        }catch (IllegalAccessException ignored) {}
    }

    public static UUID fromString(String uuid){
        if (uuid.length() == 32) {
            // expand UUIDs which do not have dashes in them
            uuid = uuid.substring(0, 8) + "-" + uuid.substring(8, 12) + "-" + uuid.substring(12, 16) +
                "-" + uuid.substring(16, 20) + "-" + uuid.substring(20, 32);
        }

        if (uuid.length() == 36) {
            // is of correct UUID length
            try {
                return UUID.fromString(uuid);
            } catch (IllegalArgumentException ex) {
                // ignored
            }
        }

        return null;
    }
}