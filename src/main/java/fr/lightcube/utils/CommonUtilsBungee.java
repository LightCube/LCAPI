package fr.lightcube.utils;

import fr.lightcube.LCAPI;
import fr.lightcube.LCBungee;
import fr.lightcube.configuration.LCYamlConfiguration;
import fr.lightcube.slack.Severity;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bukkit.util.StringUtil;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Nitorac on 05/04/2017.
 */
public class CommonUtilsBungee extends CommonUtils {

    public int getPort(){
        LCYamlConfiguration config = Utils.bungeeConfig;
        return (int)((LinkedHashMap)config.getList("listeners").get(0)).get("query_port");
    }

    public void runAsync(Runnable runnable) {
        LCBungee.getInstance().getProxy().getScheduler().runAsync(LCBungee.getInstance(), runnable);
    }

    public void runTaskLater(Runnable runnable, long delayTick) {
        LCBungee.getInstance().getProxy().getScheduler().schedule(LCBungee.getInstance(), runnable, delayTick * 50, TimeUnit.MILLISECONDS);
    }

    public void runTaskTimer(Runnable runnable, long delayTick, long repeatTick){
        LCBungee.getInstance().getProxy().getScheduler().schedule(LCBungee.getInstance(), runnable, delayTick * 50, repeatTick * 50, TimeUnit.MILLISECONDS);
    }

    public void runTask(Runnable runnable){
        runnable.run();
    }

    public void dispatchCommand(String s){
        ProxyServer ps = ProxyServer.getInstance();
        ps.getPluginManager().dispatchCommand(ps.getConsole(), s);
    }

    public static ArrayList<String> getOnlinePlayerNames(){
        ArrayList<String> result = new ArrayList<>();
        getOnlinePlayers().forEach(pl -> result.add(pl.getName()));
        return result;
    }

    public static void reloadRank(){
        LCAPI.getRankManager().reloadRanks();
        CommonUtilsBungee.getOnlinePlayers().forEach(player -> LCAPI.getPlayerManager().getLCPlayer(player).reloadRank());
    }

    public static void reloadPlayers(){
        CommonUtilsBungee.getOnlinePlayers().forEach(player -> LCAPI.getPlayerManager().reloadPlayer(player));
    }

    public static List<String> partial(String token, Collection<String> from) {
        return StringUtil.copyPartialMatches(token, from, new ArrayList<>(from.size()));
    }

    public static Collection<ProxiedPlayer> getOnlinePlayers(){
        return LCBungee.getInstance().getProxy().getPlayers();
    }

    public static UUID resolvePlayer(CommandSender sender, String arg){
        return resolvePlayer(sender, arg, false);
    }

    public static UUID resolvePlayer(CommandSender sender, String arg, boolean silent) {
        arg = arg.toLowerCase();

        // see if it resolves to a single player name
        ProxiedPlayer player = LCBungee.getInstance().getProxy().getPlayer(arg);
        if (player != null) {
            return player.getUniqueId();
        }

        UUID uuid = Utils.fromString(arg);
        if(uuid != null){
            return uuid;
        }

        if(!silent) {
            sender.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Impossible d'interpréter: " + ChatColor.WHITE + arg));
            sender.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Vous devez donner le UUID ou le nom d'un joueur en ligne."));
        }
        return null;
    }

    public void safeShutdown(String reason, boolean alert){
        Log.WARN.sendLog("Shutdown du serveur ! Raison : " + reason);
        if(alert){
            try {
                LCAPI.getSlackManager().sendMsg("2_haut-staff", "@channel Le Bungee s'est fermé !\nRaison: " + reason, Severity.ERROR, true);
            } catch (Exception e) {
                Log.FATAL.sendLog("Impossible d'avertir de la fermeture du serveur !");
                e.printStackTrace();
            }
        }
        /*if(LCBungee.getInstance() != null || LCBungee.getInstance().getProxy() != null){
            ProxyServer.getInstance().stop();
        }else{
            try {
                Class console = Class.forName("net.md_5.bungee.api.ProxyServer");
                Field instance = console.getDeclaredField("instance");
                instance.setAccessible(true);
                ProxyServer bungeeCord = (ProxyServer)instance.get(console);
                bungeeCord.stop();
            } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
                Log.FATAL.sendLog("Impossile de shutdown le serveur Bungee !!!");
                e.printStackTrace();
            }
        }*/
        ProxyServer.getInstance().stop();
    }

    public static void sendToPlayerPermFilter(String message, String perm) {
        CommonUtilsBungee.getOnlinePlayers().stream().filter(player -> LCAPI.getPermManager().hasPermissionBungee(LCAPI.getPlayerManager().getLCPlayer(player), perm)).forEach(player -> player.sendMessage(TextComponent.fromLegacyText(message)));
    }
}