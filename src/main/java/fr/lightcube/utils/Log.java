package fr.lightcube.utils;

import fr.lightcube.LCAPI;
import fr.lightcube.LCBungee;
import fr.lightcube.slack.Severity;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;

import java.util.Arrays;

/**
 * Created by Nitorac on 21/03/2017.
 */
public enum Log {
    INFO("\u00a7a[LCLog INFO] "),
    DEBUG("\u00a7b[LCLog DEBUG] "),
    WARN("\u00a7e[LCLog WARN] "),
    ERROR("\u00a7c[LCLog ERROR] "),
    FATAL("\u00a74[LCLog FATAL] ");

    String prefix;

    Log(String prefix) {
        this.prefix = prefix;
    }

    public void sendLog(boolean alert, Severity sev, String... str) {
        if(!this.equals(DEBUG) || LCAPI.getConfigManager().get(Csts.GENERAL_CONF).getBoolean("debug", true)){
            if(Csts.isBungee){
                if(LCBungee.getInstance().getProxy() == null){
                    try {
                        ProxyServer.getInstance().getConsole().sendMessage(new TextComponent(prefix + fromArrayToString(str)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else{
                    LCBungee.getInstance().getProxy().getConsole().sendMessage(new TextComponent(prefix + fromArrayToString(str)));
                }
            }else{
                Bukkit.getConsoleSender().sendMessage(prefix + fromArrayToString(str));
            }

            if(alert){
                try{
                    LCAPI.getSlackManager().sendMsg("a_server_logs", prefix + fromArrayToString(str), sev, false);
                }catch (Exception e){
                    Log.FATAL.sendLog("Impossible d'envoyer le log sur slack !");
                }
            }
        }
    }

    public void sendLog(String... str){
        sendLog(false, null, str);
    }

    public String fromArrayToString(String[] strs){
        String[] s = {""};
        Arrays.stream(strs).forEach(str -> s[0] += str);
        return s[0];
    }
}