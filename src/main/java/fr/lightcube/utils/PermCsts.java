package fr.lightcube.utils;

/**
 * Created by montenvers on 21/06/2017.
 */
public class PermCsts {
    public static String LCAPI_ALL = "lcapi.*";
    public static String LCAPI_IS_STAFF = "lcapi.staff";
    public static String LCAPI_MSGSPY_ALL = "lcapi.msgspy.*";
    public static String LCAPI_MSGSPY_BYPASS = "";
    public static String LCAPI_MSGSPY_BLOCK = "lcapi.msgspy.block";
    public static String LCAPI_MSG_ALL = "lcapi.*";
    public static String LCAPI_MSG_TOGGLE = "lcapi.msg.toggle";
    public static String LCAPI_MSG_BYPASS_COOLDOWN = "lcapi.msg.bypass-cooldown";
    public static String LCAPI_MSG_BYPASS_TOGGLE = "lcapi.msg.bypass-toggle";
    public static String LCAPI_COLORED_CHAT = "lcapi.coloredchat";
    /*************************COMMANDS*****************************/
    public static String LCAPI_STAFF_COMMAND = "lcapi.staff.ask";
    public static String LCAPI_LASTSEEN_COMMAND = "lcapi.lastseen";
    public static String LCAPI_MSG_COMMAND = "lcapi.msg";
    public static String LCAPI_MSGSPY_COMMAND = "lcapi.msgspy";
    public static String LCAPI_SETRANK_COMMAND = "lcapi.setrank";
    public static String LCAPI_WARP_COMMAND = "lcapi.warp";
    public static String LCAPI_BROADCAST_COMMAND = "lcapi.br";
    public static String LCAPI_RELOAD_COMMAND = "lcapi.reload";
    public static String LCAPI_LIGHTCOINS_COMMAND = "lcapi.lightcoins";
    public static String LCAPI_COINS_COMMAND = "lcapi.coins";
    public static String LCAPI_CONSOLE_COMMAND = "lcapi.console";
    public static String LCAPI_STAFFCHAT_COMMAND = "lcapi.sc";
    public static String LCAPI_INFO_COMMAND = "lcapi.info";

}
