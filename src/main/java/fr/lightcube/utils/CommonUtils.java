package fr.lightcube.utils;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * Created by Nitorac on 05/04/2017.
 */
public abstract class CommonUtils {

    public ImmutableMap<String, Integer> serverList;

    public abstract int getPort();

    public Map<String, Integer> getServerList(){
        if(serverList == null){
            ImmutableMap.Builder<String, Integer> result = new ImmutableMap.Builder<>();
            Utils.bungeeConfig.getSection("servers", false).forEach(s -> result.put(s, Utils.bungeeConfig.getInt("servers." + s + ".max-players")));
            serverList = result.build();
        }

        return serverList;
    }

    public abstract void runAsync(Runnable runnable);

    public abstract void runTaskLater(Runnable runnable, long delayTick);

    public abstract void runTaskTimer(Runnable runnable, long delayTick, long repeatTick);

    public abstract void dispatchCommand(String s);

    public abstract void runTask(Runnable run);

    public abstract void safeShutdown(String reason, boolean alert);
}
