package fr.lightcube.utils;

/**
 * Created by Nitorac on 21/03/2017.
 */
public class Csts {

    public static final String SHOP_API_KEY = "ab25334188ebefe3b0a92a09e0172ffe";

    public static boolean isBungee = false;
    public static final String SPLITTER = "µ~¤#";

    /*************************LOG*****************************/

    public static final String LOGERRORBDD = "§4Un problème est survenu (BDD) : ";
    public static final String LOGERRORREDIS = "§4Un problème est survenu (REDIS) : ";

    /*************************MSG*****************************/

    public static final String MSGERRORPLAYER = "§4Un problème est survenu, veuillez contacter un développeur.";

    /*************************RANKID*****************************/

    //> et < non valabes
    public static final Integer MIN_ID_VIP = 60;
    public static final Integer MAX_ID_VIP = 69;
    public static final Integer MIN_ID_STAFF = 70;
    public static final Integer MAX_ID_STAFF = 200;

    /*************************PREFIX*****************************/

    public static final String FRIEND_PREFIX = "§f[§6Light§bFriends§f]§7 » ";
    public static final String HUB_PREFIX = "§f[§6Light§bHub§f]§7 » ";
    public static final String COINS_PREFIX = "§f[§6Light§bCoins§f]§7 » ";
    public static final String INFO_PREFIX = "§f[§6Light§bInfo§f]§7 » ";
    public static final String MOD_PREFIX = "§f[§6Light§bMod§f]§7 » ";
    public static final String MSG_PREFIX = "§f[§6Light§bMsg§f]§7 » ";
    public static final String MSG_GAME = "§f[§6Light§bGame§f]§7 » ";
    public static final String MSGSPY_PREFIX = "§f[§6Light§bMsgSpy§f]§7 » ";
    public static final String PARTY_PREFIX = "§f[§6Light§bParty§f]§7 » ";
    public static final String SC_PREFIX = "§f[§6Light§bChat§f] §r";
    public static final String STAFF_PREFIX = "§f[§6Light§bStaff§f]§7 » ";

    /*************************AUTRES*****************************/

    public static final int MSG_COOLDOWN = 3;

    /*************************REDIS*****************************/
    public static final String REDIS_MAIN_CHANNEL = "lc";
    public static final String REDIS_BUNGEE = "rdsbun";
    public static final String REDIS_SPIGOT = "rdsspi";
    public static final String REDIS_ALL = "rdsall";
    public static final String REDIS_SPLITTER = "@L@MC@C@";

    /**************************STRINGS***************************/
    public static final String TABLE_PLAYERS = "players";
    public static final String TABLE_LINKSITE = "linksite";
    public static final String TABLE_GUILDS = "guilds";

    /**************************FILE CONFIGURATION NAMES***********/
    public static final String RANK_CONF = "rank.yml";
    public static final String GENERAL_CONF = "general.yml";
    public static final String WORLD_CONF = "world.yml";
    public static final String PERMS_CONF = "perms.yml";
    public static final String WARPS_CONF = "warps.yml";
}