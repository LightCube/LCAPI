package fr.lightcube.utils;

import com.google.common.collect.Lists;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Arrays;
import java.util.List;

/**
 * The MIT License (MIT)
 * Created on 31/01/2016.
 * Copyright (c) 2016 Vinetos
 */
public class JSONMessageCreator {

    private static final String[] colors = new String[]{
            "&0", "&1", "&2", "&3", "&4", "&5", "&6", "&7", "&8", "&9", "&a", "&b", "&c", "&d", "&e", "&f", "&k", "&l", "&m", "&n", "&o", "&r",
            "§0", "§1", "§2", "§3", "§4", "§5", "§6", "§7", "§8", "§9", "§a", "§b", "§c", "§d", "§e", "§f", "§k", "§l", "§m", "§n", "§o", "§r"
    };

    private JSONObject chatObject;

    @SuppressWarnings("unchecked")
    public JSONMessageCreator(String text, Color color, List<ChatFormat> formats) {
        chatObject = new JSONObject();
        chatObject.put("text", text);
        if (color != null) {
            chatObject.put("color", color.getColorString());
        }
        if (formats != null) {
            formats.forEach(format -> chatObject.put(format.getFormatString(), true));
        }
    }

    public JSONMessageCreator(String text) {
        this(text, Color.WHITE, Lists.newArrayList());
    }

    @SuppressWarnings("unchecked")
    public JSONMessageCreator addExtra(ChatExtra extraObject) {
        if (!chatObject.containsKey("extra")) {
            chatObject.put("extra", new JSONArray());
        }
        JSONArray extra = (JSONArray) chatObject.get("extra");
        extra.add(extraObject.toJSON());
        chatObject.put("extra", extra);
        return this;
    }

    public String toString() {
        return chatObject.toJSONString();
    }

    public static String stripColor(String input) {
        Arrays.stream(colors).forEach(s -> input.replaceAll(s, ""));
        return input;
    }

    public static class ChatExtra {
        private JSONObject chatExtra;

        @SuppressWarnings("unchecked")
        public ChatExtra(String text, Color color, List<ChatFormat> formats) {
            chatExtra = new JSONObject();
            chatExtra.put("text", text);
            chatExtra.put("color", color.getColorString());
            if (formats != null) {
                formats.forEach(format -> chatExtra.put(format.getFormatString(), true));
            }
        }

        @SuppressWarnings("unchecked")
        public ChatExtra setClickEvent(ClickEventType action, String value) {
            JSONObject clickEvent = new JSONObject();
            clickEvent.put("action", action.getTypeString());
            clickEvent.put("value", value);
            chatExtra.put("clickEvent", clickEvent);
            return this;
        }

        @SuppressWarnings("unchecked")
        public ChatExtra setHoverEvent(HoverEventType action, String value) {
            JSONObject hoverEvent = new JSONObject();
            hoverEvent.put("action", action.getTypeString());
            hoverEvent.put("value", value);
            chatExtra.put("hoverEvent", hoverEvent);
            return this;
        }

        public JSONObject toJSON() {
            return chatExtra;
        }

        public ChatExtra build() {
            return this;
        }
    }

    public static enum Color {
        WHITE("white"),
        YELLOW("yellow"),
        LIGHT_PURPLE("light_purple"),
        RED("red"),
        AQUA("aqua"),
        GREEN("green"),
        BLUE("blue"),
        DARK_GRAY("dark_gray"),
        GRAY("gray"),
        GOLD("gold"),
        DARK_PURPLE("dark_purple"),
        DARK_RED("dark_red"),
        DARK_AQUA("dark_aqua"),
        DARK_GREEN("dark_green"),
        DARK_BLUE("dark_blue"),
        BLACK("black");
        private final String color;

        Color(String color) {
            this.color = color;
        }

        String getColorString() {
            return color;
        }
    }

    public static enum ClickEventType {
        RUN_COMMAND("run_command"),
        SUGGEST_COMMAND("suggest_command"),
        OPEN_URL("open_url"),
        CHANGE_PAGE("change_page");

        private final String type;

        ClickEventType(String type) {
            this.type = type;
        }

        public String getTypeString() {
            return type;
        }
    }

    public static enum HoverEventType {
        SHOW_TEXT("show_text"),
        SHOW_ITEM("show_item"),
        SHOW_ACHIEVEMENT("show_achievement");
        private final String type;

        HoverEventType(String type) {
            this.type = type;
        }

        public String getTypeString() {
            return type;
        }
    }

    public static enum ChatFormat {
        BOLD("bold"),
        UNDERLINED("underlined"),
        STRIKETHROUGH("strikethrough"),
        ITALIC("italic"),
        OBFUSCATED("obfuscated");
        private final String format;

        ChatFormat(String format) {
            this.format = format;
        }

        public String getFormatString() {
            return format;
        }
    }

}