package fr.lightcube.configuration;

import java.util.ArrayList;


/**
 * Created by Nitorac on 29/03/2017.
 */
public class ConfigManager {

    private static ArrayList<LCYamlConfiguration> configs;

    public ConfigManager(){
        configs = new ArrayList<>();
    }

    /****************************************
     *          Permet d'ajouter un fichier de configuration dans la liste registered
     * @param path L'arborescence
     */
    public void registerConfig(String path){
        configs.add(new LCYamlConfiguration(path));
    }

    /**********************************
     *          Sauvegarde tous les fichiers de config qui ont été chargés
     */
    public void saveAll(){
        configs.stream().filter(LCYamlConfiguration::isLoaded).forEach(LCYamlConfiguration::save);
    }

    /**********************************
     *          Permet le chargement de tous les fichiers de config non chargés
     */
    public void loadAll(){
        configs.stream().filter(yml -> !yml.isLoaded()).forEach(yml -> {
            yml.load();
            configs.add(yml);
        });
    }

    public LCYamlConfiguration get(String name){
        return configs.stream().filter(yml -> yml.getName().toLowerCase().equals(name.toLowerCase())).findFirst().orElse(null);
    }

    /**********************************
     *          Permet de reload tous les fichiers de config (même ceux déjà chargés)
     */
    public void reloadAll(){
        configs.forEach(LCYamlConfiguration::load);
    }

    /**********************************
     *          Permet de récupérer une copie des fichiers de config registered
     */
    public ArrayList<LCYamlConfiguration> getConfigFiles(){
        return new ArrayList<>(configs);
    }
}