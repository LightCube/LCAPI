package fr.lightcube.configuration;

import fr.lightcube.LCAPI;
import fr.lightcube.utils.Log;
import org.apache.commons.io.FileUtils;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nitorac on 29/03/2017.
 */
public class LCYamlConfiguration extends YamlConfiguration{

    private File location;
    private boolean loaded = false;

    public LCYamlConfiguration(File loc){
        super();
        this.location = loc;
        load();
    }

    public LCYamlConfiguration(String path){
        super();
        this.location = new File(path);
        load();
    }

    /************************
     *          Sauvegarde la configuration dans le fichier
     */
    public void save(){
        try{
            super.save(location);
        }catch (Exception e){
            Log.ERROR.sendLog("Impossible d'écrire la config ", location.getName(), " (", e.getMessage(), ")");
        }
    }

    public void flush(){
        super.getKeys(false).forEach(key -> super.set(key, null));
    }

    /********************************
     *          Charge le fichier de config
     *          /!\ Le nom du fichier de sortie doit être le même que celui situé à la racine (voir rank.yml pour exemple)
     */
    public LCYamlConfiguration load() {
        if (location.exists()) {
            try {
                load(location);
                Log.INFO.sendLog(location.getName() + " charge ! {" + location.getCanonicalPath() + "}");
            } catch (Exception e) {
                Log.ERROR.sendLog("Configuration : " + e.getLocalizedMessage());
            }
            loaded = true;
        } else {
            try {
                location.getParentFile().mkdirs();
                InputStream jarURL = LCAPI.class.getResourceAsStream("/" + location.getName());
                FileUtils.copyInputStreamToFile(jarURL, location);
                load(location);
                Log.INFO.sendLog(location.getName() + " charge ! {" + location.getCanonicalPath() + "}");
                loaded = true;
                Log.INFO.sendLog("Configuration ", location.getName(), " ecrite sur le disque !");
            } catch (IOException | InvalidConfigurationException e) {
                Log.ERROR.sendLog("Configuration : " + e.getLocalizedMessage());
            }
        }
        return this;
    }

    public String getName(){
        return location.getName();
    }

    public YamlConfiguration getYAMLConfiguration(){
        return this;
    }

    public File getFile(){
        return location;
    }

    public ArrayList<String> getSection(String section, boolean getKeys) {
        return new ArrayList<>(super.getConfigurationSection(section).getKeys(getKeys));
    }

    public void setFirstTimeAndSave(String path, Object defaut){
        if(!super.contains(path)){
            super.set(path, defaut);
            save();
        }
    }

    //    /!\ QUELQUES OVERRIDE CI DESSOUS SONT DIRECTEMENT DÉFINIS DANS LA CLASSE YAMLCONFIGURATION DE BUKKIT /!\
    public List<String> getStringList(String path, List<String> defaut) {
        setFirstTimeAndSave(path, defaut);
        return super.getStringList(path);
    }

    public Integer getInt(String path, Integer defaut) {
        setFirstTimeAndSave(path, defaut);
        return super.getInt(path, defaut);
    }

    public List<Integer> getIntList(String path, List<Integer> defaut) {
        setFirstTimeAndSave(path, defaut);
        return super.getIntegerList(path);
    }

    public Boolean getBoolean(String path, Boolean defaut) {
        setFirstTimeAndSave(path, defaut);
        return super.getBoolean(path, defaut);
    }

    public List<Boolean> getBooleanList(String path, List<Boolean> defaut) {
        setFirstTimeAndSave(path, defaut);
        return super.getBooleanList(path);
    }

    public Long getLong(String path, Long defaut) {
        setFirstTimeAndSave(path, defaut);
        return super.getLong(path, defaut);
    }

    public List<Long> getLongList(String path, List<Long> defaut) {
        setFirstTimeAndSave(path, defaut);
        return super.getLongList(path);
    }

    public Float getFloat(String path, Float defaut) {
        setFirstTimeAndSave(path, defaut);
        return (float)super.getDouble(path, (double)defaut);
    }

    public List<Float> getFloatList(String path, List<Float> defaut) {
        setFirstTimeAndSave(path, defaut);
        return super.getFloatList(path);
    }

    public Short getShort(String path, Short defaut) {
        setFirstTimeAndSave(path, defaut);
        return (Short)super.get(path, defaut);
    }

    public List<Short> getShortList(String path, List<Short> defaut) {
        setFirstTimeAndSave(path, defaut);
        return super.getShortList(path);
    }

    public Double getDouble(String path, Double defaut) {
        setFirstTimeAndSave(path, defaut);
        return super.getDouble(path, defaut);
    }

    public List<Double> getDoubleList(String path, List<Double> defaut) {
        setFirstTimeAndSave(path, defaut);
        return super.getDoubleList(path);
    }


    public Object getObject(String path, Object defaut) {
        setFirstTimeAndSave(path, defaut);
        return super.get(path);
    }

    public List<?> getObjectList(String path, List<?> defaut) {
        setFirstTimeAndSave(path, defaut);
        return super.getList(path, defaut);
    }

    public void setObject(String path, Object value) {
        super.set(path, value);
    }

    public void reload() {
        try {
            this.load(location);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public boolean isLoaded(){
        return loaded;
    }
}
