package fr.lightcube.permissions;

import fr.lightcube.LCAPI;
import org.bukkit.configuration.ConfigurationSection;

import java.util.*;

/**
 * A class representing the global and world nodes attached to a player or group.
 */
public final class PermissionInfo {

    private final ConfigurationSection node;
    private final String groupType;

    PermissionInfo(ConfigurationSection node, String groupType) {
        this.node = node;
        this.groupType = groupType;
    }

    /**
     * Gets the list of groups this group/player inherits permissions from.
     *
     * @return The list of groups.
     */
    public List<Group> getGroups() {
        ArrayList<Group> result = new ArrayList<>();

        node.getStringList(groupType).forEach(key -> {
            Group group = LCAPI.getPermManager().getGroup(key);
            if (group != null) {
                result.add(group);
            }
        });

        return result;
    }

    /**
     * Gets a map of non-world-specific permission nodes to boolean values that this group/player defines.
     *
     * @return The map of permissions.
     */
    public Map<String, Boolean> getPermissions() {
        return LCAPI.getPermManager().getAllPerms(node.getName(), node.getCurrentPath());
    }

    /**
     * Gets a list of worlds this group/player defines world-specific permissions for.
     *
     * @return The list of worlds.
     */
    public Set<String> getServers() {
        if (node.getConfigurationSection("servers") == null) {
            return new HashSet<>();
        }
        return node.getConfigurationSection("servers").getKeys(false);
    }

    /**
     * Gets a map of server-specific permission nodes to boolean values that this group/player defines.
     *
     * @param server The name of the server.
     * @return The map of permissions.
     */
    public Map<String, Boolean> getServerPermissions(String server) {
        return LCAPI.getPermManager().getAllPerms(node.getName() + ":" + server, node.getName() + "/server/" + server);
    }

}
