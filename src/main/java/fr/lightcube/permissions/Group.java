package fr.lightcube.permissions;

import fr.lightcube.LCAPI;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * A class representing a permissions group.
 */
public final class Group {

    private final String name;
    private final int priority;

    Group(String name, int priority) {
        this.name = name;
        this.priority = priority;
    }

    public String getName() {
        return name;
    }


    public int getPriority(){
        return priority;
    }

    /**
     * @deprecated Use UUIDs instead.
     */
    @Deprecated
    public List<String> getPlayers() {
        ArrayList<String> result = new ArrayList<>();
        if (LCAPI.getPermManager().getNode("users") != null) {
            LCAPI.getPermManager().getNode("users").getKeys(false).forEach(user -> {
                ConfigurationSection node = LCAPI.getPermManager().getNode("users/" + user);
                for (String group : node.getStringList("groups")) {
                    if (name.equalsIgnoreCase(group) && !result.contains(user)) {
                        // attempt to determine the username
                        if (node.getString("name") != null) {
                            // converted node
                            result.add(node.getString("name"));
                        } else {
                            // unconverted node, or UUID node missing "name" element
                            result.add(user);
                        }
                        break;
                    }
                }
            });
        }
        return result;
    }

    public List<UUID> getPlayerUUIDs() {
        ArrayList<UUID> result = new ArrayList<UUID>();
        if (LCAPI.getPermManager().getNode("users") != null) {
            LCAPI.getPermManager().getNode("users").getKeys(false).forEach(user -> {
                UUID uuid;
                try {
                    uuid = UUID.fromString(user);
                } catch (IllegalArgumentException ex) {
                    return;
                }
                for (String group : LCAPI.getPermManager().getNode("users/" + user).getStringList("groups")) {
                    if (name.equalsIgnoreCase(group) && !result.contains(uuid)) {
                        result.add(uuid);
                        break;
                    }
                }
            });
        }
        return result;
    }

    public List<Player> getOnlinePlayers() {
        ArrayList<Player> result = new ArrayList<>();
        getPlayerUUIDs().forEach(uuid -> {
            Player player = Bukkit.getServer().getPlayer(uuid);
            if (player != null && player.isOnline()) {
                result.add(player);
            }
        });
        return result;
    }

    public PermissionInfo getInfo() {
        ConfigurationSection node = LCAPI.getPermManager().getNode("groups/" + name);
        if (node == null) {
            return null;
        }
        return new PermissionInfo(node, "inheritance");
    }

    @Override
    public boolean equals(Object o) {
        return !(o == null || !(o instanceof Group)) && name.equalsIgnoreCase(((Group) o).getName());
    }

    @Override
    public String toString() {
        return "Group{name=" + name + "}";
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

}