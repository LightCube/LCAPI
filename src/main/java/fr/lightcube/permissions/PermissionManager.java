package fr.lightcube.permissions;

import fr.lightcube.LCAPI;
import fr.lightcube.LCBungee;
import fr.lightcube.LCSpigot;
import fr.lightcube.players.LCPlayer;
import fr.lightcube.utils.*;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.util.FileUtil;

import java.io.*;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Nitorac on 24/05/2017.
 */
public class PermissionManager {

    public static final String HASNT_PERMS_MESSAGE = ChatColor.RED + "Vous n'avez pas la permission d'exécuter cette commande.";

    private final HashMap<UUID, PermissionAttachment> permissions = new HashMap<>();

    private final HashMap<UUID, HashMap<String, Boolean>> bungeePerms = new HashMap<>();

    private File configFile;
    private YamlConfiguration config;

    public boolean configLoadError = false;

    public PermissionManager(){
        configFile = new File(Utils.rootLoc, "perms.yml");
        saveDefaultConfig();
        reloadConfig();
    }

    public void init(){
        int count;
        if(!Csts.isBungee){
            CommonUtilsSpigot.getOnlinePlayers().forEach(player -> registerPlayer(LCAPI.getPlayerManager().getLCPlayer(player)));
            count = CommonUtilsSpigot.getOnlinePlayers().size();
        }else{
            CommonUtilsBungee.getOnlinePlayers().forEach(player -> registerPlayer(LCAPI.getPlayerManager().getLCPlayer(player)));
            count = CommonUtilsBungee.getOnlinePlayers().size();
        }

        LCAPI.getRedisManager().registerReceiver("PermissionManager", (subchannel, msg) -> {
            switch (subchannel){
                case "Permissions":
                    String[] splitted;
                    if((splitted = msg.split(Csts.SPLITTER, 4)).length == 4){
                        if(msg.startsWith("SetGroupPerm")){
                            String node = "permissions";
                            String group = splitted[1];
                            String perm = splitted[2];
                            Boolean value = Boolean.valueOf(splitted[3]);
                            if (perm.contains(":")) {
                                String server = perm.substring(0, perm.indexOf(':'));
                                perm = perm.substring(perm.indexOf(':') + 1);
                                node = "servers/" + server;
                            }
                            LCAPI.getPermManager().createNode("groups/" + group + "/" + node).set(perm, value);
                            LCAPI.getPermManager().refreshForGroup(group);

                        }else if(msg.startsWith("UnsetGroupPerm")){
                            String node = "permissions";
                            String group = splitted[1];
                            String perm = splitted[2];
                            if (perm.contains(":")) {
                                String server = perm.substring(0, perm.indexOf(':'));
                                perm = perm.substring(perm.indexOf(':') + 1);
                                node = "servers/" + server;
                            }
                            ConfigurationSection sec = LCAPI.getPermManager().createNode("groups/" + group + "/" + node);
                            sec.set(perm, null);
                            LCAPI.getPermManager().refreshForGroup(group);
                        }else if(msg.startsWith("PlayerSetGroup")){
                            UUID player = UUID.fromString(splitted[1]);
                            String group = splitted[2];
                            LCAPI.getPermManager().createNode("users/" + player).set("groups", Collections.singletonList(LCAPI.getPermManager().getGroup(group).getName()));
                            LCAPI.getPermManager().refreshForPlayer(player);
                        }else if(msg.startsWith("PlayerAddGroup")){
                            UUID player = UUID.fromString(splitted[1]);
                            String group = splitted[2];
                            List<String> list = LCAPI.getPermManager().createNode("users/" + player).getStringList("groups");
                            list.add(group);
                            LCAPI.getPermManager().getNode("users/" + player).set("groups", list);
                            LCAPI.getPermManager().refreshForPlayer(player);
                        }else if(msg.startsWith("PlayerRemoveGroup")){
                            UUID player = UUID.fromString(splitted[1]);
                            String group = splitted[2];
                            List<String> list = LCAPI.getPermManager().createNode("users/" + player).getStringList("groups");
                            list.remove(group);
                            LCAPI.getPermManager().getNode("users/" + player).set("groups", list);
                            LCAPI.getPermManager().refreshForPlayer(player);
                        }else if(msg.startsWith("SetPlayerPerm")){
                            String node = "permissions";
                            UUID player = UUID.fromString(splitted[1]);
                            String perm = splitted[2];
                            Boolean value = Boolean.valueOf(splitted[3]);
                            if (perm.contains(":")) {
                                String server = perm.substring(0, perm.indexOf(':'));
                                perm = perm.substring(perm.indexOf(':') + 1);
                                node = "servers/" + server;
                            }
                            LCAPI.getPermManager().createNode("users/" + player + "/" + node).set(perm, value);
                            LCAPI.getPermManager().refreshForPlayer(player);
                        }else if(msg.startsWith("UnsetPlayerPerm")){
                            String node = "permissions";
                            UUID player = UUID.fromString(splitted[1]);
                            String perm = splitted[2];
                            if (perm.contains(":")) {
                                String server = perm.substring(0, perm.indexOf(':'));
                                perm = perm.substring(perm.indexOf(':') + 1);
                                node = "servers/" + server;
                            }

                            ConfigurationSection sec = LCAPI.getPermManager().createNode("users/" + player + "/" + node);
                            sec.set(perm, null);
                            LCAPI.getPermManager().refreshForPlayer(player);
                        }
                    }else if(msg.startsWith("Reload")){
                        reloadConfig();
                        refreshPermissions();
                        Log.INFO.sendLog("Permissions rechargées !");
                    }
                    break;
            }
        });

        if (count > 0) {
            Log.INFO.sendLog("Enabled successfully, " + count + " online players registered");
        } else {
            // "0 players registered" sounds too much like an error
            Log.INFO.sendLog("Enabled successfully");
        }
    }

    public void disable(){
        CommonUtilsSpigot.getOnlinePlayers().forEach(player -> unregisterPlayer(LCAPI.getPlayerManager().getLCPlayer(player)));

        // Good day to you! I said good day!
        int count = CommonUtilsSpigot.getOnlinePlayers().size();
        if (count > 0) {
            Log.INFO.sendLog("Disabled successfully, " + count + " online players unregistered");
        }
    }

    public void onPlayerJoin(LCPlayer p) {
        debug("LCPlayer " + p.getName() + " joined, registering...");
        registerPlayer(p);

        Player player = LCSpigot.getInstance().getServer().getPlayer(p.getUniqueId());

        if (configLoadError && player.hasPermission("permissions.reload")) {
            configLoadError = false;
            player.sendMessage(ChatColor.RED + "[" + ChatColor.GREEN + "PermissionsBukkit" + ChatColor.RED + "] Your configuration is invalid, see the console for details.");
        }
    }

    public void onPlayerKick(LCPlayer p) {
        debug("LCPlayer " + p.getName() + " was kicked, unregistering...");
        unregisterPlayer(p);
    }

    public void onPlayerQuit(LCPlayer p) {
        debug("LCPlayer " + p.getName() + " quit, unregistering...");
        unregisterPlayer(p);
    }

    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_AIR) {
            return;
        }
        if (!event.getPlayer().hasPermission("permissions.build")) {
            event.setCancelled(true);
        }
    }

    public void onBlockPlace(BlockPlaceEvent event) {
        if (!event.getPlayer().hasPermission("permissions.build")) {
            event.setCancelled(true);
        }
    }

    public void onBlockBreak(BlockBreakEvent event) {
        if (!event.getPlayer().hasPermission("permissions.build")) {
            event.setCancelled(true);
        }
    }

    public FileConfiguration getConfig() {
        return config;
    }

    public boolean hasPermissionBungee(LCPlayer player, String permission){
        if(Csts.isBungee){
            HashMap<String, Boolean> playerPerms = bungeePerms.get(player.getUniqueId());
            if(playerPerms != null){
                return playerPerms.get(permission) != null && playerPerms.get(permission);
            }else{
                Log.ERROR.sendLog("Le joueur " + player.getName() + " est introuvable dans les permissions Bungee !");
            }
        }
        return false;
    }

    public void saveDefaultConfig() {
        if(!this.configFile.exists()) {
            String resourcePath = Utils.rootLoc + File.separator + "perms.yml";
            resourcePath = resourcePath.replace('\\', '/');
            InputStream in = getResource(resourcePath);
            if(in == null) {
                throw new IllegalArgumentException("The embedded resource '" + resourcePath + "' cannot be found in perms.yml");
            } else {
                File outFile = new File(resourcePath);
                int lastIndex = resourcePath.lastIndexOf(47);
                File outDir = new File(resourcePath.substring(0, lastIndex >= 0?lastIndex:0));
                if(!outDir.exists()) {
                    outDir.mkdirs();
                }

                try {
                    if(outFile.exists()) {
                        Log.WARN.sendLog("Could not save " + outFile.getName() + " to " + outFile + " because " + outFile.getName() + " already exists.");
                    } else {
                        OutputStream out = new FileOutputStream(outFile);
                        byte[] buf = new byte[1024];

                        int len;
                        while((len = in.read(buf)) > 0) {
                            out.write(buf, 0, len);
                        }

                        out.close();
                        in.close();
                    }
                } catch (IOException var10) {
                    Log.ERROR.sendLog("Could not save " + outFile.getName() + " to " + outFile + ": " + var10);
                }
            }
        }
    }

    public InputStream getResource(String filename){
        if(filename == null) {
            throw new IllegalArgumentException("Filename cannot be null");
        } else {
            try {
                URL url = this.getClass().getClassLoader().getResource(filename);
                if(url == null) {
                    return null;
                } else {
                    URLConnection connection = url.openConnection();
                    connection.setUseCaches(false);
                    return connection.getInputStream();
                }
            } catch (IOException var4) {
                return null;
            }
        }
    }

    public void reloadConfig() {
        config = new YamlConfiguration();
        config.options().pathSeparator('/');
        try {
            config.load(configFile);
        } catch (InvalidConfigurationException ex) {
            configLoadError = true;

            // extract line numbers from the exception if we can
            ArrayList<String> lines = new ArrayList<>();
            Pattern pattern = Pattern.compile("line (\\d+), column");
            Matcher matcher = pattern.matcher(ex.getMessage());
            while (matcher.find()) {
                String lineNo = matcher.group(1);
                if (!lines.contains(lineNo)) {
                    lines.add(lineNo);
                }
            }

            // make a nice message
            StringBuilder msg = new StringBuilder("Your configuration is invalid! ");
            if (lines.size() == 0) {
                msg.append("Unable to find any line numbers.");
            } else {
                msg.append("Take a look at line(s): ").append(lines.get(0));
                for (String lineNo : lines.subList(1, lines.size())) {
                    msg.append(", ").append(lineNo);
                }
            }
            Log.ERROR.sendLog(msg.toString());

            // save the whole error to config_error.txt
            try {
                File outFile = new File(Utils.rootLoc, "config_error.txt");
                PrintStream out = new PrintStream(new FileOutputStream(outFile));
                out.println("Use the following website to help you find and fix configuration errors:");
                out.println("https://yaml-online-parser.appspot.com/");
                out.println();
                out.println(ex.toString());
                out.close();
                Log.INFO.sendLog("Saved the full error message to " + outFile);
            } catch (IOException ex2) {
                Log.ERROR.sendLog("Failed to save the full error message!");
            }

            // save a backup
            File backupFile = new File(Utils.rootLoc, "perms_backup.yml");
            File sourceFile = new File(Utils.rootLoc, "perms.yml");
            if (FileUtil.copy(sourceFile, backupFile)) {
                Log.INFO.sendLog("Saved a backup of your configuration to " + backupFile);
            } else {
                Log.ERROR.sendLog("Failed to save a configuration backup!");
            }
        } catch (Exception ex) {
            Log.ERROR.sendLog("Failed to load configuration");
        }
    }

    public void saveConfig() {
        // If there's no keys (such as in the event of a load failure) don't save
        if (config.getKeys(true).size() > 0) {
            try {
                config.save(configFile);
            } catch (IOException ex) {
                Log.ERROR.sendLog("Failed to save configuration");
            }
        }
    }


    //External API :P

    /**
     * Get the group with the given name.
     *
     * @param groupName The name of the group.
     * @return A Group if it exists or null otherwise.
     */
    public Group getGroup(String groupName) {
        if (getNode("groups") != null) {
            return getNode("groups").getKeys(false).stream().filter(key -> key.equalsIgnoreCase(groupName)).findFirst()
                .map(key -> new Group(key, getNode("groups").getConfigurationSection(key).getInt("priority", 0))).orElse(null);
        }
        return null;
    }

    /**
     * Returns a list of groups a player is in.
     *
     * @param playerName The name of the player.
     * @return The groups this player is in. May be empty.
     * @deprecated Use UUIDs instead.
     */
    @Deprecated
    public List<Group> getGroups(String playerName) {
        ArrayList<Group> result = new ArrayList<>();
        ConfigurationSection node = getUsernameNode(playerName);
        if (node != null) {
            node.getStringList("groups").forEach(key -> {
                int priority = getNode("groups").getConfigurationSection(key).getInt("priority", 0);
                result.add(new Group(key, priority));
            });
        } else {
            result.add(new Group("default", 0));
        }
        return result;
    }

    /**
     * Returns a list of groups a player is in.
     *
     * @param player The uuid of the player.
     * @return The groups this player is in. May be empty.
     */
    public List<Group> getGroups(UUID player) {
        ArrayList<Group> result = new ArrayList<>();
        if (getNode("users/" + player) != null) {
            getNode("users/" + player).getStringList("groups").forEach(key -> {
                int priority = getNode("groups").getConfigurationSection(key).getInt("priority", 0);
                result.add(new Group(key, priority));
            });
        } else {
            result.add(new Group("default", 0));
        }
        return result;
    }

    /**
     * Returns permission info on the given player.
     *
     * @param playerName The name of the player.
     * @return A PermissionsInfo about this player.
     * @deprecated Use UUIDs instead.
     */
    @Deprecated
    public PermissionInfo getPlayerInfo(String playerName) {
        ConfigurationSection node = getUsernameNode(playerName);
        if (node == null) {
            return null;
        } else {
            return new PermissionInfo(node, "groups");
        }
    }

    /**
     * Returns permission info on the given player.
     *
     * @param player The uuid of the player.
     * @return A PermissionsInfo about this player.
     */
    public PermissionInfo getPlayerInfo(UUID player) {
        if (getNode("users/" + player) == null) {
            return null;
        } else {
            return new PermissionInfo(getNode("users/" + player), "groups");
        }
    }

    /**
     * Returns a list of all defined groups.
     *
     * @return The list of groups.
     */
    public List<Group> getAllGroups() {
        ArrayList<Group> result = new ArrayList<>();
        if (getNode("groups") != null) {
            getNode("groups").getKeys(false).forEach(key -> {
                int priority = getNode("groups").getConfigurationSection(key).getInt("priority", 0);
                result.add(new Group(key, priority));
            });
        }
        return result;
    }

    public void registerPlayer(LCPlayer player) {
        if(!Csts.isBungee){
            if (permissions.containsKey(player.getUniqueId())) {
                debug("Registering " + player.getName() + ": was already registered");
                unregisterPlayer(player);
            }

            Player p = LCSpigot.getInstance().getServer().getPlayer(player.getUniqueId());
            PermissionAttachment attachment = p.addAttachment(LCSpigot.getInstance());
            permissions.put(player.getUniqueId(), attachment);
        }else{
            if (bungeePerms.containsKey(player.getUniqueId())) {
                debug("Registering " + player.getName() + ": was already registered");
                unregisterPlayer(player);
            }

            bungeePerms.put(player.getUniqueId(), new HashMap<>());
        }

        calculateAttachment(player);
    }

    public void unregisterPlayer(LCPlayer player) {
        if(!Csts.isBungee){
            if (permissions.containsKey(player.getUniqueId())) {
                try {
                    Player p = LCSpigot.getInstance().getServer().getPlayer(player.getUniqueId());
                    p.removeAttachment(permissions.get(player.getUniqueId()));
                } catch (IllegalArgumentException ex) {
                    debug("Unregistering " + player.getName() + ": player did not have attachment");
                }

                permissions.remove(player.getUniqueId());
            } else {
                debug("Unregistering " + player.getName() + ": was not registered");
            }
        }else{
            if(bungeePerms.containsKey(player.getUniqueId())){
                bungeePerms.remove(player.getUniqueId());
            }else{
                debug("Unregistering " + player.getName() + ": was not registered");
            }
        }
    }

    public void refreshForPlayer(UUID player) {
        saveConfig();
        debug("Refreshing for player " + player);
        if(!Csts.isBungee){
            Player onlineLCPlayer = LCSpigot.getInstance().getServer().getPlayer(player);
            if (onlineLCPlayer != null) {
                calculateAttachment(LCAPI.getPlayerManager().getLCPlayer(onlineLCPlayer));
            }
        }else{
            ProxiedPlayer bungeeLCPlayer = LCBungee.getInstance().getProxy().getPlayer(player);
            if (bungeeLCPlayer != null) {
                calculateAttachment(LCAPI.getPlayerManager().getLCPlayer(bungeeLCPlayer));
            }
        }
    }

    private void fillChildGroups(HashSet<String> childGroups, String group) {
        if (childGroups.contains(group)) return;
        childGroups.add(group);

        getNode("groups").getKeys(false).forEach(key -> getNode("groups/" + key).getStringList("inheritance")
            .forEach(parent -> {
            if (parent.equalsIgnoreCase(group)) {
                fillChildGroups(childGroups, key);
            }
        }));
    }

    public void refreshForGroup(String group) {
        saveConfig();

        HashSet<String> childGroups = new HashSet<>();
        fillChildGroups(childGroups, group);
        debug("Refreshing for group " + group + " (total " + childGroups.size() + " subgroups)");

        if(!Csts.isBungee){
            permissions.keySet().forEach(uuid -> {
                LCPlayer player = LCAPI.getPlayerManager().getLCPlayer(LCSpigot.getInstance().getServer().getPlayer(uuid));
                ConfigurationSection node = getUserNode(player);

                // if the player isn't in the config, act like they're in default
                List<String> groupList = (node != null) ? node.getStringList("groups") : Arrays.asList("default");
                for (String userGroup : groupList) {
                    if (childGroups.contains(userGroup)) {
                        calculateAttachment(player);
                        break;
                    }
                }
            });
        }else{
            bungeePerms.keySet().forEach(uuid -> {
                LCPlayer player = LCAPI.getPlayerManager().getLCPlayer(LCSpigot.getInstance().getServer().getPlayer(uuid));
                ConfigurationSection node = getUserNode(player);

                // if the player isn't in the config, act like they're in default
                List<String> groupList = (node != null) ? node.getStringList("groups") : Arrays.asList("default");
                for (String userGroup : groupList) {
                    if (childGroups.contains(userGroup)) {
                        calculateAttachment(player);
                        break;
                    }
                }
            });
        }
    }

    public void refreshPermissions() {
        if(!Csts.isBungee){
            debug("Refreshing all permissions (for " + permissions.size() + " players)");
            permissions.keySet().forEach(player -> calculateAttachment(LCAPI.getPlayerManager().getLCPlayer(LCSpigot.getInstance().getServer().getPlayer(player))));
        }else{
            debug("Refreshing all bungee permissions (for " + bungeePerms.size() + " players)");
            bungeePerms.keySet().forEach(player -> calculateAttachment(LCAPI.getPlayerManager().getLCPlayer(LCBungee.getInstance().getProxy().getPlayer(player))));
        }
    }

    public ConfigurationSection getNode(String node) {
        return getConfig().getKeys(true).stream().filter(entry -> node.equalsIgnoreCase(entry) && getConfig().isConfigurationSection(entry))
            .findFirst().map(s -> getConfig().getConfigurationSection(s)).orElse(null);
    }

    public ConfigurationSection getUserNode(LCPlayer player) {
        ConfigurationSection sec = getNode("users/" + player.getUniqueId());
        if (sec == null) {
            sec = getNode("users/" + player.getName());
            if (sec != null) {
                getConfig().set(sec.getCurrentPath(), null);
                getConfig().set("users/" + player.getUniqueId(), sec);
                sec.set("name", player.getName());
                debug("Migrated " + player.getName() + " to UUID " + player.getUniqueId());
                saveConfig();
            }
        }

        // make sure name field matches
        if (sec != null) {
            if (!player.getName().equals(sec.getString("name"))) {
                debug("Updating name of " + player.getUniqueId() + " to: " + player.getName());
                sec.set("name", player.getName());
                saveConfig();
            }
        }

        return sec;
    }

    public ConfigurationSection getUsernameNode(String name) {
        // try to look up node based on username rather than UUID
        ConfigurationSection sec = getNode("users");
        if (sec != null) {
            for (String child : sec.getKeys(false)) {
                ConfigurationSection node = sec.getConfigurationSection(child);
                if (node != null && (name.equals(node.getString("name")) || name.equals("child"))) {
                    // either the "name" field matches or the key matches
                    return node;
                }
            }
        }
        return null;
    }

    public ConfigurationSection createNode(String node) {
        final ConfigurationSection[] sec = {getConfig()};
        Arrays.stream(node.split("/")).forEach(piece -> {
            ConfigurationSection sec2 = getNode(sec[0] == getConfig() ? piece : sec[0].getCurrentPath() + "/" + piece);
            if (sec2 == null) {
                sec2 = sec[0].createSection(piece);
            }
            sec[0] = sec2;
        });

        return sec[0];
    }

    public HashMap<String, Boolean> getAllPerms(String desc, String path) {
        ConfigurationSection node = getNode(path);

        final int failures[] = {0};
        final String firstFailure[] = {""};

        // Make an attempt to autofix incorrect nesting
        final boolean fixed[] = {false, false};
        while (fixed[1]) {
            fixed[1] = false;
            node.getKeys(true).forEach(key -> {
                if (node.isBoolean(key) && key.contains("/")) {
                    node.set(key.replace("/", "."), node.getBoolean(key));
                    node.set(key, null);
                    fixed[0] = fixed[1] = true;
                } else if (node.isConfigurationSection(key) && node.getConfigurationSection(key).getKeys(true).size() == 0) {
                    node.set(key, null);
                    fixed[0] = fixed[1] = true;
                }
            });
        }
        if (fixed[0]) {
            Log.INFO.sendLog("Fixed broken nesting in " + desc + ".");
            saveConfig();
        }

        LinkedHashMap<String, Boolean> result = new LinkedHashMap<>();
        node.getKeys(false).forEach(key -> {
            if (node.isBoolean(key)) {
                result.put(key, node.getBoolean(key));
            } else {
                ++failures[0];
                if (firstFailure[0].length() == 0) {
                    firstFailure[0] = key;
                }
            }
        });

        if (failures[0] == 1) {
            Log.WARN.sendLog("In " + desc + ": " + firstFailure[0] + " is non-boolean.");
        } else if (failures[0] > 1) {
            Log.WARN.sendLog("In " + desc + ": " + firstFailure[0] + " is non-boolean (+" + (failures[0] - 1) + " more).");
        }

        return result;
    }

    public void debug(String message) {
        if (LCAPI.getConfigManager().get(Csts.GENERAL_CONF).getBoolean("debug", true)) {
            Log.DEBUG.sendLog("Debug: " + message);
        }
    }

    public void calculateAttachment(LCPlayer player) {
        if (player == null) {
            return;
        }

        if(!Csts.isBungee){
            PermissionAttachment attachment = permissions.get(player.getUniqueId());

            if (attachment == null) {
                debug("Calculating permissions on " + player.getName() + ": attachment was null");
                return;
            }

            Map<String, Boolean> values = calculatePlayerPermissions(player, LCAPI.getServerManager().getCurrentServer().getGroup());

            // Fill the attachment reflectively so we don't recalculate for each permission
            // it turns out there's a lot of permissions!
            Map<String, Boolean> dest = reflectMap(attachment);
            dest.clear();
            dest.putAll(values);

            debug("Calculated permissions on " + player.getName() + ": " + dest.size() + " values");
            LCSpigot.getInstance().getServer().getPlayer(player.getUniqueId()).recalculatePermissions();

        }else{
            Map<String, Boolean> gettedPerms = bungeePerms.get(player.getUniqueId());
            Map<String, Boolean> values = calculatePlayerPermissions(player, LCAPI.getServerManager().getCurrentServer().getGroup());
            gettedPerms.clear();
            gettedPerms.putAll(values);

            debug("Calculated bungee permissions on " + player.getName() + ": " + gettedPerms.size() + " values");
        }
    }

    // -- Private stuff

    private Field pField;

    @SuppressWarnings("unchecked")
    private Map<String, Boolean> reflectMap(PermissionAttachment attachment) {
        try {
            if (pField == null) {
                pField = PermissionAttachment.class.getDeclaredField("permissions");
                pField.setAccessible(true);
            }
            return (Map<String, Boolean>) pField.get(attachment);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    // normally, LinkedHashMap.put (and thus putAll) will not reorder the list
    // if that key is already in the map, which we don't want - later puts should
    // always be bumped to the end of the list
    private <K, V> void put(Map<K, V> dest, K key, V value) {
        dest.remove(key);
        dest.put(key, value);
    }

    private <K, V> void putAll(Map<K, V> dest, Map<K, V> src) {
        src.forEach((key, value) -> put(dest, key, value));
    }

    private Map<String, Boolean> calculatePlayerPermissions(LCPlayer player, String server) {
        ConfigurationSection node = getUserNode(player);

        // if the player isn't in the config, act like they're in default
        if (node == null) {
            return calculateGroupPermissions("default", server);
        }

        String nodePath = node.getCurrentPath();
        Map<String, Boolean> perms = new LinkedHashMap<>();

        // first, apply the player's groups (getStringList returns an empty list if not found)
        // later groups override earlier groups
        for (String group : node.getStringList("groups")) {
            putAll(perms, calculateGroupPermissions(group, server));
        }

        // now apply user-specific permissions
        if (getNode(nodePath + "/permissions") != null) {
            putAll(perms, getAllPerms("user " + player, nodePath + "/permissions"));
        }

        // now apply server- and user-specific permissions
        if (getNode(nodePath + "/servers/" + server) != null) {
            putAll(perms, getAllPerms("user " + player + " server " + server, nodePath + "/servers/" + server));
        }

        return perms;
    }

    private Map<String, Boolean> calculateGroupPermissions(String group, String server) {
        return calculateGroupPermissions0(new HashSet<>(), group, server);
    }

    private Map<String, Boolean> calculateGroupPermissions0(Set<String> recursionBuffer, String group, String server) {
        String groupNode = "groups/" + group;

        // if the group's not in the config, nothing
        if (getNode(groupNode) == null) {
            return new LinkedHashMap<>();
        }

        recursionBuffer.add(group);
        Map<String, Boolean> perms = new LinkedHashMap<String, Boolean>();

        // first apply any parent groups (see calculatePlayerPermissions for more)
        for (String parent : getNode(groupNode).getStringList("inheritance")) {

        }

        getNode(groupNode).getStringList("inheritance").forEach(parent -> {
            if (recursionBuffer.contains(parent)) {
                Log.DEBUG.sendLog("In group " + group + ": recursive inheritance from " + parent);
                return;
            }

            putAll(perms, calculateGroupPermissions0(recursionBuffer, parent, server));
        });

        // now apply the group's permissions
        if (getNode(groupNode + "/permissions") != null) {
            putAll(perms, getAllPerms("group " + group, groupNode + "/permissions"));
        }

        // now apply server-specific permissions
        if (getNode(groupNode + "/servers/" + server) != null) {
            putAll(perms, getAllPerms("group " + group + " server " + server, groupNode + "/servers/" + server));
        }

        return perms;
    }
}