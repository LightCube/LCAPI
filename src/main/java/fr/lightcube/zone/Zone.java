package fr.lightcube.zone;

import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Created by Nitorac on 25/06/2017.
 */
public abstract class Zone {

    public PlayerEnterZone enterEvent;
    public PlayerQuitZone quitEvent;

    public Zone(){
        enterEvent = (p, l) -> {};
        quitEvent = (p, l) -> {};
    }

    public abstract boolean isInZone(Location l);

    public void setEnterZoneEvent(PlayerEnterZone event){
        this.enterEvent = event;
    }

    public void setQuitZoneEvent(PlayerQuitZone event){
        this.quitEvent = event;
    }

    public interface PlayerEnterZone {
        void onPlayerEnter(Player p, Location currentLoc);
    }

    public interface PlayerQuitZone {
        void onPlayerQuit(Player p, Location currentLoc);
    }
}