package fr.lightcube.zone.regions;

import fr.lightcube.zone.Zone;
import org.bukkit.Location;

/**
 * Created by Nitorac on 25/06/2017.
 */

public class ZoneRectangle extends Zone {

    private Location a;
    private Location b;

    public ZoneRectangle(Location aPoint, Location bPoint){
        super();
        this.a = aPoint;
        this.b = bPoint;
    }

    @Override
    public boolean isInZone(Location l) {
        return (l.getBlockX() <= Math.max(a.getBlockX(), b.getBlockX()) && l.getBlockX() >= Math.min(a.getBlockX(), b.getBlockX())) &&
            (l.getBlockY() <= Math.max(a.getBlockY(), b.getBlockY()) && l.getBlockY() >= Math.min(a.getBlockY(), b.getBlockY())) &&
            (l.getBlockZ() <= Math.max(a.getBlockZ(), b.getBlockZ()) && l.getBlockZ() >= Math.min(a.getBlockZ(), b.getBlockZ()));
    }
}
