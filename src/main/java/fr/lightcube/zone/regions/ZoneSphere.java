package fr.lightcube.zone.regions;

import fr.lightcube.zone.Zone;
import org.bukkit.Location;

/**
 * Created by Nitorac on 25/06/2017.
 */

public class ZoneSphere extends Zone {

    protected double radius;

    protected Location center;

    public ZoneSphere(Location center, int radius){
        super();
        this.center = center;
        this.radius = radius+1;
    }

    @Override
    public boolean isInZone(Location l) {
        return radius * radius > distance(l);
    }

    protected int distance(Location l) {
        int xDif = center.getBlockX() - l.getBlockX();
        int yDif = center.getBlockY() - l.getBlockY();
        int zDif = center.getBlockZ() - l.getBlockZ();
        return xDif * xDif + yDif * yDif + zDif * zDif;
    }
}
