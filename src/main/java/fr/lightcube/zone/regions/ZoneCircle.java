package fr.lightcube.zone.regions;

import org.bukkit.Location;

/**
 * Created by Nitorac on 26/06/2017.
 */

public class ZoneCircle extends ZoneSphere {

    public ZoneCircle(Location center, int radius){
        super(center, radius);
    }

    @Override
    public boolean isInZone(Location l) {
        return l.getBlockY() == center.getBlockY() && radius * radius > distance(l);
    }
}
