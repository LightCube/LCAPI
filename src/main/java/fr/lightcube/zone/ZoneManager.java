package fr.lightcube.zone;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.lightcube.LCSpigot;
import fr.lightcube.utils.CommonUtilsSpigot;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.*;

/**
 * Created by Nitorac on 25/06/2017.
 */
public class ZoneManager implements Listener{

    private static Map<String, Zone> registeredZones;
    private static Map<UUID, ArrayList<String>> playerZone;

    public ZoneManager(){
        registeredZones = Maps.newHashMap();
        playerZone = Maps.newHashMap();
        LCSpigot.getInstance().getServer().getPluginManager().registerEvents(this, LCSpigot.getInstance());
    }

    public void registerZone(String key, Zone zone){
        if(registeredZones.containsKey(key)) return;
        registeredZones.putIfAbsent(key, zone);

        CommonUtilsSpigot.getOnlinePlayers().forEach(this::checkInteractZone);
    }

    public void replaceZone(String key, Zone newZone){
        if(!registeredZones.containsKey(key)) return;
        registeredZones.replace(key, newZone);
        CommonUtilsSpigot.getOnlinePlayers().forEach(this::checkInteractZone);
    }

    public void unregisterZone(String key){
        if(!registeredZones.containsKey(key)) return;
        registeredZones.remove(key);
        CommonUtilsSpigot.getOnlinePlayers().forEach(this::checkInteractZone);
    }

    public boolean isPlayerInZone(String zone, Player p){
        Zone z;
        return ((z = registeredZones.get(zone)) != null) && z.isInZone(p.getLocation());
    }

    public Zone getZoneFromName(String name){
        return registeredZones.get(name);
    }

    public ArrayList<String> getZonesFromPlayer(Player p){
        return getZonesFromLoc(p.getLocation());
    }

    public ArrayList<String> getZonesFromLoc(Location l){
        ArrayList<String> result = new ArrayList<>();
        registeredZones.forEach((name, zone) -> {
            if(zone.isInZone(l)){
                result.add(name);
            }
        });
        return result;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e){
        playerZone.put(e.getPlayer().getUniqueId(), getZonesFromPlayer(e.getPlayer()));
    }

    @EventHandler
    public void onTeleport(PlayerTeleportEvent e){
        checkInteractZone(e.getPlayer(), e.getTo());
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e){
        checkInteractZone(e.getPlayer());
    }

    public void checkInteractZone(Player p){
        checkInteractZone(p, null);
    }

    public void checkInteractZone(Player p, Location l){
        Set<String> oldZones = new HashSet<>(playerZone.getOrDefault(p.getUniqueId(), new ArrayList<>()));
        Set<String> newZones = new HashSet<>((l != null) ? getZonesFromLoc(l) : getZonesFromPlayer(p));

        Set<String> entered = Sets.difference(newZones, oldZones);
        Set<String> quitted = Sets.difference(oldZones, newZones);

        entered.forEach(name -> getZoneFromName(name).enterEvent.onPlayerEnter(p, (l != null) ? l : p.getLocation()));
        quitted.forEach(name -> getZoneFromName(name).quitEvent.onPlayerQuit(p, (l != null) ? l : p.getLocation()));

        playerZone.put(p.getUniqueId(), new ArrayList<>(newZones));
    }
}
