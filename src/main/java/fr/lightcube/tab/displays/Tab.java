package fr.lightcube.tab.displays;

import java.util.UUID;

/**
 * The highest level of a tab list.
 */
public interface Tab {
    UUID getUuid();

    /**
     * Enables the tab list, starts any necessary listeners/schedules.
     * @return The tab list.
     */
    Tab enable();

    /**
     * Disables the tab list: stops existing listeners/schedules.
     * @return The tab list.
     */
    Tab disable();

    Tab clone();
}
