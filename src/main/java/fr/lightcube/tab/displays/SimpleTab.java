package fr.lightcube.tab.displays;

import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers.NativeGameMode;
import com.comphenix.protocol.wrappers.EnumWrappers.PlayerInfoAction;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.google.common.base.Preconditions;
import fr.lightcube.tab.items.CustomTabItem;
import fr.lightcube.utils.Log;
import fr.lightcube.utils.Utils;
import lombok.Getter;
import org.bukkit.Bukkit;

import java.util.*;
import java.util.Map.Entry;

/**
 * A simple implementation of a custom tab list that supports batch updates.
 */
public class SimpleTab extends TitledTab implements CustomTab {

    public static int MAXIMUM_ITEMS = 4 * 20; // client maximum is 4x20 (4 columns, 20 rows)

    protected final Map<Integer, CustomTabItem> items;
    protected final int maxItems;
    protected final int minColumnWidth;
    protected final int maxColumnWidth;

    @Getter protected boolean batchEnabled;
    protected final Map<Integer, CustomTabItem> clientItems;

    public SimpleTab(UUID uuid, int maxItems, int minColumnWidth, int maxColumnWidth) {
        super(uuid);
        Preconditions.checkArgument(maxItems <= MAXIMUM_ITEMS, "Impossible la limite d'items maximales de " + MAXIMUM_ITEMS);
        Preconditions.checkArgument(minColumnWidth <= maxColumnWidth || maxColumnWidth < 0, "minColumnWidth ne peut pas être plus grand que maxColumnWidth");

        this.maxItems = maxItems < 0 ? MAXIMUM_ITEMS : maxItems;
        this.minColumnWidth = minColumnWidth;
        this.maxColumnWidth = maxColumnWidth;
        this.clientItems = new HashMap<>();
        this.items = new HashMap<>();
    }

    protected SimpleTab(UUID uuid, String header, String footer, int maxItems, int minColumnWidth, int maxColumnWidth, Map<Integer, CustomTabItem> clientItems, Map<Integer, CustomTabItem> items, boolean update) {
        super(uuid, header, footer);
        Preconditions.checkArgument(maxItems <= MAXIMUM_ITEMS, "Impossible la limite d'items maximales de " + MAXIMUM_ITEMS);
        Preconditions.checkArgument(minColumnWidth <= maxColumnWidth || maxColumnWidth < 0, "minColumnWidth ne peut pas être plus grand que maxColumnWidth");

        this.maxItems = maxItems < 0 ? MAXIMUM_ITEMS : maxItems;
        this.minColumnWidth = minColumnWidth;
        this.maxColumnWidth = maxColumnWidth;
        this.clientItems = clientItems;
        this.items = items;

        if(update){
            update();
        }
    }

    public int getMaxItems() {
        return maxItems;
    }

    @Override
    public SimpleTab enable() {
        super.enable();
        return this;
    }

    @Override
    public SimpleTab disable() {
        super.disable();
        return this;
    }

    /**
     * Sends the batch update to the player and resets the batch.
     */
    public void batchUpdate() {
        update(this.clientItems, this.items, true);
        this.clientItems.clear();
        this.clientItems.putAll(this.items);
    }

    /**
     * Reset the existing batch.
     */
    public void batchReset() {
        this.items.clear();
        this.items.putAll(this.clientItems);
    }

    /**
     * Enable batch processing of tab items. Modifications to the tab list
     * will not be sent to the client until {@link #batchUpdate()} is called.
     * @param batchEnabled
     */
    public void setBatchEnabled(boolean batchEnabled) {
        if (this.batchEnabled == batchEnabled)
            return;
        this.batchEnabled = batchEnabled;
        this.clientItems.clear();

        if (this.batchEnabled)
            this.clientItems.putAll(this.items);
    }

    public void add(CustomTabItem item) {
        set(getNextIndex(), item);
    }

    public void add(int index, CustomTabItem item) {
        validateIndex(index);
        Map<Integer, CustomTabItem> current = new HashMap<>();
        current.putAll(this.items);

        Map<Integer, CustomTabItem> map = new HashMap<>();
        for (int i = index; i < getMaxItems(); i++) {
            if (!contains(i))
                break;
            CustomTabItem move = get(i);
            map.put(i + 1, move);
        }
        map.put(index, item);
        update(current, map);
    }

    public CustomTabItem set(int index, CustomTabItem item) {
        Map<Integer, CustomTabItem> items = new HashMap<>(1);
        items.put(index, item);
        return set(items).get(index);
    }

    public Map<Integer, CustomTabItem> set(Map<Integer, CustomTabItem> items) {
        for (Entry<Integer, CustomTabItem> entry : items.entrySet())
            validateIndex(entry.getKey());

        Map<Integer, CustomTabItem> oldItems = new HashMap<>();
        oldItems.putAll(this.items);
        update(oldItems, items);
        return oldItems;
    }

    public CustomTabItem remove(int index) {
        validateIndex(index);
        CustomTabItem removed = this.items.remove(index);
        update(index, removed, null);
        return removed;
    }

    public <T extends CustomTabItem> T remove(T item) {
        Iterator<Entry<Integer, CustomTabItem>> iterator = this.items.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<Integer, CustomTabItem> entry = iterator.next();
            if (entry.getValue().equals(item))
                remove(entry.getKey());
        }
        return item;
    }

    public boolean contains(int index) {
        validateIndex(index);
        return this.items.containsKey(index);
    }

    public CustomTabItem get(int index) {
        validateIndex(index);
        return this.items.get(index);
    }

    public void update() {
        update(this.items, this.items);
    }

    public void update(int index) {
        Map<Integer, CustomTabItem> map = new HashMap<>();
        map.put(index, get(index));
        update(index, get(index), get(index));
    }

    public int getNextIndex() {
        for (int index = 0; index < getMaxItems(); index++) {
            if (!contains(index))
                return index;
        }
        return -1;
    }

    protected void update(int index, CustomTabItem oldItem, CustomTabItem newItem) {
        Map<Integer, CustomTabItem> oldItems = new HashMap<>(1);
        oldItems.put(index, oldItem);

        Map<Integer, CustomTabItem> newItems = new HashMap<>(1);
        newItems.put(index, newItem);

        update(oldItems, newItems);
    }

    protected void update(Map<Integer, CustomTabItem> oldItems, Map<Integer, CustomTabItem> items) {
        update(oldItems, items, false);
    }

    private void validateIndex(int index) {
        Preconditions.checkArgument(index > 0 || index < getMaxItems(), "index not in allowed range");
    }

    private boolean put(int index, CustomTabItem item) {
        if (index < 0 || index >= getMaxItems())
            return false;
        if (item == null) {
            this.items.remove(index);
            return true;
        }
        this.items.put(index, item);
        return true;
    }

    private Map<Integer, CustomTabItem> putAll(Map<Integer, CustomTabItem> items) {
        HashMap<Integer, CustomTabItem> result = new HashMap<>(items.size());
        for (Entry<Integer, CustomTabItem> entry : items.entrySet())
            if (put(entry.getKey(), entry.getValue()))
                result.put(entry.getKey(), entry.getValue());
        return result;
    }

    private void update(Map<Integer, CustomTabItem> oldItems, Map<Integer, CustomTabItem> items, boolean isBatch) {
        if (this.batchEnabled && !isBatch) {
            this.items.putAll(items);
            return;
        }

        Map<Integer, CustomTabItem> newItems = putAll(items);
        Utils.send(Bukkit.getPlayer(uuid), getUpdate(oldItems, newItems));
    }

    private List<PacketContainer> getUpdate(int index, CustomTabItem oldItem, CustomTabItem newItem) {
        if (newItem == null && oldItem != null)
            return Collections.singletonList(Utils.getPacket(PlayerInfoAction.REMOVE_PLAYER, getPlayerInfoData(index, oldItem)));

        List<PacketContainer> packets = new ArrayList<>(2);

        boolean skinChanged = oldItem == null || newItem.updateSkin() || !newItem.getSkin().equals(oldItem.getSkin());
        boolean textChanged = oldItem == null || newItem.updateText() || !newItem.getText().equals(oldItem.getText());
        boolean pingChanged = oldItem == null || newItem.updatePing() || oldItem.getPing() != newItem.getPing();

        if (skinChanged) {
            if(oldItem != null)
                packets.add(Utils.getPacket(PlayerInfoAction.REMOVE_PLAYER, getPlayerInfoData(index, oldItem)));
            //Log.INFO.sendLog("REMP > Index : " + index + " | oldItem : "+ oldItem);
            packets.add(Utils.getPacket(PlayerInfoAction.ADD_PLAYER, getPlayerInfoData(index, newItem)));
            //Log.INFO.sendLog("ADDP > Index : " + index + " | newItem : "+ newItem);
        }
        else {
            if(pingChanged)
                packets.add(Utils.getPacket(PlayerInfoAction.UPDATE_LATENCY, getPlayerInfoData(index, newItem)));
                //Log.INFO.sendLog("UPPING > Index : " + index + " | newItem : "+ newItem);
        }

        packets.add(Utils.getPacket(PlayerInfoAction.UPDATE_DISPLAY_NAME, getPlayerInfoData(index, newItem)));
        /*Log.INFO.sendLog("UPDISPNAME > Index : " + index + " | newItem : "+ newItem);

        if (packets.size() > 0) {
                Log.INFO.sendLog("packet update made:");
            Log.INFO.sendLog("  @" + index);
            Log.INFO.sendLog("  (" + skinChanged + "/" + textChanged + "/" + pingChanged + " = " + packets.size() + " packets");
        }*/

        return packets;
    }

    private List<PacketContainer> getUpdate(Map<Integer, CustomTabItem> oldItems, Map<Integer, CustomTabItem> items) {
        List<PacketContainer> all = new ArrayList<>(items.size() * 2);

        for (Entry<Integer, CustomTabItem> entry : items.entrySet())
            all.addAll(getUpdate(entry.getKey(), oldItems.get(entry.getKey()), entry.getValue()));
        List<PlayerInfoData> removePlayer = new ArrayList<>();
        List<PlayerInfoData> addPlayer = new ArrayList<>();
        List<PlayerInfoData> displayChanged = new ArrayList<>();
        List<PlayerInfoData> pingChanged = new ArrayList<>();

        for (PacketContainer packet : all) {
            if (packet.getPlayerInfoAction().read(0) == PlayerInfoAction.REMOVE_PLAYER)
                removePlayer.addAll(packet.getPlayerInfoDataLists().read(0));
            if (packet.getPlayerInfoAction().read(0) == PlayerInfoAction.ADD_PLAYER)
                addPlayer.addAll(packet.getPlayerInfoDataLists().read(0));
            if (packet.getPlayerInfoAction().read(0) == PlayerInfoAction.UPDATE_DISPLAY_NAME)
                displayChanged.addAll(packet.getPlayerInfoDataLists().read(0));
            if (packet.getPlayerInfoAction().read(0) == PlayerInfoAction.UPDATE_LATENCY)
                pingChanged.addAll(packet.getPlayerInfoDataLists().read(0));
        }

        List<PacketContainer> result = new ArrayList<>(4);

        if (removePlayer.size() > 0 || addPlayer.size() > 0) {
            result.add(Utils.getPacket(PlayerInfoAction.REMOVE_PLAYER, removePlayer));
            result.add(Utils.getPacket(PlayerInfoAction.ADD_PLAYER, addPlayer));
        }
        if (displayChanged.size() > 0)
            result.add(Utils.getPacket(PlayerInfoAction.UPDATE_DISPLAY_NAME, displayChanged));
        if (pingChanged.size() > 0)
            result.add(Utils.getPacket(PlayerInfoAction.UPDATE_LATENCY, pingChanged));


        return result;
    }

    private PlayerInfoData getPlayerInfoData(int index, CustomTabItem item) {
        WrappedGameProfile profile = getGameProfile(index, item);
        return getPlayerInfoData(profile, item.getPing(), item.getText());
    }

    private PlayerInfoData getPlayerInfoData(WrappedGameProfile profile, int ping, String displayName) {
        if (displayName != null) {
            // min width
            StringBuilder displayNameBuilder = new StringBuilder(displayName);
            while (displayNameBuilder.length() < this.minColumnWidth)
                displayNameBuilder.append(" ");
            displayName = displayNameBuilder.toString();

            // max width
            if (this.maxColumnWidth > 0)
                while (displayName.length() > this.maxColumnWidth)
                    displayName = displayName.substring(0, displayName.length() - 1);
        }

        return new PlayerInfoData(profile, ping, NativeGameMode.SURVIVAL, displayName == null ? null : WrappedChatComponent.fromText(displayName));
    }

    private WrappedGameProfile getGameProfile(int index, CustomTabItem item) {
        String name = getStringIndex(index);
        UUID uuid = UUID.nameUUIDFromBytes(name.getBytes());

        WrappedGameProfile profile = new WrappedGameProfile(uuid, name + "|UpdateMC");
        profile.getProperties().put("textures", item.getSkin().getProperty());
        return profile;
    }

    private String getStringIndex(int index) {
        return String.format("%03d", index);
    }

    public Tab clone(){
        return new SimpleTab(uuid, getHeader(), getFooter(), maxItems, minColumnWidth, maxColumnWidth, clientItems, items, true);
    }
}
