package fr.lightcube.tab.displays;

import fr.lightcube.LCSpigot;
import fr.lightcube.tab.items.CustomTabItem;
import fr.lightcube.tab.items.PlayerCustomTabItem;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

/**
 * An implementation of SimpleTab that behaves like vanilla Minecraft.
 */
public final class DefaultTab extends SimpleTab implements Listener {
    private Map<Player,String> names = new HashMap<>();

    private int taskId;

    public DefaultTab(UUID uuid, int maxItems) {
        super(uuid, maxItems, -1, -1);
    }

    @Override
    public DefaultTab enable() {
        super.enable();
        Bukkit.getServer().getPluginManager().registerEvents(this, LCSpigot.getInstance());

        for (Player target : Bukkit.getOnlinePlayers())
            addPlayer(target);

        // Because there is no PlayerListNameUpdateEvent in Bukkit
        this.taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(LCSpigot.getInstance(), () -> {
            for (Player target : Bukkit.getOnlinePlayers()) {
                if (!names.containsKey(target))
                    continue;

                String prevName = names.get(target);
                String currName = target.getPlayerListName();

                if (prevName.equals(currName))
                    continue;

                int index = getTabItemIndex(target);
                update(index);
                names.put(target, currName);
            }
        }, 0, 5);

        return this;
    }

    @Override
    public DefaultTab disable() {
        super.disable();
        HandlerList.unregisterAll(this);
        Bukkit.getServer().getScheduler().cancelTask(this.taskId);
        return this;
    }

    /*@EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        addPlayer(event.getPlayer());
    }

    @EventHandler
    public void onPlayerJoin(PlayerQuitEvent event) {remove(getTabItemIndex(event.getPlayer()));}*/

    private void addPlayer(Player player) {
        add(getInsertLocation(player), new PlayerCustomTabItem(player));
        this.names.put(player, player.getPlayerListName());
    }

    private int getTabItemIndex(Player player) {
        for (Entry<Integer, CustomTabItem> item : this.items.entrySet()) {
            // items will always be players in this case, cast is safe
            PlayerCustomTabItem tabItem = (PlayerCustomTabItem) item.getValue();
            if (tabItem.getPlayer().equals(player))
                return item.getKey();
        }
        return -1;
    }

    private int getInsertLocation(Player player) {
        for (Entry<Integer, CustomTabItem> item : this.items.entrySet()) {
            // items will always be players in this case, cast is safe
            PlayerCustomTabItem tabItem = (PlayerCustomTabItem) item.getValue();

            if (player.getName().compareTo(tabItem.getPlayer().getName()) < 0)
                return item.getKey();
        }
        return getNextIndex();
    }

    /**
     * UNCLONEABLE !!
     * @return
     */
    public Tab clone(){
        return null;
    }
}
