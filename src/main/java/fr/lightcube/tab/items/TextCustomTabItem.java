package fr.lightcube.tab.items;

import fr.lightcube.skins.Skin;
import fr.lightcube.skins.SkinManager;
import lombok.Getter;
import lombok.ToString;

import java.util.Objects;

/**
 * A tab item with custom text, ping and skin.
 */
@ToString
public class TextCustomTabItem implements CustomTabItem {
    @Getter private String text;
    @Getter private int ping;
    @Getter private Skin skin;

    private String newText;
    private int newPing;
    private Skin newSkin;

    public TextCustomTabItem(String text) {
        this(text, 1000);
    }

    public TextCustomTabItem(String text, int ping) {
        this(text, ping, SkinManager.DEFAULT_SKIN);
    }

    public TextCustomTabItem(String text, int ping, Skin skin) {
        this.newText = text;
        this.newPing = ping;
        this.newSkin = skin;
        updateText();
        updatePing();
        updateSkin();
    }

    public void setText(String text) {
        this.newText = text;
    }

    public void setPing(int ping) {
        this.newPing = ping;
    }

    public void setSkin(Skin skin) {
        this.newSkin = skin;
    }

    @Override
    public boolean updateText() {
        boolean update = !Objects.equals(this.text, this.newText);
        this.text = this.newText;
        return update;
    }

    @Override
    public boolean updatePing() {
        boolean update = this.ping != this.newPing;
        this.ping = this.newPing;
        return update;
    }

    @Override
    public boolean updateSkin() {
        boolean update = !Objects.equals(this.skin, this.newSkin);
        this.skin = this.newSkin;
        return update;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof TextCustomTabItem))
            return false;
        TextCustomTabItem other = (TextCustomTabItem) object;
        return this.text.equals(other.getText()) && this.skin.equals(other.getSkin()) && this.ping == other.getPing();
    }
}
