package fr.lightcube.tab.items;

import fr.lightcube.skins.Skin;
import fr.lightcube.skins.SkinManager;
import lombok.ToString;

/**
 * A blank TextCustomTabItem
 */
@ToString
public class BlankCustomTabItem extends TextCustomTabItem {
    public BlankCustomTabItem(Skin skin) {
        super("", 1000, skin);
    }

    public BlankCustomTabItem() {
        this(SkinManager.DEFAULT_SKIN);
    }
}
