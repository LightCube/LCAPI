package fr.lightcube.tab;

import com.google.common.base.Preconditions;
import fr.lightcube.tab.displays.*;
import org.bukkit.Bukkit;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Nitorac on 28/04/2017.
 */
public class TabManager {

    private static Map<UUID, Tab> tabs;

    public TabManager(){
        tabs = new HashMap<>();
    }

    /**
     * Récupère le tab custom du joueur
     * @param uuid UUID
     * @return Le Tab ou un null
     */
    public Tab getTabList(UUID uuid) {
        return tabs.get(uuid);
    }

    /**
     * Désactive le tab custom pour un joueur
     * @param uuid UUID
     * @return le tab custom supprimé (ou null)
     */
    public Tab destroyTabList(UUID uuid) {
        Tab tab = getTabList(uuid);
        if (tab == null)
            return null;

        tabs.remove(uuid);
        return tab.disable();
    }

    public void reloadTab(UUID player, Tab tab){
        destroyTabList(player);
        put(player, tab.enable());
    }

    /**
     * Désactive un tab custom
     * @param tab
     * @return Le tab custom est supprimé.
     */
    public Tab destroyTabList(Tab tab) {
        return destroyTabList(tab.getUuid());
    }

    /**
     * Crée un nouveau TitledTab pour le player donné
     * @param uuid
     * @return Le nouveau TitledTab
     */
    public TitledTab newTitledTabList(UUID uuid) {
        return put(uuid, new TitledTab(uuid).enable());
    }

    /**
     * Creates a new DefaultTab.
     * @param uuid
     * @return
     */
    public DefaultTab newDefaultTabList(UUID uuid) {
        return put(uuid, new DefaultTab(uuid, -1).enable());
    }

    /**
     * Creates a new CustomTab with the given parameters.
     * @param uuid
     * @return
     */
    public SimpleTab newSimpleTabList(UUID uuid) {
        return newSimpleTabList(uuid, SimpleTab.MAXIMUM_ITEMS);
    }

    /**
     * Creates a new CustomTab with the given parameters.
     * @param uuid
     * @param maxItems
     * @return
     */
    public SimpleTab newSimpleTabList(UUID uuid, int maxItems) {
        return newSimpleTabList(uuid, maxItems, -1);
    }

    /**
     * Creates a new CustomTab with the given parameters.
     * @param uuid
     * @param maxItems
     * @param minColumnWidth
     * @return
     */
    public SimpleTab newSimpleTabList(UUID uuid, int maxItems, int minColumnWidth) {
        return newSimpleTabList(uuid, maxItems, minColumnWidth, -1);
    }

    /**
     * Creates a new CustomTab with the given parameters.
     * @param uuid
     * @param maxItems
     * @param minColumnWidth
     * @param maxColumnWidth
     * @return
     */
    public SimpleTab newSimpleTabList(UUID uuid, int maxItems, int minColumnWidth, int maxColumnWidth) {
        return put(uuid, new SimpleTab(uuid, maxItems, minColumnWidth, maxColumnWidth).enable());
    }

    /**
     * Creates a new TableTab with the given parameters.
     * @param uuid
     * @return
     */
    public TableTab newTableTabList(UUID uuid) {
        return newTableTabList(uuid, 4);
    }

    /**
     * Creates a new TableTab with the given parameters.
     * @param uuid
     * @param columns
     * @return
     */
    public TableTab newTableTabList(UUID uuid, int columns) {
        return newTableTabList(uuid, columns, -1);
    }

    /**
     * Creates a new TableTab with the given parameters.
     * @param uuid
     * @param columns
     * @param minColumnWidth
     * @return
     */
    public TableTab newTableTabList(UUID uuid, int columns, int minColumnWidth) {
        return newTableTabList(uuid, columns, minColumnWidth, -1);
    }

    /**
     * Creates a new TableTab with the given parameters.
     * @param uuid
     * @param columns
     * @param minColumnWidth
     * @param maxColumnWidth
     * @return
     */
    public TableTab newTableTabList(UUID uuid, int columns, int minColumnWidth, int maxColumnWidth) {
        return put(uuid, new TableTab(uuid, columns, minColumnWidth, maxColumnWidth).enable());
    }

    private <T extends Tab> T put(UUID uuid, T tabList) {
        Preconditions.checkArgument(!tabs.containsKey(uuid), "Le joueur '" + Bukkit.getPlayer(uuid).getName() + "' a deja une liste de tab !");
        tabs.put(uuid, tabList);
        return tabList;
    }
}
