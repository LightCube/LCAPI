package fr.lightcube;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import fr.lightcube.actionbar.ActionBarManager;
import fr.lightcube.bossbar.BossBarManager;
import fr.lightcube.commands.spigot.ConsoleCommand;
import fr.lightcube.commands.spigot.PermissionsCommand;
import fr.lightcube.commands.spigot.ReloadCommand;
import fr.lightcube.commands.spigot.WarpCommand;
import fr.lightcube.gui.GuiListener;
import fr.lightcube.hologram.HoloManager;
import fr.lightcube.hologram.HoloTouchListener;
import fr.lightcube.listeners.spigot.ChatEvents;
import fr.lightcube.listeners.spigot.PlayerEvents;
import fr.lightcube.listeners.spigot.ServerEvents;
import fr.lightcube.listeners.spigot.WorldEvents;
import fr.lightcube.scoreboard.LCScoreboardManager;
import fr.lightcube.skins.SkinManager;
import fr.lightcube.tab.TabManager;
import fr.lightcube.utils.CommonUtilsSpigot;
import fr.lightcube.utils.Csts;
import fr.lightcube.utils.Log;
import fr.lightcube.utils.Utils;
import fr.lightcube.warp.WarpManager;
import fr.lightcube.zone.ZoneManager;
import org.bukkit.Bukkit;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

public class LCSpigot extends JavaPlugin {

    private static LCSpigot instance;
    private static WarpManager warpManager;
    private static TabManager tabManager;
    private static SkinManager skinManager;
    private static BossBarManager bossBarManager;
    private static LCScoreboardManager lcScoreboardManager;
    private static ActionBarManager actionBarManager;
    private static ZoneManager zoneManager;
    private static ProtocolManager protocolManager;
    private static HoloManager holoManager;
    private static boolean failure = false;

    public static final ArrayList<Listener> BASE_EVENTS = new ArrayList<>(Arrays.asList(
            new ServerEvents(),
            new WorldEvents(),
            new PlayerEvents(),
            new ChatEvents(),
            new HoloTouchListener(),
            new GuiListener()
    ));

    public static final Map<String, TabExecutor> BASE_COMMANDS = Utils.createMap(
               "warp", new WarpCommand(),
               "lcreload", new ReloadCommand(),
               "permissions", new PermissionsCommand(),
               "console", new ConsoleCommand()
    );

    public LCSpigot(){
        instance = this;
        failure = !new LCAPI().init();
    }

    @Override
    public void onEnable(){
        if(failure){
            Log.FATAL.sendLog("Impossible de démarrer l'API correctement");
            LCAPI.getCommonUtils().safeShutdown("Le démarrage a mal fonctionné", true);
            return;
        }

        /*ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(this, PacketType.Play.Server.PLAYER_INFO) {
            @Override
            public void onPacketSending(PacketEvent event) {
                event.setCancelled(true);
            }
        });*/

        //RESET PLAYER MANAGER
        LCAPI.getPlayerManager().reset();
        warpManager = new WarpManager();
        skinManager = new SkinManager();
        tabManager = new TabManager();
        bossBarManager = new BossBarManager();
        lcScoreboardManager = new LCScoreboardManager();
        actionBarManager = new ActionBarManager();
        zoneManager = new ZoneManager();
        holoManager = new HoloManager();
        LCAPI.getPermManager().init();

        //Ajouter les joueurs existants #OnAimeLeSupportDuReload
        Bukkit.getOnlinePlayers().forEach(player -> LCAPI.getPlayerManager().addPlayer(player));

        // Register Base Commands

        BASE_COMMANDS.forEach((command, exec) -> {
            getCommand(command).setExecutor(exec);
            getCommand(command).setTabCompleter(exec);
        });

        // Register Base Listeners
        BASE_EVENTS.forEach(event -> Bukkit.getServer().getPluginManager().registerEvents(event, LCSpigot.this));

        registerRedisListener();
        stopWorldDownloader();

        protocolManager = ProtocolLibrary.getProtocolManager();

        Log.INFO.sendLog("LCAPI (Spigot) est active !");
    }

    public void registerRedisListener(){
        LCAPI.getRedisManager().init();

        LCAPI.getRedisManager().registerReceiver("LCSpigotInit", (subchannel, msg) -> {
            switch(subchannel){
                case "PlaySound":
                    String[] splitted = msg.split(Csts.SPLITTER, 4);
                    if(splitted.length != 4) return;
                    String server = splitted[0];
                    if(LCAPI.getServerManager().getCurrentServer().toString().equals(server)){
                        UUID player = UUID.fromString(splitted[1]);
                        String sound = splitted[2];
                        Float pitch = Float.parseFloat(splitted[3]);
                        Player p = LCSpigot.getInstance().getServer().getPlayer(player);
                        p.playSound(p.getLocation(), sound, 3.0F, pitch);
                    }
                    break;
                case "LCReload":
                    if(msg.equals("All")){
                        CommonUtilsSpigot.reloadWarp();
                        CommonUtilsSpigot.reloadPlayers();
                        CommonUtilsSpigot.reloadRank();
                    }else if(msg.equals("Warps")){
                        CommonUtilsSpigot.reloadWarp();
                    }else if(msg.equals("Players")) {
                        CommonUtilsSpigot.reloadPlayers();
                    }else if(msg.equals("Ranks")){
                        CommonUtilsSpigot.reloadRank();
                    }else if(msg.startsWith("Player ")){
                        UUID playerUuid = Utils.fromString(msg.replaceAll("Player ", ""));
                        Player victim = getInstance().getServer().getPlayer(playerUuid);
                        if(victim != null){
                            LCAPI.getPlayerManager().reloadPlayer(victim);
                        }
                    }
                    break;
            }
        });
    }
//
    @Override
    public void onDisable(){
        Bukkit.getMessenger().unregisterOutgoingPluginChannel(this, "WDL|CONTROL");
        Bukkit.getMessenger().unregisterIncomingPluginChannel(this, "WDL|INIT");
        LCAPI.getPermManager().disable();
        getHoloManager().disable();
        Log.WARN.sendLog("LCAPI (Spigot) est desactive !");
    }

    public void stopWorldDownloader(){
        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "WDL|CONTROL");
        Bukkit.getMessenger().registerIncomingPluginChannel(this, "WDL|INIT", (s, player, bytes) -> {
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeInt(1);
            out.writeBoolean(false);
            out.writeInt(1);
            out.writeBoolean(false);
            out.writeBoolean(false);
            out.writeBoolean(false);
            out.writeBoolean(false);
            Log.WARN.sendLog("WorldDownloader bloqué pour " + player.getName());
            player.kickPlayer("§r[§cLightCubeGuard§r] §c WorldDownloader est interdit sur le serveur.");
            player.sendPluginMessage(this, "WDL|CONTROL", out.toByteArray());
        });
    }

    public static LCSpigot getInstance(){
        return instance;
    }

    public static WarpManager getWarpManager(){
        return warpManager;
    }

    public static TabManager getTabManager(){
        return tabManager;
    }

    public static SkinManager getSkinManager(){
        return skinManager;
    }

    public static ProtocolManager getProtocolManager(){ return protocolManager; }

    public static BossBarManager getBossBarManager(){
        return bossBarManager;
    }

    public static LCScoreboardManager getScoreboardManager(){
        return lcScoreboardManager;
    }

    public static ActionBarManager getActionBarManager(){
        return actionBarManager;
    }

    public static ZoneManager getZoneManager(){
        return zoneManager;
    }

    public static HoloManager getHoloManager(){
        return holoManager;
    }
}