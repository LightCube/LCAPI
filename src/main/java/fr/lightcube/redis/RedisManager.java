package fr.lightcube.redis;

import fr.lightcube.LCAPI;
import fr.lightcube.utils.Csts;
import fr.lightcube.utils.Log;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nitorac on 15/04/2017.
 */
public class RedisManager {

    private LCSuscriber suscriber;
    private JedisPool pool;
    private static Map<String, ILCReceiver> receivers;

    public RedisManager(){
        receivers = new HashMap<>();
        pool = new JedisPool(new JedisPoolConfig(), LCAPI.getConfigManager().get(Csts.GENERAL_CONF).getString("redis.ip", "127.0.0.1"),
            LCAPI.getConfigManager().get(Csts.GENERAL_CONF).getInt("redis.port", 6379), 0,
            LCAPI.getConfigManager().get(Csts.GENERAL_CONF).getString("redis.password", "t5GFzjSVr31H451zKx"));
    }

    public void init(){
        Runnable run = () -> {
            suscriber = new LCSuscriber();
            Jedis jedis = pool.getResource();
            try {
                jedis.subscribe(suscriber, Csts.REDIS_MAIN_CHANNEL);
            } catch (Exception e) {
                Log.ERROR.sendLog("Impossible de se connecter au serveur Redis !");
                e.printStackTrace();
            }finally {
                jedis.close();
            }
        };
        LCAPI.getCommonUtils().runAsync(run);
    }

    public void registerReceiver(String key, ILCReceiver receiver){
        receivers.putIfAbsent(key, receiver);
    }

    public void sendMessage(ReceiverType type, final String channelTitle, final String message){
        Runnable run = () -> {
            Jedis jedis = pool.getResource();
            try {
                switch (type){
                    case SPIGOT_ONLY:
                        jedis.publish(Csts.REDIS_MAIN_CHANNEL,  Csts.REDIS_SPIGOT + Csts.REDIS_SPLITTER + channelTitle + Csts.REDIS_SPLITTER + message);
                        break;
                    case BUNGEE_ONLY:
                        jedis.publish(Csts.REDIS_MAIN_CHANNEL,  Csts.REDIS_BUNGEE + Csts.REDIS_SPLITTER + channelTitle + Csts.REDIS_SPLITTER + message);
                        break;
                    case BOTH:
                        jedis.publish(Csts.REDIS_MAIN_CHANNEL,  Csts.REDIS_ALL + Csts.REDIS_SPLITTER + channelTitle + Csts.REDIS_SPLITTER + message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }finally{
                jedis.close();
            }
        };

        LCAPI.getCommonUtils().runAsync(run);
    }

    public void replaceReceiver(String key, ILCReceiver newRec){
        receivers.replace(key, newRec);
    }

    /*******
     * ATTENTION, A NE JAMAIS UTILISER
     *
     * @param subchannel ATTENTION, A NE JAMAIS UTILISER
     * @param msg ATTENTION, A NE JAMAIS UTILISER
     */
    public void callAll(String subchannel, String msg){
        receivers.forEach((key, rec) -> rec.onMessage(subchannel, msg));
    }

    public void unregisterReceiver(String key){
        receivers.remove(key);
    }

    public enum ReceiverType{
        SPIGOT_ONLY,
        BUNGEE_ONLY,
        BOTH
    }
}
