package fr.lightcube.redis;

/**
 * Created by Nitorac on 16/04/2017.
 */
public interface ILCReceiver {

    void onMessage(String subchannel, String msg);
}
