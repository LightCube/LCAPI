package fr.lightcube.listeners.bungee;

import fr.lightcube.LCAPI;
import fr.lightcube.LCBungee;
import fr.lightcube.servers.Server;
import fr.lightcube.servers.ServerManager;
import fr.lightcube.utils.Csts;
import fr.lightcube.utils.Log;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Nitorac on 24/03/2017.
 */
public class ServerEvents implements Listener {

    private static Map<UUID, ServerInfo> pendingConnectionsTargets = new HashMap<>();

    @EventHandler
    public void onPlayerLogin(LoginEvent e){
        Server serverToConnect = LCAPI.getServerManager().getServerFromGroup(e.getConnection().getUniqueId(), "lobby", ServerManager.Strategy.LOBBY);

        if(serverToConnect == null){
            e.setCancelReason(Csts.INFO_PREFIX + "§4 Le serveur est plein, reconnectez-vous plus tard !");
            e.setCancelled(true);
            return;
        }

        pendingConnectionsTargets.put(e.getConnection().getUniqueId(), LCBungee.getInstance().getProxy().getServerInfo(serverToConnect.toString()));
        ServerInfo serv = pendingConnectionsTargets.get(e.getConnection().getUniqueId());
        Log.INFO.sendLog("Connexion au serveur " + serv.getName() + " pour le joueur " + e.getConnection().getName());
    }

    @EventHandler
    public void onPlayerPostLogin(PostLoginEvent e){
        LCAPI.getPlayerManager().addPlayer(e.getPlayer());
        LCAPI.getPermManager().registerPlayer(LCAPI.getPlayerManager().getLCPlayer(e.getPlayer()));
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerDisconnectEvent e){
        LCAPI.getPermManager().unregisterPlayer(LCAPI.getPlayerManager().getLCPlayer(e.getPlayer()));
        LCAPI.getPlayerManager().removePlayer(e.getPlayer());
    }

    @EventHandler
    public void onPlayerConnectServer(ServerConnectEvent e){
        if(pendingConnectionsTargets.containsKey(e.getPlayer().getUniqueId())){
            e.setTarget(pendingConnectionsTargets.get(e.getPlayer().getUniqueId()));
            pendingConnectionsTargets.remove(e.getPlayer().getUniqueId());
        }

        updateConnectedServer(e.getPlayer(), LCAPI.getServerManager().getServerSilent(e.getTarget().getName()).toString());
    }

    public void updateConnectedServer(ProxiedPlayer player, String server){
        LCAPI.getCommonUtils().runAsync(() -> {
            try {
                LCAPI.getDBManager().update("UPDATE players SET CUR_SERVER = ? WHERE UUID = ?", server, player.getUniqueId().toString());
            } catch (SQLException e) {
                Log.ERROR.sendLog("Impossible de mettre à jour le CUR_SERVER de " + player.getName());
                e.printStackTrace();
            }
        });
    }
}