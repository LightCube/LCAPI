package fr.lightcube.listeners.spigot;

import fr.lightcube.LCAPI;
import fr.lightcube.utils.Csts;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

/**
 * Created by Nitorac on 22/03/2017.
 */
public class WorldEvents implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent e) {

    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onLeaveDecay(LeavesDecayEvent e) {
        if (LCAPI.getConfigManager().get(Csts.WORLD_CONF).getBoolean("leaf-decay-block", true)) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onWeatherChange(WeatherChangeEvent e) {
        if (LCAPI.getConfigManager().get(Csts.WORLD_CONF).getBoolean("weather-force-clear", true) && e.toWeatherState()) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    void onEntityExplode(EntityExplodeEvent e) {
        if (LCAPI.getConfigManager().get(Csts.WORLD_CONF).getBoolean("explosion-cancelled", true)) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onVineGrow(BlockSpreadEvent e) {
        if (e.getNewState().getData().getItemType().equals(Material.VINE) && !LCAPI.getConfigManager().get(Csts.WORLD_CONF).getBoolean("vine-spreading", false)) {
            e.setCancelled(true);
        } else if (e.getNewState().getData().getItemType().equals(Material.MYCEL) && !LCAPI.getConfigManager().get(Csts.WORLD_CONF).getBoolean("mycelium-spreading", false)) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockFromTo(BlockFromToEvent e) {
        if (LCAPI.getConfigManager().get(Csts.WORLD_CONF).getBoolean("stop-ice-melting", true) && e.getBlock().getType().compareTo(Material.ICE) == 0 && e.getToBlock().getType().compareTo(Material.WATER) == 0) {
            e.setCancelled(true);
        }
    }
}
