package fr.lightcube.listeners.spigot;

import fr.lightcube.LCAPI;
import fr.lightcube.permissions.PermissionManager;
import fr.lightcube.players.LCPlayer;
import fr.lightcube.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.SimplePluginManager;

import java.util.ArrayList;
import java.util.Arrays;

import static fr.lightcube.utils.PermCsts.LCAPI_COLORED_CHAT;

/**
 * Created by Nitorac on 09/04/2017.
 */
public class ChatEvents implements Listener {

    public static final ArrayList<String> BANNED_COMMANDS = new ArrayList<>(Arrays.asList(
        "reload",
        "killall"
    ));

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void handleChat(AsyncPlayerChatEvent event){
        event.setCancelled(true);
        LCPlayer lcp = LCAPI.getPlayerManager().getLCPlayer(event.getPlayer());

        String chatDisplay = ChatColor.translateAlternateColorCodes('&', LCAPI.getRankManager().getChatDisplay());
        String formattedPseudo = lcp.getFullPrefix();
        String msg = (event.getPlayer().hasPermission(LCAPI_COLORED_CHAT)) ? ChatColor.translateAlternateColorCodes('&', event.getMessage()) : event.getMessage();
        ArrayList<Player> askedplayers = new ArrayList<Player>();
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (msg.lastIndexOf(p.getDisplayName()) != -1) {
                askedplayers.add(p);
            }
        }
        if (askedplayers.isEmpty()) {
            Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(chatDisplay.replace("%pseudo%", formattedPseudo).replace("%msg%", msg)));
        } else {
            String msgasked;
            for (Player p : askedplayers) {
                msgasked = msg.replace(p.getName(), "§e" + p.getName() + "§r");
                p.sendMessage(chatDisplay.replace("%pseudo%", formattedPseudo).replace("%msg%", msgasked));
                p.playSound(p.getLocation(), "random.orb", 3.0F, 1.5F);
            }
            Bukkit.getOnlinePlayers().forEach(p -> {if (!askedplayers.contains(p)) {p.sendMessage(chatDisplay.replace("%pseudo%", formattedPseudo).replace("%msg%", msg));}});
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onCommandPreProcess(PlayerCommandPreprocessEvent e){
        Player p = e.getPlayer();
        CommandMap commandMap = getCommandMap();
        String label = e.getMessage().substring(1).split(" ")[0];
        Command command = commandMap.getCommand(label);

        if(BANNED_COMMANDS.contains(label)){
            e.setCancelled(true);
            e.getPlayer().sendMessage("§cCette commande est interdite !");
        }

        if(command == null){
            e.setCancelled(true);
            e.getPlayer().sendMessage("§cCette commande n'existe pas !");
            return;
        }

        if(command.getPermission() == null || command.getPermission().isEmpty()){
            return;
        }

        // Contre le système de perm intégré et géré par WorldEdit.
        if(command.getPermission().startsWith("worldedit")){
            return;
        }else if(!p.hasPermission(command.getPermission())){
            e.setCancelled(true);
            e.getPlayer().sendMessage(PermissionManager.HASNT_PERMS_MESSAGE);
        }
    }

    private CommandMap getCommandMap() {
        CommandMap commandMap;
        commandMap = (CommandMap) Utils.getFieldValueSilent(SimplePluginManager.class, Bukkit.getServer().getPluginManager(), "commandMap");
        return commandMap;
    }
}
