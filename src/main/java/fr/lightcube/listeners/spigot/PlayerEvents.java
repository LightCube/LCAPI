package fr.lightcube.listeners.spigot;

import fr.lightcube.LCAPI;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created by Nitorac on 22/03/2017.
 */
public class PlayerEvents implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInteract(PlayerInteractEvent e){
        LCAPI.getPermManager().onPlayerInteract(e);

        /*if((e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) && e.getItem() != null && e.getItem().getType().equals(Material.COMPASS)){
            BaseGui menu = new BaseGui("§cCOUCOU BANDE DE COCHONS", BaseGui.Size.THREE_LINE);
            BaseGui submenu = new BaseGui("§cCOUCOU BANDE DE COCHONS", BaseGui.Size.THREE_LINE);
            BaseMenuItemGui clickTest = new BaseMenuItemGui("§bTESTER", new ItemStack(Material.EMERALD_BLOCK), "§cUne jolie ligne de description", "§bOhhh une autre :D", "§dEt la troisième x)");
            clickTest.setClickEvent(event -> event.getPlayer().sendMessage("§5§lLOOOLLL"));

            BaseMenuItemGui soumenuClick = new BaseMenuItemGui("§bSOUSMENU", new ItemStack(Material.RED_SANDSTONE), "§cUne jolie ligne de description", "§bOhhh une autre :D", "§dEt la troisième x)");

            menu.setItem(10, clickTest).setItem(13, clickTest).setItem(16, clickTest);
            menu.setItem(26, new SubMenuItemGui("VOIR DU SUPPLÉMENTAIRE", new ItemStack(Material.ANVIL), submenu, "§aLOLOLOLOLOL"));
            menu.setItem(26-8, new CloseItemGui());

            menu.fillEmptySlots();

            submenu.setItem(9, soumenuClick).setItem(11, soumenuClick).setItem(13, soumenuClick).setItem(15, soumenuClick);
            submenu.setItem(26, new BackItemGui());
            submenu.setParent(menu);

            PageMenuGui basePage = new PageMenuGui("§bCOSMÉTIQUES", menu);
            PageGui page1 = new PageGui(basePage, 0, BaseGui.Size.SIX_LINE);
            PageGui page2 = new PageGui(basePage, 1, BaseGui.Size.SIX_LINE);
            PageGui page3 = new PageGui(basePage, 2, BaseGui.Size.FIVE_LINE);
            PageGui page4 = new PageGui(basePage, 3, BaseGui.Size.SIX_LINE);
            PageGui page5 = new PageGui(basePage, 4, BaseGui.Size.SIX_LINE);

            page1.setItem(0, new BackItemGui())
                .setItem(1, new CloseItemGui())
                .setItem(6, new BaseMenuItemGui("§oCOUCOU PAGE 1", new ItemStack(Material.DIRT, 1), "§2PAGE 1 LOLILOL"))
                .setDefaultControls(new PrevPageItemGui(page1), new NextPageItemGui(page1));

            page2.setItem(0, new BackItemGui())
                .setItem(1, new CloseItemGui())
                .setItem(10, new BaseMenuItemGui("§oCOUCOU PAGE 2", new ItemStack(Material.STONE, 1), "§2PAGE 2 LOLILOL"))
                .setDefaultControls(new PrevPageItemGui(page2), new NextPageItemGui(page2));

            page3.setItem(0, new BackItemGui())
                .setItem(1, new CloseItemGui())
                .setItem(15, new BaseMenuItemGui("§oCOUCOU PAGE 3", new ItemStack(Material.STICK, 1), "§2PAGE 3 LOLILOL"))
                .setDefaultControls(new PrevPageItemGui(page3), new NextPageItemGui(pa8+ge3));

            page4.setItem(0, new BackItemGui())
                .setItem(1, new CloseItemGui())
                .setItem(19, new BaseMenuItemGui("§oCOUCOU PAGE 4", new ItemStack(Material.CARROT_STICK, 1), "§2PAGE 4 LOLILOL"))
                .setDefaultControls(new PrevPageItemGui(page4), new NextPageItemGui(page4));

            page5.setItem(0, new BackItemGui())
                .setItem(1, new CloseItemGui())
                .setItem(22, new BaseMenuItemGui("§oCOUCOU PAGE 5", new ItemStack(Material.GOLD_AXE, 1), "§2PAGE 5 LOLILOL"))
                .setDefaultControls(new PrevPageItemGui(page5), new NextPageItemGui(page5));

            basePage.addPage(page1, page2, page3, page4, page5);

            BaseMenuItemGui pageTestEntrance = new BaseMenuItemGui("§cTEST PAGE", new ItemStack(Material.ACACIA_STAIRS));
            pageTestEntrance.setClickEvent(e1 -> basePage.open(e1.getPlayer()));
            menu.setItem(21, pageTestEntrance);
            menu.open(e.getPlayer());
        }*/
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        LCAPI.getPermManager().onBlockPlace(event);
        /*Hologram holo = new Hologram(event.getBlock().getLocation().add(0, 1.0, 0), "§aCECI EST UN TEST AVEC " + count++, "§bDEUXIEME LINE MA PUTE", "§cXD LOLILOL JADORE TA COUILLE", "§dBLABLABLA");
        holo.addEvent("TestEvent", (p, h) -> h.setLocation(p.getLocation().add(0, 1, 0)));

        LCSpigot.getHoloManager().registerHologram("TestHolo" + count, holo);*/

        /* *********************************************************************************************************************** */

        /*if(zoneTest == null){
            ZoneRectangle rect1 = new ZoneRectangle(new Location(event.getBlock().getWorld(), 20, 59, -5), new Location(event.getBlock().getWorld(), 11, 54, 1));
            rect1.setEnterZoneEvent((player, l) -> CommonUtilsSpigot.getOnlinePlayers().forEach(p -> p.sendMessage("Le joueur " + player.getName() + " se trouve dans les zones : " + LCSpigot.getZoneManager().getZonesFromLoc(l))));
            rect1.setQuitZoneEvent((player, l) -> CommonUtilsSpigot.getOnlinePlayers().forEach(p -> p.sendMessage("Le joueur " + player.getName() + " se trouve dans les zones : " + LCSpigot.getZoneManager().getZonesFromLoc(l))));
            LCSpigot.getZoneManager().registerZone("ROUGE", rect1);

            ZoneRectangle rect2 = new ZoneRectangle(new Location(event.getBlock().getWorld(), 17, 56, -1), new Location(event.getBlock().getWorld(), 23, 63, 8));
            rect2.setEnterZoneEvent((player, l) -> CommonUtilsSpigot.getOnlinePlayers().forEach(p -> p.sendMessage("Le joueur " + player.getName() + " se trouve dans les zones : " + LCSpigot.getZoneManager().getZonesFromLoc(l))));
            rect2.setQuitZoneEvent((player, l) -> CommonUtilsSpigot.getOnlinePlayers().forEach(p -> p.sendMessage("Le joueur " + player.getName() + " se trouve dans les zones : " + LCSpigot.getZoneManager().getZonesFromLoc(l))));
            LCSpigot.getZoneManager().registerZone("NOIR", rect2);

            ZoneSphere sphere = new ZoneSphere(new Location(event.getBlock().getWorld(), 29, 60, 22), 5);
            sphere.setEnterZoneEvent((player, l) -> CommonUtilsSpigot.getOnlinePlayers().forEach(p -> p.sendMessage("Le joueur " + player.getName() + " se trouve dans les zones : " + LCSpigot.getZoneManager().getZonesFromLoc(l))));
            sphere.setQuitZoneEvent((player, l) -> CommonUtilsSpigot.getOnlinePlayers().forEach(p -> p.sendMessage("Le joueur " + player.getName() + " se trouve dans les zones : " + LCSpigot.getZoneManager().getZonesFromLoc(l))));
            LCSpigot.getZoneManager().registerZone("BLANC", sphere);

            ZoneCircle circle = new ZoneCircle(new Location(event.getBlock().getWorld(), 15, 60, 5), 9);
            circle.setEnterZoneEvent((player, l) -> CommonUtilsSpigot.getOnlinePlayers().forEach(p -> p.sendMessage("Le joueur " + player.getName() + " se trouve dans les zones : " + LCSpigot.getZoneManager().getZonesFromLoc(l))));
            circle.setQuitZoneEvent((player, l) -> CommonUtilsSpigot.getOnlinePlayers().forEach(p -> p.sendMessage("Le joueur " + player.getName() + " se trouve dans les zones : " + LCSpigot.getZoneManager().getZonesFromLoc(l))));
            LCSpigot.getZoneManager().registerZone("VERT", circle);

        }*/
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        LCAPI.getPermManager().onBlockBreak(event);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerDeath(PlayerDeathEvent e){
        PacketPlayInClientCommand packet = new PacketPlayInClientCommand(PacketPlayInClientCommand.EnumClientCommand.PERFORM_RESPAWN);
        ((CraftPlayer)e.getEntity()).getHandle().playerConnection.a(packet);
    }
}
