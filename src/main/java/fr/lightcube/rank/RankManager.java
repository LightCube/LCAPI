package fr.lightcube.rank;

import fr.lightcube.LCAPI;
import fr.lightcube.configuration.LCYamlConfiguration;
import fr.lightcube.utils.Csts;
import fr.lightcube.utils.Log;

import java.util.ArrayList;

/**
 * Created by Nitorac on 02/04/2017.
 */
public class RankManager {

    private static ArrayList<Rank> currentRanks;

    private static String chatDisplay;

    public RankManager(){
        currentRanks = new ArrayList<>();
        chatDisplay = LCAPI.getConfigManager().get(Csts.GENERAL_CONF).getString("chat-display", "%pseudo% > %msg%");
        readRanksFromFile();
    }

    public void reloadRanks(){
        chatDisplay = LCAPI.getConfigManager().get(Csts.GENERAL_CONF).load().getString("chat-display", "%pseudo% > %msg%");
        readRanksFromFile();
    }

    public Rank getRankFromName(String rankName){
        return currentRanks.stream().filter(rank -> rank.getName().equalsIgnoreCase(rankName)).findFirst().orElse(null);
    }

    public Rank getRankFromID(int rankID){
        return currentRanks.stream().filter(rank -> rank.getRankID() == rankID).findFirst().orElse(currentRanks.stream().filter(rank -> rank.getRankID() == 0).findFirst().orElse(null));
    }

    public ArrayList<String> getRanksNames(){
        ArrayList<String> result = new ArrayList<>();
        currentRanks.forEach(rank -> result.add(rank.getName()));
        return result;
    }

    public void readRanksFromFile(){
        try{
            currentRanks.clear();
            LCYamlConfiguration config = LCAPI.getConfigManager().get("rank.yml").load();
            config.getSection("", false).forEach(path -> currentRanks.add(new Rank(path, config.getString(path + ".prefix", ""),
                config.getString(path + ".color", ""), config.getInt(path + ".rank-lvl", 0))));

        }catch (Exception e){
            Log.ERROR.sendLog("Impossible de charger les rangs !");
        }
    }

    public void saveRanksToFile(){
        LCYamlConfiguration config = LCAPI.getConfigManager().get("rank.yml");
        currentRanks.forEach(rank -> {
            String basePath = rank.getName();

            config.setObject(basePath + ".prefix", rank.getPrefix());
            config.setObject(basePath + ".rank-lvl", rank.getRankID());
            config.setObject(basePath + ".color", rank.getColor());
        });
        config.save();
    }

    public String getChatDisplay(){
        return chatDisplay;
    }

    public ArrayList<Rank> getRanks(){
        return new ArrayList<>(currentRanks);
    }
}
