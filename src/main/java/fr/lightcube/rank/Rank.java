package fr.lightcube.rank;

import fr.lightcube.utils.Csts;
import lombok.Getter;
import org.bukkit.ChatColor;

/**
 * Created by Nitorac on 02/04/2017.
 */
public class Rank {

    @Getter private String name;
    @Getter private String prefix;
    @Getter private String color;
    @Getter private int rankID;

    public Rank(String name, String prefix, String color, int rankID){
        this.name = name;
        this.prefix = prefix;
        this.rankID = rankID;
        this.color = (Csts.isBungee) ? color.replace('&', '§') : ChatColor.translateAlternateColorCodes('&', color);
    }

    @Override
    public boolean equals(Object obj){
        if(obj != null && obj instanceof Rank && ((Rank)obj).getName().equals(getName()) &&
                ((Rank)obj).getRankID() == getRankID() && ((Rank)obj).getPrefix().equals(getPrefix())){
            return true;
        }
        return false;
    }
}
