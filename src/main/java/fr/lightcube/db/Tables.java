package fr.lightcube.db;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import fr.lightcube.utils.Utils;

import java.util.Map;

import static fr.lightcube.utils.Csts.*;

/**
 * Created by Nitorac on 21/03/2017.
 */
public enum Tables {
    //@TODO DECLARER MA TABLE
    PLAYERS(Utils.createMap(
        TABLE_PLAYERS,
        //            Str------Str------Str-------Int------Int----Int-----Int---------Int---------Boolean------String---------BigInt---------BigInt-------Boolean--------String-------Boolean---
        new String[]{"UUID", "NAME", "NICKNAME", "RANK", "LEVEL", "XP", "COINS", "LIGHT_COINS", "IS_ONLINE", "CUR_SERVER", "FIRST_LOGIN", "LAST_LOGIN", "MSG_TOGGLE", "GUILD_UUID", "MOD_TOGGLE"}
    )),
    GUILDS(Utils.createMap(
        TABLE_GUILDS,
        //             Str-----Str-----Str-----Int-----Int------Int--------Int-------
        new String[]{"UUID", "NAME", "LEADER", "XP", "LEVEL", "COINS", "LIGHT_COINS"}
    )),
    LINKSITE(Utils.createMap(
            TABLE_LINKSITE,
            //           Int------Str---
            new String[]{"ID", "BOSSBAR"}
    ));

    //Exemple: Renvoie "uuid, name, is_online"  de la requête d'exemple : INSERT INTO players (uuid, name, is_online) VALUES(?, ?, ?)
    public String getInsertColumns(){
        return Joiner.on(", ").join(getMap().values());
    }

    public Map<String, String[]> getMap() {
        return Maps.newHashMap(map);
    }

    public Map<String, String[]> map;

    Tables(Map<String, String[]> map) {
        this.map = map;
    }
}