package fr.lightcube.db;

import fr.lightcube.LCAPI;
import fr.lightcube.configuration.LCYamlConfiguration;
import fr.lightcube.utils.Csts;
import fr.lightcube.utils.Log;
import fr.lightcube.utils.Utils;

import java.sql.*;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by Nitorac on 21/03/2017.
 */
public class DBManager {

    public Connection connection;
    private static String DB_URL;
    private static String password;
    private static String user;

    /***************************************
     *      Initialisation de la base de données
     * @throws SQLException SQLException
     */
    public void init() throws SQLException {
        LCYamlConfiguration config = LCAPI.getConfigManager().get(Csts.GENERAL_CONF);
        DB_URL = "jdbc:mysql://" + config.getString("database.url", "") + "/" + config.getString("database.db", "") + "?autoReconnect=true";
        password = config.getString("database.password", "");
        user = config.getString("database.user", "");
        connection = DriverManager.getConnection(DB_URL, user, password);
    }

    public Connection getConnection() throws SQLException{
        if (!connection.isValid(0)) {
            Log.DEBUG.sendLog("La connection à MySql a été rechargée !");
            return connection = DriverManager.getConnection(DB_URL, user, password);
        }
        return connection;
    }

    /****************************************
     * @param ps Le PreparedStatement instancié
     * @param objs Les objects à mettre dans le statement
     * @return Le statement rempli des objects donnés
     * @throws SQLException SQLException
     */
    private PreparedStatement getReadyStatement(PreparedStatement ps, Object... objs) throws SQLException {
        int c = 1;
        for (Object obj : objs) {
            ps.setObject(c++, obj);
        }
        return ps;
    }

    /****************************************
     *          Ferme la connection si elle existe
     */
    public void closeConnection() {
        try {
            if (getConnection() != null) {
                getConnection().close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***************************************
     *          Permet d'ajouter une ligne dans la base de données
     * @param tableEnum Table
     * @param vals Valeurs
     * @throws SQLException SQLException
     */
    public void addRow(Tables tableEnum, Object... vals) throws SQLException{
        Map<String, String[]> table = tableEnum.getMap();

        Log.INFO.sendLog("Table size :" + table.entrySet().size());

        if(table.entrySet().size() <= 0)
            return;

        Map.Entry<String, String[]> entry = table.entrySet().iterator().next();
        String tableStr = entry.getKey();
        String[] typesStr = entry.getValue();

        if(entry.getValue().length != vals.length){
            Log.FATAL.sendLog("Le nombre de paramètres fourni ne correspond à la longueur des colonnes de la table");
            return;
        }

        StringBuilder sbTypes = new StringBuilder();
        StringBuilder sbVals = new StringBuilder();

        Arrays.stream(typesStr).forEach(s -> sbTypes.append(s).append(","));
        Arrays.stream(vals).forEach(obj -> sbVals.append("?,"));

        String types = Utils.removeLastChar(sbTypes.toString());
        String values = Utils.removeLastChar(sbVals.toString());

        PreparedStatement ps = getReadyStatement(getConnection().prepareStatement(
                "INSERT INTO " + tableStr + "(" + types + ") VALUES(" + values + ")"), vals);
        ps.executeUpdate();
        ps.close();
    }

    /*******************************************
     *          Renvoie les résultats d'une requête
     * @param quest Requête SQL
     * @param objs Objets à définir
     * @return Le ResultSet de la requête
     * @throws SQLException SQLException
     */
    public ResultSet getResultPreStat(String quest, Object... objs) throws SQLException {
        return getReadyStatement(getConnection().prepareStatement(quest), objs).executeQuery();
    }

    /*******************************************
     *          Renvoie les résultats d'une requête silencieusement
     * @param quest Requête SQL
     * @param objs Objets à définir
     * @return Le ResultSet de la requête
     * @throws SQLException SQLException
     */
    public ResultSet getResultPreStatSilent(String quest, Object... objs){
        try {
            return getReadyStatement(getConnection().prepareStatement(quest), objs).executeQuery();
        } catch (SQLException e) {
            Log.ERROR.sendLog("Erreur de query de la base de données : " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public void updateAsync(String quest, Object... objs){
        LCAPI.getCommonUtils().runAsync(() -> updateSilent(quest, objs));
    }

    public void update(String quest, Object... objs) throws SQLException {
        getReadyStatement(getConnection().prepareStatement(quest), objs).executeUpdate();
    }

    public void updateSilent(String quest, Object... objs){
        try {
            getReadyStatement(getConnection().prepareStatement(quest), objs).executeUpdate();
        } catch (SQLException e) {
            Log.ERROR.sendLog("Erreur d'update de la base de données : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public String getDbUrl(){
        return DB_URL;
    }
}
