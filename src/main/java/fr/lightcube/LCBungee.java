package fr.lightcube;

import com.ullink.slack.simpleslackapi.events.SlackEventType;
import fr.lightcube.commands.bungee.*;
import fr.lightcube.listeners.bungee.ServerEvents;
import fr.lightcube.utils.CommonUtilsBungee;
import fr.lightcube.utils.Csts;
import fr.lightcube.utils.Log;
import fr.lightcube.utils.Utils;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import org.apache.commons.lang.StringEscapeUtils;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import static fr.lightcube.utils.PermCsts.*;

/**
 * Created by Nitorac on 31/03/2017.
 */
public class LCBungee extends Plugin {

    private static boolean failure = false;
    private static LCBungee instance;

    public static final ArrayList<Listener> baseListeners = new ArrayList<>(Arrays.asList(
        new ServerEvents()
    ));

    public static final ArrayList<CommandPermissive> baseCommands = new ArrayList<>(Arrays.asList(
        new BroadcastCommand("br", LCAPI_BROADCAST_COMMAND),
        new CoinsCommand("coins", LCAPI_COINS_COMMAND),
        new CoinsCommand("c", LCAPI_COINS_COMMAND),
        new LightCoinsCommand("lightcoins",LCAPI_LIGHTCOINS_COMMAND),
        new LightCoinsCommand("lc",LCAPI_LIGHTCOINS_COMMAND),
        new MsgCommand("msg", LCAPI_MSG_COMMAND),
        new RCommand("r", LCAPI_MSG_COMMAND),
        new MsgSpyCommand("msgspy", LCAPI_MSGSPY_COMMAND),
        new LastSeenCommand("lastseen", LCAPI_LASTSEEN_COMMAND),
        new InfoCommand("info", LCAPI_INFO_COMMAND),
        new StaffCommand("staff", LCAPI_STAFF_COMMAND),
        new SCCommand("staffchat", LCAPI_STAFFCHAT_COMMAND),
        new SCCommand("sc", LCAPI_STAFFCHAT_COMMAND),
        new SetRankCommand("setrank", LCAPI_SETRANK_COMMAND)
    ));

    public LCBungee(){
        instance = this;
        Csts.isBungee = true;

        failure = !new LCAPI().init();
    }

    @Override
    public void onEnable(){
        if(failure){
            Log.FATAL.sendLog("Impossible de démarrer l'API correctement");
            if(LCAPI.getCommonUtils() != null){
                LCAPI.getCommonUtils().safeShutdown("Le Bungee n'a pas réussi à démarrer !", true);
            }
            return;
        }

        LCAPI.getPermManager().init();

        // LE RELOAD
        LCAPI.getPlayerManager().reset();
        getInstance().getProxy().getPlayers().forEach(player -> LCAPI.getPlayerManager().addPlayer(player));

        registerRedisListener();
        //Register Events
        baseListeners.forEach(list -> getInstance().getProxy().getPluginManager().registerListener(this, list));

        //Register Commands
        baseCommands.forEach(command -> getInstance().getProxy().getPluginManager().registerCommand(this, command));

        LCAPI.getSlackManager().getSession().addMessagePostedListener((event, session) -> {
            if(!Csts.isBungee && event.getChannel().getName().equalsIgnoreCase("staff-chat") && !event.getSender().isBot() && event.getEventType().equals(SlackEventType.SLACK_MESSAGE_POSTED)){
                String pseudo = Csts.SC_PREFIX + " §d" + event.getSender().getRealName().split("-")[0] + "(Slack)§r";
                String chatDisplay = ChatColor.translateAlternateColorCodes('&', LCAPI.getRankManager().getChatDisplay());
                CommonUtilsBungee.sendToPlayerPermFilter(chatDisplay.replace("%pseudo%", pseudo).replace("%msg%", StringEscapeUtils.unescapeHtml(event.getMessageContent())), LCAPI_IS_STAFF);
            }
        });

        Log.INFO.sendLog("LCAPI (Bungee) a ete active !");
    }

    public void registerRedisListener(){
        LCAPI.getRedisManager().init();
        LCAPI.getRedisManager().registerReceiver("LCBungeeInit", (subchannel, msg) -> {
            switch(subchannel){
                case "LCReload":
                    if(msg.equals("All")){
                        CommonUtilsBungee.reloadPlayers();
                        CommonUtilsBungee.reloadRank();
                    }else if(msg.equals("Players")) {
                        CommonUtilsBungee.reloadPlayers();
                    }else if(msg.equals("Ranks")){
                        CommonUtilsBungee.reloadRank();
                    }else if(msg.startsWith("Player ")){
                        UUID playerUuid = Utils.fromString(msg.replaceAll("Player ", ""));
                        ProxiedPlayer victim = getProxy().getPlayer(playerUuid);
                        if(victim != null){
                            LCAPI.getPlayerManager().reloadPlayer(victim);
                        }
                    }
                    break;
            }
        });
    }

    @Override
    public void onDisable(){
        Log.WARN.sendLog("LCAPI (Bungee) a ete desactive !");
    }

    public static LCBungee getInstance(){
        return instance;
    }
}