package fr.lightcube.hologram;

import fr.lightcube.LCSpigot;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

/**
 * Created by Nitorac on 03/07/2017.
 */
public class HoloTouchListener implements Listener {

    @EventHandler
    public void onTouch(PlayerInteractAtEntityEvent event) {
        if (event.getRightClicked() instanceof ArmorStand && event.getRightClicked().getCustomName() != null && !((ArmorStand) event.getRightClicked()).isVisible()) {
            ArmorStand armorStand = (ArmorStand) event.getRightClicked();
            Hologram hologram = LCSpigot.getHoloManager().getHologram(armorStand);
            if (hologram != null) {
                event.setCancelled(true);
                hologram.callEvents(event.getPlayer(), hologram);
            }
        }
    }

    @EventHandler
    public void onDamageEntity(EntityDamageEvent e){
        if(e.getEntity().getType().equals(EntityType.ARMOR_STAND) && e.getEntity().getCustomName() != null && !((ArmorStand) e.getEntity()).isVisible()){
            ArmorStand armorStand = (ArmorStand) e.getEntity();
            Hologram hologram = LCSpigot.getHoloManager().getHologram(armorStand);
            if(hologram != null){
                e.setCancelled(true);
            }
        }
    }
}