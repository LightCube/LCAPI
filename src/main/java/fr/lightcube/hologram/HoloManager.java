package fr.lightcube.hologram;

import org.bukkit.entity.ArmorStand;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nitorac on 03/07/2017.
 */
public class HoloManager {

    private Map<String, Hologram> holograms;

    public HoloManager(){
        holograms = new HashMap<>();
    }

    public Map<String, Hologram> getHologramMap() {
        return holograms;
    }

    public void registerHologram(String name, Hologram hologram){
        holograms.putIfAbsent(name, hologram);
    }

    public void replaceHologram(String name, Hologram newHolo){
        holograms.replace(name, newHolo);
    }

    public void deleteHologram(String name){
        Hologram h = holograms.remove(name);
        if (h != null) {
            h.remove();
        }
    }

    public Hologram getHologram(ArmorStand armorStand){
        return holograms.values().stream().filter(h -> h.getPhysicalEntities().contains(armorStand)).findFirst().orElse(null);
    }

    public Hologram getHologram(String name){
        return holograms.get(name);
    }

    public void disable(){
        holograms.values().forEach(Hologram::remove);
    }
}
