package fr.lightcube.hologram;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * Created by Nitorac on 03/07/2017.
 */
public class Hologram {

    private ArrayList<ArmorStand> physicalEntities;
    private ArrayList<String> content;
    private Map<String, HoloTouchEvent> events;
    private Location hologramLocation;

    public Hologram(Location location, String firstLine, String... lines) {
        this.content = new ArrayList<>();
        this.physicalEntities = new ArrayList<>();
        this.events = new HashMap<>();

        this.content.add(firstLine);
        this.content.addAll(Arrays.asList(lines));

        spawn(location);
    }

    private void spawn(Location location) {
        this.hologramLocation = location;

        int cycle = 0;
        for (String str : content) {
            cycle++;
            if (cycle > 1) {
                location = location.subtract(0, 0.25, 0);
            }
            ArmorStand physicalEntity = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
            physicalEntity.setVisible(false);
            physicalEntity.setGravity(false);
            physicalEntity.setCustomNameVisible(true);
            physicalEntity.setCustomName(ChatColor.translateAlternateColorCodes('&', str));
            physicalEntity.setRemoveWhenFarAway(false);

            physicalEntities.add(physicalEntity);
        }
    }

    public boolean isTouchscreen() {
        return !(events.size() <= 0 || events == null);
    }

    public ArrayList<String> getContent() {
        return content;
    }

    public List<ArmorStand> getPhysicalEntities() {
        return physicalEntities;
    }

    public void remove() {
        for (ArmorStand armorStand : getPhysicalEntities()) {
            armorStand.remove();
        }
    }

    public Location getLocation() {
        return hologramLocation;
    }

    public void addEvent(String eventName, HoloTouchEvent e){
        events.putIfAbsent(eventName, e);
    }

    public void replaceEvent(String eventName, HoloTouchEvent newEv){
        events.replace(eventName, newEv);
    }

    public void removeEvent(String eventName){
        events.remove(eventName);
    }

    public void callEvents(Player p, Hologram h){
        events.values().forEach(e -> e.onPlayerTouchHolo(p, h));
    }

    public void setLocation(Location location) {
        this.hologramLocation = location;
        this.remove();
        this.spawn(location);
    }

    public void addLine(String line) {
        content.add(ChatColor.translateAlternateColorCodes('&', line));
        this.remove();
        this.spawn(this.getLocation());
    }

public interface HoloTouchEvent{
    void onPlayerTouchHolo(Player player, Hologram touched);
}
}