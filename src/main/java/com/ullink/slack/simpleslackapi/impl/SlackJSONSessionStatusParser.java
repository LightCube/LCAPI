package com.ullink.slack.simpleslackapi.impl;

import com.ullink.slack.simpleslackapi.*;
import fr.lightcube.utils.Log;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.HashMap;
import java.util.Map;

class SlackJSONSessionStatusParser {

    private Map<String, SlackChannel>       channels     = new HashMap<>();
    private Map<String, SlackUser>          users        = new HashMap<>();
    private Map<String, SlackIntegration>   integrations = new HashMap<>();

    private SlackPersona              sessionPersona;

    private SlackTeam                 team;

    private String                    webSocketURL;

    private String                    toParse;

    private String                    error;

    SlackJSONSessionStatusParser(String toParse)
    {
        this.toParse = toParse;
    }

    Map<String, SlackChannel> getChannels()
    {
        return channels;
    }

    Map<String, SlackUser> getUsers()
    {
        return users;
    }

    Map<String,SlackIntegration> getIntegrations() {
        return integrations;
    }

    public String getWebSocketURL()
    {
        return webSocketURL;
    }

    public String getError()
    {
        return error;
    }
    
    void parse() throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject jsonResponse = (JSONObject) parser.parse(toParse);
        Boolean ok = (Boolean)jsonResponse.get("ok");
        if (Boolean.FALSE.equals(ok)) {
            error = (String)jsonResponse.get("error");
            return;
        }
        JSONArray usersJson = (JSONArray) jsonResponse.get("users");

        for (Object jsonObject : usersJson)
        {
            JSONObject jsonUser = (JSONObject) jsonObject;
            SlackUser slackUser = SlackJSONParsingUtils.buildSlackUser(jsonUser);
            users.put(slackUser.getId(), slackUser);
        }

        StringBuilder sb = new StringBuilder();
        users.values().forEach(slackUser -> sb.append(slackUser.getRealName()).append(",    "));
        Log.INFO.sendLog("Utilisateurs trouvés (" + users.size() + ") : " + sb.toString());

        JSONArray integrationsJson = (JSONArray) jsonResponse.get("bots");
        if (integrationsJson != null) {
            for (Object jsonObject : integrationsJson)
            {
                JSONObject jsonIntegration = (JSONObject) jsonObject;
                SlackIntegration slackIntegration = SlackJSONParsingUtils.buildSlackIntegration(jsonIntegration);
                integrations.put(slackIntegration.getId(), slackIntegration);
            }
        }

        JSONArray channelsJson = (JSONArray) jsonResponse.get("channels");

        for (Object jsonObject : channelsJson)
        {
            JSONObject jsonChannel = (JSONObject) jsonObject;
            SlackChannelImpl channel = SlackJSONParsingUtils.buildSlackChannel(jsonChannel, users);
            channels.put(channel.getId(), channel);
        }

        JSONArray groupsJson = (JSONArray) jsonResponse.get("groups");

        for (Object jsonObject : groupsJson)
        {
            JSONObject jsonChannel = (JSONObject) jsonObject;
            SlackChannelImpl channel = SlackJSONParsingUtils.buildSlackChannel(jsonChannel, users);
            channels.put(channel.getId(), channel);
        }

        StringBuilder sb2 = new StringBuilder();
        channels.values().forEach(slackChannel -> sb2.append(slackChannel.getName()).append(" (").append(slackChannel.getType().name()).append("),    "));
        Log.INFO.sendLog("Channels trouvés (" + channels.size() + ") : " + sb2.toString());

        JSONArray imsJson = (JSONArray) jsonResponse.get("ims");

        for (Object jsonObject : imsJson)
        {
            JSONObject jsonChannel = (JSONObject) jsonObject;
            SlackChannelImpl channel = SlackJSONParsingUtils.buildSlackImChannel(jsonChannel, users);
            channels.put(channel.getId(), channel);
        }

        JSONObject selfJson = (JSONObject) jsonResponse.get("self");
        sessionPersona = SlackJSONParsingUtils.buildSlackUser(selfJson);

        JSONObject teamJson = (JSONObject) jsonResponse.get("team");
        team = SlackJSONParsingUtils.buildSlackTeam(teamJson);

        webSocketURL = (String) jsonResponse.get("url");

    }

    public SlackPersona getSessionPersona()
    {
        return sessionPersona;
    }

    public SlackTeam getTeam()
    {
        return team;
    }
}
