package com.ullink.slack.simpleslackapi.events;

public interface SlackEvent {

    SlackEvent UNKNOWN_EVENT = () -> SlackEventType.UNKNOWN;
    
    SlackEventType getEventType();
}
